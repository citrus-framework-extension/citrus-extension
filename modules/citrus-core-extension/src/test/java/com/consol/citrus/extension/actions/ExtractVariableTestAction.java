package com.consol.citrus.extension.actions;

import com.consol.citrus.actions.CreateVariablesAction;
import com.consol.citrus.exceptions.CitrusRuntimeException;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ExtractVariableTestAction extends AbstractTestNGUnitTest {

    @Test
    public void testExtractVariableTestAction() {
        String variableNameToExtract = "var1";
        String variableValue = "value1";
        Map<String, String> variables = new HashMap<String, String>() {{
            put(variableNameToExtract, variableValue);
            put("var2", "value2");
        }};
        CreateVariablesAction createVariablesAction = new CreateVariablesAction();
        createVariablesAction.setVariables(variables);
        createVariablesAction.execute(context);

        ExtractVariableAction extractVariableAction = new ExtractVariableAction();
        extractVariableAction.setVariableName(variableNameToExtract);

        extractVariableAction.execute(context);
        String extractedVariableValue = String.valueOf(extractVariableAction.getExtractedVariable());

        Assert.assertEquals(extractedVariableValue, variableValue);
    }

    @Test(expectedExceptions = {CitrusRuntimeException.class},
            expectedExceptionsMessageRegExp = "Unknown variable '.*'")
    public void testExtractVariableTestActionVariableNotFound() {
        String variableNameToExtract = "var1";
        Map<String, String> variables = new HashMap<String, String>() {{
            put("var2", "value2");
        }};
        CreateVariablesAction createVariablesAction = new CreateVariablesAction();
        createVariablesAction.setVariables(variables);
        createVariablesAction.execute(context);

        ExtractVariableAction extractVariableAction = new ExtractVariableAction();
        extractVariableAction.setVariableName(variableNameToExtract);

        extractVariableAction.execute(context);
    }

    @Test
    public void testExtractVariableTestActionVariableNoVariablesToExtract() {
        String variableNameToExtract = "var1";
        ExtractVariableAction extractVariableAction = new ExtractVariableAction();
        extractVariableAction.setVariableName(variableNameToExtract);
        extractVariableAction.execute(context);
    }

    @Test(expectedExceptions = {CitrusRuntimeException.class},
            expectedExceptionsMessageRegExp = "Unknown variable '.*'")
    public void testExtractVariableTestActionVariableNotSetVariableNameToExtract() {
        Map<String, String> variables = new HashMap<String, String>() {{
            put("var1", "value1");
            put("var2", "value2");
        }};
        CreateVariablesAction createVariablesAction = new CreateVariablesAction();
        createVariablesAction.setVariables(variables);
        createVariablesAction.execute(context);

        ExtractVariableAction extractVariableAction = new ExtractVariableAction();

        extractVariableAction.execute(context);
    }

}
