package com.consol.citrus.extension.message;

import com.consol.citrus.message.DefaultMessage;
import com.consol.citrus.message.MessageHeaders;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;

public abstract class DefaultMessageDeserializerBaseTest extends AbstractTestNGUnitTest {

    protected void assertDefaultMessage(DefaultMessage defaultMessage) {
        Assert.assertNotNull(defaultMessage);
        Assert.assertNotNull(defaultMessage.getHeader(MessageHeaders.ID));
        Assert.assertNotNull(defaultMessage.getHeader(MessageHeaders.TIMESTAMP));
    }

}
