package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Collection;
import java.util.List;

public class AbstractTestDataLoaderTest extends AbstractTestNGUnitTest {

    @Test
    public void testGetterSetterTestDataPath() {
        String testDataPath = "classpath:test-data-GetterSetter/";
        AbstractTestDataLoader testDataLoader = new AbstractTestDataLoader() {
            @Override
            protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) {
            }
        };

        testDataLoader.setTestDataPath(testDataPath);

        Assert.assertEquals(testDataLoader.getTestDataPath(), testDataPath);
    }

    @Test
    public void testGetterExtensions() {
        AbstractTestDataLoader testDataLoader = new AbstractTestDataLoader() {
            @Override
            protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) {
            }
        };

        Assert.assertNull(testDataLoader.getExtensions());
    }

    @Test
    public void testSetterGetterRecursive() {
        boolean recursive = true;
        AbstractTestDataLoader testDataLoader = new AbstractTestDataLoader() {
            @Override
            protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) {
            }
        };

        //noinspection ConstantConditions
        testDataLoader.setRecursive(recursive);

        Assert.assertTrue(testDataLoader.isRecursive());
    }

    @Test
    public void testFindFilesToLoad() {
        int numberOfExpectedFiles = 2;
        String testDataPath = "classpath:test-data-testFindFilesToLoad/";
        AbstractTestDataLoader testDataLoader = new AbstractTestDataLoader() {
            @Override
            protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) {
            }
        };
        testDataLoader.setTestDataPath(testDataPath);

        Collection<File> files = testDataLoader.findFilesToLoad();

        Assert.assertNotNull(files);
        Assert.assertEquals(files.size(), numberOfExpectedFiles);
    }

    @Test
    public void testFindFilesToLoadExceptions() {
        String incorrectTestDataPath = "classpath:incorrect-test-data/";
        AbstractTestDataLoader testDataLoader = new AbstractTestDataLoader() {
            @Override
            protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) {
            }
        };
        testDataLoader.setTestDataPath(incorrectTestDataPath);

        testDataLoader.findFilesToLoad();
    }

    @Test
    public void testDirectPathToFile() {
        String directPathToFile = "classpath:test-data-direct-path/file.txt";
        AbstractTestDataLoader testDataLoader = new AbstractTestDataLoader(new String[]{"txt"}) {
            @Override
            protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) throws IOException {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(testDataFile));
                String textLine = bufferedReader.readLine();
                testDataList.add(textLine);
            }
        };
        testDataLoader.setTestDataPath(directPathToFile);

        Object[][] testData = testDataLoader.getTestData();

        Assert.assertEquals(testData[0][0], "Test data");
    }

}
