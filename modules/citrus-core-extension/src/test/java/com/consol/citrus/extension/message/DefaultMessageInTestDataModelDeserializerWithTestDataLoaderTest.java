package com.consol.citrus.extension.message;

import com.consol.citrus.extension.data.TestDataLoader;
import com.consol.citrus.extension.data.TestDataLoaders;
import com.consol.citrus.message.DefaultMessage;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class DefaultMessageInTestDataModelDeserializerWithTestDataLoaderTest extends DefaultMessageDeserializerBaseTest {

    private TestDataLoader testDataLoader;

    @BeforeTest
    public void setUpObjectMapper() {
        testDataLoader = TestDataLoaders.json()
                .clazz(TestDataModel.class)
                .testDataPath("classpath:default-message-deserializer-test-data-loader/")
                .build();
    }

    @Test
    public void testDefaultMessageInTestDataModelDeserializerWithTestDataLoaderTest() {
        Object[][] testData = testDataLoader.getTestData();

        TestDataModel testDataModel = (TestDataModel) testData[0][0];
        DefaultMessage defaultMessageRequest = testDataModel.getDefaultMessageRequest();
        DefaultMessage defaultMessageResponse = testDataModel.getDefaultMessageResponse();

        assertDefaultMessage(defaultMessageRequest);
        assertDefaultMessage(defaultMessageResponse);
    }

}
