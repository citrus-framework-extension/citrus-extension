package com.consol.citrus.extension.data.model;

public class ComplexModelForTest1 {

    private int number;
    private String string;
    private ComplexModelForTest2 complexModelForTest2;

    public ComplexModelForTest1() {
        this(0, "", new ComplexModelForTest2());
    }

    public ComplexModelForTest1(int number, String string, ComplexModelForTest2 complexModelForTest2) {
        this.number = number;
        this.string = string;
        this.complexModelForTest2 = complexModelForTest2;
    }

    public int getNumber() {
        return number;
    }

    public ComplexModelForTest1 setNumber(int number) {
        this.number = number;
        return this;
    }

    public String getString() {
        return string;
    }

    public ComplexModelForTest1 setString(String string) {
        this.string = string;
        return this;
    }

    public ComplexModelForTest2 getComplexModelForTest2() {
        return complexModelForTest2;
    }

    public ComplexModelForTest1 setComplexModelForTest2(ComplexModelForTest2 complexModelForTest2) {
        this.complexModelForTest2 = complexModelForTest2;
        return this;
    }
}
