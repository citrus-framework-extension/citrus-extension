package com.consol.citrus.extension.data;

import com.consol.citrus.extension.data.model.ComplexModelForTest1;
import com.consol.citrus.extension.data.model.ComplexModelForTest2;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static com.consol.citrus.extension.data.AbstractTestDataFilter.NOT_FOUND;
import static com.consol.citrus.extension.data.AbstractTestDataFilter.PROP_PREFIX;

@TestPropertySource(locations = {"classpath:abstract-test-data-filter.properties"})
@ContextConfiguration(classes = {AbstractTestDataFilterConfig.class})
public class AbstractTestDataFilterTest extends AbstractTestNGUnitTest {

    @Autowired
    private AbstractTestDataFilter abstractTestDataFilter;

    @BeforeMethod
    public void setUpTestDataFilterBeforeTestMethod() {
        abstractTestDataFilter.setFilterExpression("");
    }

    @Test
    public void testCreateInstance() {
        String testDataFilterName = "abstractTestDataFilter";
        Assert.assertEquals(abstractTestDataFilter.getName(), testDataFilterName);
    }

    @Test
    public void testSetterGetterFilterExpression() {
        String filterExpression = "a='1' AND b='2'";
        abstractTestDataFilter.setFilterExpression(filterExpression);
        Assert.assertEquals(abstractTestDataFilter.getFilterExpression(), filterExpression);
    }

    @Test
    public void testGetFullFilterName() {
        String expectedName = PROP_PREFIX + "." + "abstractTestDataFilter";
        Assert.assertEquals(abstractTestDataFilter.getFullFilterName(), expectedName);
    }

    @Test
    public void testFindFilterExpression_Found() {
        String expectedFilterExpression = "a='1' AND b='2'";
        String filterExpression = abstractTestDataFilter.findFilterExpression();
        Assert.assertEquals(filterExpression, expectedFilterExpression);
    }

    @Test(dataProvider = "testDataForTestIsExpressionCorrect")
    public void testIsExpressionCorrect(String filterExpression, boolean expectedResult) {
        log.info("Input test data: filterExpression [" + filterExpression + "] expectedResult [" + expectedResult + "]");
        boolean result = abstractTestDataFilter.isExpressionCorrect(filterExpression);
        Assert.assertEquals(result, expectedResult);
    }

    @DataProvider
    public Object[][] testDataForTestIsExpressionCorrect() {
        return new Object[][]{
                {null, false},
                {"null", false},
                {"", false},
                {"filedName=''", true},
                {"filedName='1'", true},
                {"filedName='1' ", false},
                {"filedName='1'  ", false},
                {"filedName='1'   ", false},
                {"filedName='someText'", true},
                {"filedName='someText' OR filedName2", false},
                {"filedName'someText'", false},
                {"filedName=''1'", false},
                {"filedName='1''", false},
                {"filedName=1", false},
                {"filedName", false},
                {"filedName=", false},
                {"someFiled.filedName='1'", true},
                {"someFiled.filedName='someText'", true},
                {"someFiled.filedName=", false},
                {"someFiled.filedName.field3='1'", true},
                {"filedName='1' AND field2='2'", true},
                {"filedName='someText' AND field2='someText'", true},
                {"filedName='someText' AND field2='someText' ", false},
                {"filedName='someText' AND field2=someText'", false},
                {"filedName='someText' AND field2=someText", false},
                {"filedName='someText' AND field2=", false},
                {"filedName='someText' OR field2='someText'", true},
                {"filedName='someText' AND field2='someText' OR filed3='someValue'", true},
                {"somObj.field1='someText' AND someObj.field2='someText'", true},
        };
    }

    @Test(dataProvider = "testDataForTestOperations")
    public void testOperations(String filterExpression, List<String> expectedOperations) {
        log.info("Input test data: filterExpression [" + filterExpression + "] expectedOperations [" + expectedOperations + "]");
        List<FilterOperation> foundOperations = abstractTestDataFilter.operations(filterExpression);
        Assert.assertEquals(foundOperations, expectedOperations);
    }

    @DataProvider
    public Object[][] testDataForTestOperations() {
        return new Object[][]{
                {"filed1='1'", new ArrayList<FilterOperation>() {{
                    add(new FilterOperation("filed1='1'", FilterOperator.NONE));
                }}},
                {"filed1='1' AND filed2='2'", new ArrayList<FilterOperation>() {{
                    add(new FilterOperation("filed1='1'", FilterOperator.AND));
                    add(new FilterOperation("filed2='2'", FilterOperator.NONE));
                }}},
                {"filed1='1' AND filed2='2' AND filed3='3'", new ArrayList<FilterOperation>() {{
                    add(new FilterOperation("filed1='1'", FilterOperator.AND));
                    add(new FilterOperation("filed2='2'", FilterOperator.AND));
                    add(new FilterOperation("filed3='3'", FilterOperator.NONE));
                }}},
                {"filed1='1' AND filed2='2' AND filed3='3' AND filed4='4'", new ArrayList<FilterOperation>() {{
                    add(new FilterOperation("filed1='1'", FilterOperator.AND));
                    add(new FilterOperation("filed2='2'", FilterOperator.AND));
                    add(new FilterOperation("filed3='3'", FilterOperator.AND));
                    add(new FilterOperation("filed4='4'", FilterOperator.NONE));
                }}},
                {"filed1='1' OR filed2='2'", new ArrayList<FilterOperation>() {{
                    add(new FilterOperation("filed1='1'", FilterOperator.OR));
                    add(new FilterOperation("filed2='2'", FilterOperator.NONE));
                }}},
                {"filed1='1' AND filed2='2' OR filed3='3'", new ArrayList<FilterOperation>() {{
                    add(new FilterOperation("filed1='1'", FilterOperator.AND));
                    add(new FilterOperation("filed2='2'", FilterOperator.OR));
                    add(new FilterOperation("filed3='3'", FilterOperator.NONE));
                }}},
        };
    }

    @Test(dataProvider = "testDataForTestGetResultFromObject")
    public void testGetResultFromObject(String filterExpression, Object inputObject, String expectedResult) {
        String result = abstractTestDataFilter.getResultFromObject(filterExpression, inputObject);
        Assert.assertEquals(result, expectedResult);
    }

    @DataProvider
    public Object[][] testDataForTestGetResultFromObject() {
        return new Object[][]{
                {"", null, "null"},
                {"notExistingElement='1'", new TestDataSumCalc(1, 2, 3), NOT_FOUND},
                {"notExistingElement", new TestDataSumCalc(1, 2, 3), NOT_FOUND},
                {"notExistingElement=", new TestDataSumCalc(1, 2, 3), NOT_FOUND},
                {"number1='1'", new TestDataSumCalc(1, 2, 3), "1"},
                {"number1=", new TestDataSumCalc(1, 2, 3), "1"},
                {"number1=''", new TestDataSumCalc(1, 2, 3), "1"},
                {"number2='1'", new TestDataSumCalc(1, 2, 3), "2"},
                {"expectedResult='1'", new TestDataSumCalc(1, 2, 3), "3"},
                {"number='1'", new ComplexModelForTest1(), "0"},
                {"number='1'", new ComplexModelForTest1(1, "string", new ComplexModelForTest2()), "1"},
                {"complexModelForTest2.number='1'", new ComplexModelForTest1(1, "string", null), "null"},
                {"complexModelForTest2.number='1'", new ComplexModelForTest1(1, "string", new ComplexModelForTest2(20)), "20"},
        };
    }

}