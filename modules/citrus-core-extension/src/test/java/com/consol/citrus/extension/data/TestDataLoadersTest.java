package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {AbstractTestDataFilterConfig.class})
public class TestDataLoadersTest extends AbstractTestNGUnitTest {

    @Autowired
    private TestDataFilter defaultTestDataFilter;

    @Test
    public void testJsonTestDataLoaderBuilder() {
        JsonTestDataLoaderBuilder jsonTestDataLoaderBuilder = TestDataLoaders.json();
        Assert.assertNotNull(jsonTestDataLoaderBuilder);
    }

    @Test
    public void testXmlTestDataLoaderBuilder() {
        XmlTestDataLoaderBuilder xmlTestDataLoaderBuilder = TestDataLoaders.xml();
        Assert.assertNotNull(xmlTestDataLoaderBuilder);
    }

    @Test
    public void testJsonTestDataLoaderBuilderWithTestDataFilter() {
        JsonTestDataLoaderBuilder jsonTestDataLoaderBuilder = TestDataLoaders.json(defaultTestDataFilter);
        Assert.assertNotNull(jsonTestDataLoaderBuilder);
    }

    @Test
    public void testXmlTestDataLoaderBuilderWithTestDataFilter() {
        XmlTestDataLoaderBuilder xmlTestDataLoaderBuilder = TestDataLoaders.xml(defaultTestDataFilter);
        Assert.assertNotNull(xmlTestDataLoaderBuilder);
    }

}