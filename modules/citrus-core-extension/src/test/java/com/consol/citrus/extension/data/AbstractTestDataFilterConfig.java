package com.consol.citrus.extension.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.List;


@Configuration
public class AbstractTestDataFilterConfig {

    @Autowired
    private Environment environment;

    @Bean
    public TestDataFilter abstractTestDataFilter() {
        return new AbstractTestDataFilter(environment) {
            @Override
            public List<Object> filterTestData(List<Object> testData) {
                return null;
            }
        };
    }

    @Bean
    public TestDataFilter defaultTestDataFilter() {
        return new DefaultTestDataFilter(environment);
    }

}
