package com.consol.citrus.extension.data;

import java.util.Objects;

@SuppressWarnings("unused")
public class TestDataSumCalc {

    private int number1, number2, expectedResult;

    public TestDataSumCalc() {
    }

    public TestDataSumCalc(int number1, int number2, int expectedResult) {
        this.number1 = number1;
        this.number2 = number2;
        this.expectedResult = expectedResult;
    }

    public int getNumber1() {
        return number1;
    }

    public void setNumber1(int number1) {
        this.number1 = number1;
    }

    public int getNumber2() {
        return number2;
    }

    public void setNumber2(int number2) {
        this.number2 = number2;
    }

    public int getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(int expectedResult) {
        this.expectedResult = expectedResult;
    }

    @Override
    public String toString() {
        return "TestDataSumCalc{" +
                "number1=" + number1 +
                ", number2=" + number2 +
                ", expectedResult=" + expectedResult +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(number1, number2, expectedResult);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        TestDataSumCalc that = (TestDataSumCalc) other;
        return number1 == that.number1 && number2 == that.number2 && expectedResult == that.expectedResult;
    }

}
