package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class JsonTestDataLoaderTest extends AbstractTestNGUnitTest {

    @Test
    public void testSetterGetterObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonTestDataLoader testDataLoader = new JsonTestDataLoader();

        testDataLoader.setObjectMapper(objectMapper);

        Assert.assertEquals(testDataLoader.getObjectMapper(), objectMapper);
    }

    @Test
    public void testGetterExtensions() {
        String[] expectedExtensions = {"json"};
        JsonTestDataLoader testDataLoader = new JsonTestDataLoader();

        Assert.assertEquals(testDataLoader.getExtensions(), expectedExtensions);
    }

    @Test
    public void testSetterGetterClass() {
        class SomeClass {

        }
        JsonTestDataLoader testDataLoader = new JsonTestDataLoader();

        testDataLoader.setClazz(SomeClass.class);

        Assert.assertEquals(testDataLoader.getClazz(), SomeClass.class);
    }

    @Test
    public void testLoadJsonTestDataFiles() {
        int numberOfExpectedLoadedData = 2;
        String testDataPath = "classpath:test-data-json/";
        JsonTestDataLoader testDataLoader = new JsonTestDataLoader();
        testDataLoader.setTestDataPath(testDataPath);
        testDataLoader.setClazz(TestDataSumCalc.class);

        Object[][] testDataArray = testDataLoader.getTestData();

        Assert.assertNotNull(testDataArray);
        Assert.assertEquals(testDataArray.length, numberOfExpectedLoadedData);
        Arrays.stream(testDataArray)
                .forEach(testDataElement -> {
                    TestDataSumCalc testDataSumCalc = (TestDataSumCalc) testDataElement[0];

                    int actualSum = testDataSumCalc.getNumber1() + testDataSumCalc.getNumber2();
                    Assert.assertEquals(actualSum, testDataSumCalc.getExpectedResult());
                });
    }

}