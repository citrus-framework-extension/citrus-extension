package com.consol.citrus.extension.actions;

import com.consol.citrus.extension.exceptions.CitrusExtensionRuntimeException;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SleepActionTest extends AbstractTestNGUnitTest {

    @Test(dataProvider = "testDataProviderForTestSleepActionWithCorrectValues")
    public void testSleepActionWithCorrectValues(String delayTimeExpression) {
        SleepAction sleepAction = new SleepAction().setDelayTimeExpression(delayTimeExpression)
                .setHandleException(false);

        sleepAction.doExecute(context);
    }

    @Test(dataProvider = "testDataProviderForTestSleepActionWithCorrectValues")
    public void testSleepActionWithCorrectValuesWithUseOfContext(String delayTimeExpression) {
        context.setVariable("delayVar", delayTimeExpression);

        SleepAction sleepAction = new SleepAction()
                .setDelayTimeExpression("${delayVar}");

        sleepAction.doExecute(context);
    }

    @DataProvider
    public Object[][] testDataProviderForTestSleepActionWithCorrectValues() {
        return new Object[][]{
                {""},
                {"0"},
                {"1"},
                {"11"},
                {"1ms"},
                {"500ms"},
                {"1s"},
                {"10s"},
                {"1m"},
                {"05ms"},
        };
    }

    @Test(dataProvider = "testDataProviderForTestSleepActionException", expectedExceptions = CitrusExtensionRuntimeException.class)
    public void testSleepActionException(String delayTimeExpression) {
        SleepAction sleepAction = new SleepAction().setDelayTimeExpression(delayTimeExpression)
                .setHandleException(false);

        sleepAction.doExecute(context);
    }

    @Test(dataProvider = "testDataProviderForTestSleepActionException")
    public void testSleepActionExceptionWithEnabledHandleIncorrectExpression(String delayTimeExpression) {
        SleepAction sleepAction = new SleepAction().setDelayTimeExpression(delayTimeExpression)
                .setHandleException(true);

        sleepAction.doExecute(context);
    }

    @DataProvider
    public Object[][] testDataProviderForTestSleepActionException() {
        return new Object[][]{
                {"ms1"},
                {"ms"},
                {"s"},
                {"mm"},
                {"1a"},
                {"1b"},
                {"1c"},
                {"Lorem ipsum"},
                {"0.5s"},
        };
    }

}
