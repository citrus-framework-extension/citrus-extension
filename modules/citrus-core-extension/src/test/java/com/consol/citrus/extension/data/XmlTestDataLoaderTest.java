package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;

public class XmlTestDataLoaderTest extends AbstractTestNGUnitTest {

    @Test
    public void testGetterExtensions() {
        String[] expectedExtensions = {"xml"};
        XmlTestDataLoader xmlTestDataLoader = new XmlTestDataLoader();
        Assert.assertEquals(xmlTestDataLoader.getExtensions(), expectedExtensions);
    }

    @Test
    public void testSetterGetterClass() {
        class SomeClass {
        }
        XmlTestDataLoader xmlTestDataLoader = new XmlTestDataLoader();

        xmlTestDataLoader.setClazz(SomeClass.class);

        Assert.assertEquals(xmlTestDataLoader.getClazz(), SomeClass.class);
    }

    @Test
    public void testSetterGetterPathToXsdSchema() {
        String pathToXsdSchema = "classpath:some-path";
        XmlTestDataLoader xmlTestDataLoader = new XmlTestDataLoader();

        xmlTestDataLoader.setPathToXsdSchema(pathToXsdSchema);

        Assert.assertEquals(xmlTestDataLoader.getPathToXsdSchema(), pathToXsdSchema);
    }

    @Test
    public void testLoadXmlTestDataFiles() {
        int numberOfExpectedLoadedData = 2;
        String testDataPath = "classpath:test-data-xml/";
        XmlTestDataLoader xmlTestDataLoader = new XmlTestDataLoader();
        xmlTestDataLoader.setTestDataPath(testDataPath);
        xmlTestDataLoader.setClazz(TestDataSumCalcXml.class);

        Object[][] testDataArray = xmlTestDataLoader.getTestData();

        Assert.assertNotNull(testDataArray);
        Assert.assertEquals(testDataArray.length, numberOfExpectedLoadedData);
        Arrays.stream(testDataArray)
                .forEach(testDataElement -> {
                    TestDataSumCalcXml testDataSumCalc = (TestDataSumCalcXml) testDataElement[0];

                    int actualSum = testDataSumCalc.getNumber1() + testDataSumCalc.getNumber2();
                    Assert.assertEquals(actualSum, testDataSumCalc.getExpectedResult());
                });
    }

    @Test
    public void testLoadXmlTestDataFilesWithXsd() {
        int numberOfExpectedLoadedData = 2;
        String testDataPath = "classpath:test-data-xml-with-xsd/";
        String pathToXsd = "classpath:test-data-xml-with-xsd/test-data-sum-calc.xsd";
        XmlTestDataLoader xmlTestDataLoader = new XmlTestDataLoader();
        xmlTestDataLoader.setTestDataPath(testDataPath);
        xmlTestDataLoader.setClazz(TestDataSumCalcXmlWithXsd.class);
        xmlTestDataLoader.setPathToXsdSchema(pathToXsd);

        Object[][] testDataArray = xmlTestDataLoader.getTestData();

        Assert.assertNotNull(testDataArray);
        Assert.assertEquals(testDataArray.length, numberOfExpectedLoadedData);
        Arrays.stream(testDataArray)
                .forEach(testDataElement -> {
                    TestDataSumCalcXmlWithXsd testDataSumCalc = (TestDataSumCalcXmlWithXsd) testDataElement[0];

                    int actualSum = testDataSumCalc.getNumber1() + testDataSumCalc.getNumber2();
                    Assert.assertEquals(actualSum, testDataSumCalc.getExpectedResult());
                });
    }

}