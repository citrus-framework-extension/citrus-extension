package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {AbstractTestDataFilterConfig.class})
public class JsonTestDataLoaderWithTestDataFilterTest extends AbstractTestNGUnitTest {

    @Autowired
    private TestDataFilter defaultTestDataFilter;


    @Test
    public void testLoadJsonTestDataFilesAndFilterTestData() {
        String filterExpression = "number1='1'";
        ((AbstractTestDataFilter)defaultTestDataFilter).setFilterExpression(filterExpression);

        int numberOfExpectedLoadedData = 1;
        String testDataPath = "classpath:test-data-json/";
        JsonTestDataLoader testDataLoader = new JsonTestDataLoader(defaultTestDataFilter);
        testDataLoader.setTestDataPath(testDataPath);
        testDataLoader.setClazz(TestDataSumCalc.class);

        Object[][] testDataArray = testDataLoader.getTestData();

        Assert.assertNotNull(testDataArray);
        Assert.assertEquals(testDataArray.length, numberOfExpectedLoadedData);
        Assert.assertEquals(((TestDataSumCalc)testDataArray[0][0]).getNumber1(), 1);
    }

}
