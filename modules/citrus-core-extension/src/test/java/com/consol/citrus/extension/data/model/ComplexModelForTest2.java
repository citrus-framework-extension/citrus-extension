package com.consol.citrus.extension.data.model;

public class ComplexModelForTest2 {

    private int number;

    public ComplexModelForTest2() {
        this(0);
    }

    public ComplexModelForTest2(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public ComplexModelForTest2 setNumber(int number) {
        this.number = number;
        return this;
    }
}
