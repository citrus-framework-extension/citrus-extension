package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

public class JsonTestDataLoaderBuilderTest extends AbstractTestNGUnitTest {

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testJsonTestDataLoaderBuilder() {
        String testDataPath = "classpath:someDirectory";
        boolean recursiveSearch = true;
        ObjectMapper objectMapper = new ObjectMapper();
        JsonTestDataLoaderBuilder jsonTestDataLoaderBuilder = new JsonTestDataLoaderBuilder();

        JsonTestDataLoader testDataLoader = jsonTestDataLoaderBuilder
                .testDataPath(testDataPath)
                .recursiveSearch(recursiveSearch)
                .objectMapper(objectMapper)
                .clazz(TestDataSumCalc.class)
                .build();

        Assert.assertNotNull(testDataLoader);
        Assert.assertEquals(testDataLoader.getTestDataPath(), testDataPath);
        Assert.assertEquals(testDataLoader.isRecursive(), recursiveSearch);
        Assert.assertEquals(testDataLoader.getObjectMapper(), objectMapper);
        Assert.assertEquals(testDataLoader.getClazz(), TestDataSumCalc.class);
    }

}