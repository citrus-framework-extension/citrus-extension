package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class XmlTestDataLoaderBuilderTest extends AbstractTestNGUnitTest {

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testXmlTestDataLoaderBuilder() {
        String testDataPath = "classpath:someDirectory";
        boolean recursiveSearch = true;
        String pathToXsd = "classpath:test-data-xml-with-xsd/test-data-sum-calc.xsd";
        XmlTestDataLoaderBuilder xmlTestDataLoaderBuilder = new XmlTestDataLoaderBuilder();

        XmlTestDataLoader xmlTestDataLoader = xmlTestDataLoaderBuilder
                .testDataPath(testDataPath)
                .recursiveSearch(recursiveSearch)
                .clazz(TestDataSumCalcXml.class)
                .pathToXsdSchema(pathToXsd)
                .build();

        Assert.assertNotNull(xmlTestDataLoader);
        Assert.assertEquals(xmlTestDataLoader.getTestDataPath(), testDataPath);
        Assert.assertEquals(xmlTestDataLoader.isRecursive(), recursiveSearch);
        Assert.assertEquals(xmlTestDataLoader.getClazz(), TestDataSumCalcXml.class);
    }

}