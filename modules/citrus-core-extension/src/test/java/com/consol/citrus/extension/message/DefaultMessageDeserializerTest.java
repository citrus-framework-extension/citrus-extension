package com.consol.citrus.extension.message;

import com.consol.citrus.message.DefaultMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DefaultMessageDeserializerTest extends DefaultMessageDeserializerBaseTest {

    private ObjectMapper objectMapper;

    @BeforeTest
    public void setUpObjectMapper() {
        objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addDeserializer(DefaultMessage.class, new DefaultMessageDeserializer<>());
        objectMapper.registerModule(module);
    }

    @Test
    public void testDefaultMessageDeserializerPayload() throws IOException {
        String message = "{\n" +
                "  \"payload\": \"Some payload\"\n" +
                "}";
        DefaultMessage defaultMessage = objectMapper.readValue(message, DefaultMessage.class);

        assertDefaultMessage(defaultMessage);
        Assert.assertEquals(defaultMessage.getPayload(), "Some payload");
    }

    @Test
    public void testDefaultMessageDeserializerHeaderData() throws IOException {
        String message = "{\n" +
                "  \"headerData\": [\n" +
                "    \"a\",\n" +
                "    \"b\"\n" +
                "  ]\n" +
                "}";

        DefaultMessage defaultMessage = objectMapper.readValue(message, DefaultMessage.class);
        List<String> strings = new ArrayList<String>() {{
            add("a");
            add("b");
        }};

        assertDefaultMessage(defaultMessage);
        Assert.assertEquals(defaultMessage.getHeaderData(), strings);
    }

    @Test
    public void testDefaultMessageDeserializerHeaders() throws IOException {
        String message = "{\n" +
                "  \"headers\": {\n" +
                "    \"h1\": \"v1\",\n" +
                "    \"h2\": \"v2\"\n" +
                "  }\n" +
                "}";

        DefaultMessage defaultMessage = objectMapper.readValue(message, DefaultMessage.class);

        assertDefaultMessage(defaultMessage);
        Assert.assertEquals(defaultMessage.getHeader("h1"), "v1");
        Assert.assertEquals(defaultMessage.getHeader("h2"), "v2");
    }

    @Test
    public void testDefaultMessageDeserializerName() throws IOException {
        String message = "{\n" +
                "    \"name\": \"SomeName\"\n" +
                "}";

        DefaultMessage defaultMessage = objectMapper.readValue(message, DefaultMessage.class);

        assertDefaultMessage(defaultMessage);
        Assert.assertEquals(defaultMessage.getName(), defaultMessage.getName());
    }

}