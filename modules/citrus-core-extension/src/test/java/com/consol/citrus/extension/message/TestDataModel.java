package com.consol.citrus.extension.message;

import com.consol.citrus.message.DefaultMessage;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class TestDataModel {

    @JsonDeserialize(using = DefaultMessageDeserializer.class)
    private DefaultMessage defaultMessageRequest;
    @JsonDeserialize(using = DefaultMessageDeserializer.class)
    private DefaultMessage defaultMessageResponse;

    public TestDataModel() {
        this(new DefaultMessage(), new DefaultMessage());
    }

    public TestDataModel(DefaultMessage defaultMessageRequest, DefaultMessage defaultMessageResponse) {
        this.defaultMessageRequest = defaultMessageRequest;
        this.defaultMessageResponse = defaultMessageResponse;
    }

    public DefaultMessage getDefaultMessageRequest() {
        return defaultMessageRequest;
    }

    public void setDefaultMessageRequest(DefaultMessage defaultMessageRequest) {
        this.defaultMessageRequest = defaultMessageRequest;
    }

    public DefaultMessage getDefaultMessageResponse() {
        return defaultMessageResponse;
    }

    public void setDefaultMessageResponse(DefaultMessage defaultMessageResponse) {
        this.defaultMessageResponse = defaultMessageResponse;
    }

    @Override
    public String toString() {
        return "TestDataModel{" +
                "defaultMessageRequest=" + defaultMessageRequest +
                ", defaultMessageResponse=" + defaultMessageResponse +
                '}';
    }
}
