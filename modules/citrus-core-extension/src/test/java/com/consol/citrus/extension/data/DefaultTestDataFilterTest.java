package com.consol.citrus.extension.data;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@TestPropertySource(locations = {"classpath:abstract-test-data-filter.properties"})
@ContextConfiguration(classes = {AbstractTestDataFilterConfig.class})
public class DefaultTestDataFilterTest extends AbstractTestNGUnitTest {

    @Autowired
    private DefaultTestDataFilter defaultTestDataFilter;

    @Test
    public void testFilterExpressionIsEmpty() {
        List<Object> inputTestData = new ArrayList<Object>() {{
            add(new TestDataSumCalc(1, 2, 3));
        }};

        List<Object> filtratedTestData = defaultTestDataFilter.filterTestData(inputTestData);

        Assert.assertEquals(filtratedTestData.size(), inputTestData.size());
        Assert.assertEquals(filtratedTestData, inputTestData);
    }

    @Test
    public void testNoFiledName() {
        String filterExpressionWithNotExistingFieldNameInObject = "notExistingField='a'";
        List<Object> inputTestData = new ArrayList<Object>() {{
            add(new TestDataSumCalc(1, 2, 3));
        }};
        defaultTestDataFilter.setFilterExpression(filterExpressionWithNotExistingFieldNameInObject);

        List<Object> filtratedTestData = defaultTestDataFilter.filterTestData(inputTestData);

        Assert.assertEquals(filtratedTestData.size(), 0);
        Assert.assertNotEquals(filtratedTestData.size(), inputTestData.size());
        Assert.assertNotEquals(filtratedTestData, inputTestData);
    }

    @Test(dataProvider = "testDataForTestFilterTestData")
    public void testFilterTestData(String filterExpression, List<Object> inputList, List<Object> expectedResultList) {
        defaultTestDataFilter.setFilterExpression(filterExpression);

        List<Object> filteredTestData = defaultTestDataFilter.filterTestData(inputList);

        Assert.assertEquals(filteredTestData.size(), expectedResultList.size());
        Assert.assertEquals(filteredTestData, expectedResultList);
    }

    @DataProvider
    public Object[][] testDataForTestFilterTestData() {
        return new Object[][]{
                {"number1='1'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                        }},
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                        }}
                },
                {"number1='1'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                        }}
                },
                {"number1='1' AND number2='2'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                        }}
                },
                {"number1='1' OR number2='2'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }}
                },
                {"number1='9' AND number2='9'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>()
                },
                {"number1='9' OR number2='9'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>()
                },
                {"",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                },
                {"number1='[1|2]'",
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }},
                        new ArrayList<TestDataSumCalc>() {{
                            add(new TestDataSumCalc(1, 2, 3));
                            add(new TestDataSumCalc(2, 2, 4));
                        }}
                },
        };
    }


}
