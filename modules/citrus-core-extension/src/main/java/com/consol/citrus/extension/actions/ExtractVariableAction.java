package com.consol.citrus.extension.actions;

import com.consol.citrus.actions.AbstractTestAction;
import com.consol.citrus.context.TestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * This test action allows to extract variable from Citrus Framework.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class ExtractVariableAction extends AbstractTestAction {

    /**
     * Logger
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Name of variable to extract.
     */
    private String variableName = "";

    /**
     * Extracted variable form Citrus Framework.
     */
    private Object extractedVariable;

    /**
     * Default constructor.
     */
    public ExtractVariableAction() {
        setName("extract-variable");
    }

    @Override
    public void doExecute(TestContext context) {
        if (context.getVariables().size() > 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("Declared variables:");
                for (Map.Entry<String, Object> variableEntry : context.getVariables().entrySet()) {
                    logger.debug("Variable key '" + variableEntry.getKey() + "' value '" + variableEntry.getValue() + "'");
                }
            }

            extractedVariable = context.getVariable(variableName);
            if (extractedVariable != null) {
                logger.info("Variable '" + variableName + "' found, and was extracted with value '" + extractedVariable + "'");
            }
        } else {
            logger.debug("No variables were declared. Nothing to extract");
        }
    }

    /**
     * Sets name for variable to extract.
     *
     * @param variableName Name of variable to extract.
     * @return Returns instance of this obejct.
     */
    @SuppressWarnings("UnusedReturnValue")
    public ExtractVariableAction setVariableName(String variableName) {
        this.variableName = variableName;
        return this;
    }

    /**
     * Returns value of extracted variable from Citrus Framework.
     *
     * @return Value of extracted variable.
     */
    public Object getExtractedVariable() {
        return extractedVariable;
    }
}
