package com.consol.citrus.extension.data;

/**
 * Interface for loading test data.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public interface TestDataLoader {

    /**
     * Gets test data.
     *
     * @return Returns test data as a two-dimensional array.
     */
    Object[][] getTestData();

}
