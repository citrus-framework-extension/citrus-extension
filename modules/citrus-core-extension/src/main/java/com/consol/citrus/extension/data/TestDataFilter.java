package com.consol.citrus.extension.data;

import java.util.List;

/**
 * Interface for filtering test data.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.3
 */
public interface TestDataFilter {

    /**
     * Filter test data and returns data which match to the filter.
     *
     * @param testData Test data to filter.
     * @return Filtered test data.
     */
    List<Object> filterTestData(List<Object> testData);

}
