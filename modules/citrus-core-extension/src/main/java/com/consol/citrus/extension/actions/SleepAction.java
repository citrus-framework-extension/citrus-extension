package com.consol.citrus.extension.actions;

import com.consol.citrus.actions.AbstractTestAction;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.extension.exceptions.CitrusExtensionRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * Stop the test execution for a given amount of time.
 * Stop time can be provided with the unit of wait.
 * Example:
 * <p><ul>
 * <li>5ms - 5 milliseconds</li>
 * <li>5s - 5 seconds</li>
 * <li>5m - 5 minutes</li>
 * <li>5h - 5 hours ;)</li>
 * <li>5d - 5 days O_O</li>
 * <li>5M - 5 months ಠ_ಠ</li>
 * <li>5y - 5 years ¯\_(ツ)_/¯</li>
 * </ul><p>
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class SleepAction extends AbstractTestAction {

    private static final Logger logger = LoggerFactory.getLogger(SleepAction.class);

    private static final String REGEX = "(\\d+)(ms|s|m|h|d|M|y)?";

    private static final String DEFAULT_DELAY_TIME_EXPRESSION = "0ms";

    /**
     * Delay time
     */
    private String delayTimeExpression = "0ms";
    /**
     * Tells if to handle incorrect delay time expression.
     */
    private boolean handleException = false;

    @Override
    public void doExecute(TestContext context) {
        try {
            String delayTimeExpression;

            if (StringUtils.hasText(this.delayTimeExpression)) {
                delayTimeExpression = context.resolveDynamicValue(this.delayTimeExpression);
                if (StringUtils.isEmpty(delayTimeExpression)) {
                    delayTimeExpression = DEFAULT_DELAY_TIME_EXPRESSION;
                }
            } else {
                delayTimeExpression = DEFAULT_DELAY_TIME_EXPRESSION;
            }

            if (!delayTimeExpression.matches(REGEX)) {
                throw new CitrusExtensionRuntimeException("Delay expression has been provided in incorrect format '" + delayTimeExpression + "'");
            }

            String delay = delayTimeExpression.replaceAll("\\D", "");
            String unit = delayTimeExpression.replaceAll(delay, "");

            Unit enumUnit = Unit.find(unit);
            long calculatedDelayTime = Long.parseLong(delay) * enumUnit.getMultiplier();

            logger.info(String.format("Sleeping %s %s", delay, enumUnit.unit));
            Thread.sleep(calculatedDelayTime);
            logger.info(String.format("Returning after %s %s", delay, enumUnit.unit));

        } catch (Exception exception) {
            if (handleException) {
                logger.warn("Error detected ", exception);
                logger.warn("Skipping sleep action");
            } else {
                throw new CitrusExtensionRuntimeException(exception);
            }
        }
    }

    public SleepAction setDelayTimeExpression(String delayTimeExpression) {
        this.delayTimeExpression = delayTimeExpression;
        return this;
    }

    public SleepAction setHandleException(boolean handleException) {
        this.handleException = handleException;
        return this;
    }

    private enum Unit {

        MS_UNIT("ms", 1),
        S_UNIT("s", 1000),
        M_UNIT("m", 60000),
        H_UNIT("h", 3600000),
        D_UNIT("d", 86400000),
        MONTH_UNIT("M", 2629800000L),
        Y_UNIT("y", 31556952000L);

        Unit(String unit, long multiplier) {
            this.unit = unit;
            this.multiplier = multiplier;
        }

        private final String unit;
        private final long multiplier;

        public String getUnit() {
            return unit;
        }

        public long getMultiplier() {
            return multiplier;
        }

        public static Unit find(String value) {
            for (Unit unit : Unit.values()) {
                if (unit.unit.matches(value))
                    return unit;
            }
            return MS_UNIT;
        }

    }

}
