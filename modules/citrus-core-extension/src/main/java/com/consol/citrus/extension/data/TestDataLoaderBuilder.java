package com.consol.citrus.extension.data;

/**
 * It builds test data loader.
 *
 * @param <T> Type of test data loader.
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public interface TestDataLoaderBuilder<T extends TestDataLoader> {

    /**
     * It builds test data loader.
     *
     * @return Test data loader.
     */
    T build();

}
