package com.consol.citrus.extension.exceptions;

import com.consol.citrus.exceptions.CitrusRuntimeException;

/**
 * Basic custom runtime exception for all errors in Citrus Extension.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.3
 */
public class CitrusExtensionRuntimeException extends CitrusRuntimeException {

    /**
     * Default constructor.
     */
    public CitrusExtensionRuntimeException() {
    }

    /**
     * Constructor using fields.
     *
     * @param message Runtime exception message
     */
    public CitrusExtensionRuntimeException(String message) {
        super(message);
    }

    /**
     * Constructor using fields.
     *
     * @param cause Runtime exception cause
     */
    public CitrusExtensionRuntimeException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor using fields.
     *
     * @param message Runtime exception message
     * @param cause   Runtime exception cause
     */
    public CitrusExtensionRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
