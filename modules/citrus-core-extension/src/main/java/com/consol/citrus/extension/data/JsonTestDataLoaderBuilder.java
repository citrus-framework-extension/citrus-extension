package com.consol.citrus.extension.data;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Test data loader builder for Json files.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class JsonTestDataLoaderBuilder extends AbstractTestDataLoaderBuilder<JsonTestDataLoaderBuilder, JsonTestDataLoader> {

    /**
     * Default constructor.
     */
    protected JsonTestDataLoaderBuilder() {
        super(new JsonTestDataLoader());
    }

    /**
     * JsonTestDataLoaderBuilder with test data filter.
     *
     * @param testDataFilter Test data filter.
     */
    public JsonTestDataLoaderBuilder(TestDataFilter testDataFilter) {
        super(new JsonTestDataLoader(testDataFilter));
    }


    /**
     * Sets object mapper.
     *
     * @param objectMapper Object mapper
     * @return Return builder instance.
     */
    public JsonTestDataLoaderBuilder objectMapper(ObjectMapper objectMapper) {
        getTestDataLoader().setObjectMapper(objectMapper);
        return this;
    }

}
