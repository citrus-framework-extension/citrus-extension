package com.consol.citrus.extension.data;

/**
 * Abstract class for test data loader builders.
 *
 * @param <B> Test data loader builder type.
 * @param <T> Test data loader type.
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
@SuppressWarnings("unchecked")
public abstract class AbstractTestDataLoaderBuilder
        <B extends AbstractTestDataLoaderBuilder<B, T>, T extends AbstractTestDataLoader>
        implements TestDataLoaderBuilder<T> {

    /**
     * Test data loader.
     */
    private final T testDataLoader;

    /**
     * Default constructor.
     *
     * @param testDataLoader Test data loader.
     */
    protected AbstractTestDataLoaderBuilder(T testDataLoader) {
        this.testDataLoader = testDataLoader;
    }

    /**
     * Sets path to test data.
     * Examples of correct paths:
     * <p><ul>
     * <li>Must support fully qualified URLs, e.g. "file:C:/some-folder".
     * <li>Must support classpath pseudo-URLs, e.g. "classpath:some-folder".
     * </ul>
     *
     * @param testDataPath Path to test data.
     * @return Return builder instance.
     */
    public B testDataPath(String testDataPath) {
        testDataLoader.setTestDataPath(testDataPath);
        return (B) this;
    }

    /**
     * It allows to decide if search for test data to load in subdirectories.
     *
     * @param recursive true - if should search in sub-folders and false if not.
     * @return Return builder instance.
     */
    public B recursiveSearch(boolean recursive) {
        testDataLoader.setRecursive(recursive);
        return (B) this;
    }

    /**
     * Sets class which represent test data model.
     *
     * @param clazz Class of test data model.
     * @return Return builder instance.
     */
    public B clazz(Class<?> clazz) {
        getTestDataLoader().setClazz(clazz);
        return (B) this;
    }

    /**
     * Returns test data loader.
     *
     * @return Test data loader.
     */
    protected T getTestDataLoader() {
        return testDataLoader;
    }

    @Override
    public T build() {
        return testDataLoader;
    }
}
