package com.consol.citrus.extension.message;

import com.consol.citrus.message.DefaultMessage;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Base deserializer for citrus DefaultMessage.
 *
 * @param <T>
 * @author Mariusz Marciniuk
 * @since 0.0.2
 */
public class DefaultMessageDeserializer<T extends DefaultMessage> extends StdDeserializer<T> {

    public static final String NAME_OF_FIELD_PAYLOAD = "payload";
    public static final String NAME_OF_FIELD_HEADER_DATA = "headerData";
    public static final String NAME_OF_FIELD_HEADERS = "headers";
    public static final String NAME_OF_FIELD_NAME = "name";

    /**
     * JsonNode can by used by classes which extends this class.
     * To use this variable you will need to call 'deserialize' method form this class.
     */
    protected JsonNode jsonNode;

    /**
     * Default constructor.
     */
    public DefaultMessageDeserializer() {
        this(null);
    }

    /**
     * Constructor with Class parameter.
     *
     * @param vc Class
     */
    public DefaultMessageDeserializer(Class<?> vc) {
        super(vc);
    }

    @SuppressWarnings("unchecked")
    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        jsonNode = p.getCodec().readTree(p);
        DefaultMessage defaultMessage = new DefaultMessage();

        if (jsonNode.get(NAME_OF_FIELD_PAYLOAD) != null) {
            if (jsonNode.get(NAME_OF_FIELD_PAYLOAD).isTextual()) {
                String payload = jsonNode.get(NAME_OF_FIELD_PAYLOAD).asText();
                defaultMessage.setPayload(payload);
            } else {
                Object payload = jsonNode.get(NAME_OF_FIELD_PAYLOAD).asText();
                defaultMessage.setPayload(payload);
            }
        }

        if (jsonNode.get(NAME_OF_FIELD_HEADER_DATA) != null) {
            if (jsonNode.get(NAME_OF_FIELD_HEADER_DATA).isArray()) {
                ArrayNode arrayNode = (ArrayNode) jsonNode.get(NAME_OF_FIELD_HEADER_DATA);
                Iterator<JsonNode> jsonNodeIterator = arrayNode.elements();
                while (jsonNodeIterator.hasNext()) {
                    JsonNode jsonNodeHeaderData = jsonNodeIterator.next();
                    defaultMessage.addHeaderData(jsonNodeHeaderData.asText());
                }
            }
        }

        if (jsonNode.get(NAME_OF_FIELD_HEADERS) != null) {
            ObjectNode headersObj = (ObjectNode) jsonNode.get(NAME_OF_FIELD_HEADERS);
            Iterator<Map.Entry<String, JsonNode>> jsonNodeHeader = headersObj.fields();

            jsonNodeHeader.forEachRemaining(entry -> {
                Object objectValue = entry.getValue().asText();
                defaultMessage.setHeader(entry.getKey(), objectValue);
            });
        }

        if (jsonNode.get(NAME_OF_FIELD_NAME) != null) {
            if (jsonNode.get(NAME_OF_FIELD_NAME).isTextual()) {
                defaultMessage.setName(jsonNode.get(NAME_OF_FIELD_NAME).asText());
            }
        }

        return (T) defaultMessage;
    }
}
