package com.consol.citrus.extension.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.core.env.Environment;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.consol.citrus.extension.data.FilterOperator.*;

/**
 * Abstract base implementation of the TestDataFilter
 *
 * @author Mariusz Marciniuk
 * @since 0.0.3
 */
public abstract class AbstractTestDataFilter implements TestDataFilter, BeanNameAware {

    /**
     * Property name prefix.
     */
    public static final String PROP_PREFIX = "filter";
    /**
     * Getter prefix.
     */
    protected static final String GETTER_PREFIX = "get";
    /**
     * Not found
     */
    protected static final String NOT_FOUND = "not-found";
    /**
     * Regex of correct filter expression.
     */
    protected static final String REGEX_FILTER_EXPRESSION_PATTERN = "(.*)=('[a-zA-Z0-9_.-]*'(\\s(" + AND + "|" + OR + ")\\s)*)";
    /**
     * Logger
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * Name of the test data filter. This name will be used to find filtering rules in properties for this test data filter.
     */
    private String name;
    /**
     * Environment
     */
    private final Environment environment;
    /**
     * Filter expression
     */
    private String filterExpression = "";


    /**
     * Default constructor
     */
    protected AbstractTestDataFilter(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setBeanName(@SuppressWarnings("NullableProblems") String name) {
        this.name = name;
    }

    /**
     * Returns name of the test data filter.
     *
     * @return Name of the test data filter.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns filter expression
     *
     * @return Filter expression
     */
    public String getFilterExpression() {
        return filterExpression;
    }

    /**
     * Sets filter expression
     *
     * @param filterExpression Filter expression
     */
    public void setFilterExpression(String filterExpression) {
        this.filterExpression = filterExpression;
    }

    protected String findFilterExpression() {
        return environment.getProperty(PROP_PREFIX + "." + name);
    }

    public String getFullFilterName() {
        return PROP_PREFIX + "." + name;
    }

    /**
     * Checks if expressions is correct.
     *
     * @param filterExpression Filter expression
     * @return Returns true if expression is correct.
     */
    protected final boolean isExpressionCorrect(final String filterExpression) {
        if (filterExpression == null)
            return false;
        Pattern pattern = Pattern.compile(REGEX_FILTER_EXPRESSION_PATTERN);
        Matcher matcher = pattern.matcher(filterExpression);
        return matcher.matches();
    }

    /**
     * Method separates filter expression into single operations.
     *
     * @param filterExpression Filter expression
     * @return List of operations
     */
    protected final List<FilterOperation> operations(final String filterExpression) {
        List<FilterOperation> filterOperationList = new ArrayList<>();

        String[] splitFilterExpression = filterExpression.split(" ");
        if (splitFilterExpression.length == 1) {
            filterOperationList.add(new FilterOperation(splitFilterExpression[0], NONE));
        } else {
            for (int i = 0; i < splitFilterExpression.length; i = i + 2) {
                String expression = splitFilterExpression[i];
                if (i + 1 < splitFilterExpression.length) {
                    if ((splitFilterExpression[i + 1].equals(AND.toString()) || splitFilterExpression[i + 1].equals(OR.toString()))) {
                        FilterOperator filterOperator = FilterOperator.valueOf(splitFilterExpression[i + 1]);
                        filterOperationList.add(new FilterOperation(expression, filterOperator));
                    }
                } else {
                    filterOperationList.add(new FilterOperation(expression, NONE));
                }
            }
        }
        return filterOperationList;
    }

    /**
     * Method get the result from the object instance base on the filterExpression.
     *
     * @param filterExpression Expression
     * @param object           Instance object with test data to check
     * @return Returns the result from the getter method from the object instance.
     */
    protected final String getResultFromObject(final String filterExpression, final Object object) {
        if (object == null) {
            return "null";
        }

        String filterExpressionFields = filterExpression.split("=")[0];
        String[] filterExpressionFieldsArray = filterExpressionFields.split("[.]");
        String filterExpressionField = filterExpressionFieldsArray[0];

        filterExpressionField = filterExpressionField.substring(0, 1).toUpperCase() + filterExpressionField.substring(1);
        String getterMethodName = GETTER_PREFIX + filterExpressionField;

        for (Method method : object.getClass().getMethods()) {
            if (method.getName().equals(getterMethodName)) {
                Object resultOfMethod = null;
                try {
                    resultOfMethod = method.invoke(object);
                } catch (IllegalAccessException | InvocationTargetException ignored) {
                }

                if (filterExpressionFieldsArray.length <= 1) {
                    return String.valueOf(resultOfMethod);
                } else {
                    String newFilterExpression = filterExpression.replace(filterExpressionFieldsArray[0] + ".", "");
                    return getResultFromObject(newFilterExpression, resultOfMethod);
                }
            }
        }

        return NOT_FOUND;
    }

}
