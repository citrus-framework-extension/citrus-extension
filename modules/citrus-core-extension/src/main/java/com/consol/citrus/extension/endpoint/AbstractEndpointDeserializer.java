package com.consol.citrus.extension.endpoint;

import com.consol.citrus.endpoint.AbstractEndpoint;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * Base abstract endpoint deserializer for Citrus.
 *
 * @param <E>
 * @author Mariusz Marciniuk
 * @since 0.0.14
 */
public abstract class AbstractEndpointDeserializer<E extends AbstractEndpoint> extends StdDeserializer<E> {

    protected static final String NAME_OF_FIELD_NAME = "name";
    protected static final String NAME_OF_FIELD_ENDPOINT_CONFIGURATION = "endpointConfiguration";

    /**
     * Default constructor.
     */
    protected AbstractEndpointDeserializer() {
        this(null);
    }

    /**
     * Constructor with Class parameter.
     *
     * @param vc Class
     */
    protected AbstractEndpointDeserializer(Class<?> vc) {
        super(vc);
    }

}
