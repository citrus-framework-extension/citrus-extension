package com.consol.citrus.extension.data;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Implementation of the test data loader for XML files.
 * It allows to load test data from XML files to model class of the test data.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class XmlTestDataLoader extends AbstractTestDataLoader {

    /**
     * Path to XSD schema.
     * When this path is equals null validation against XSD will not be performed.
     */
    private String pathToXsdSchema;

    /**
     * Default constructor
     */
    public XmlTestDataLoader() {
        super(new String[]{"xml"});
    }

    public XmlTestDataLoader(TestDataFilter testDataFilter) {
        super(new String[]{"xml"}, testDataFilter);
    }

    @Override
    protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) throws IOException, SAXException, JAXBException {
        if (canExecuteValidationAgainstXsd())
            validateAgainstXSD(new FileInputStream(testDataFile));

        JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Object object = unmarshaller.unmarshal(testDataFile);
        testDataList.add(object);
    }

    private boolean canExecuteValidationAgainstXsd() {
        return pathToXsdSchema != null;
    }

    /**
     * Method check whether XML file match to XSD schema.
     *
     * @param xml XML file
     * @throws IOException  Will be thrown when XSD or XML couldn't be loaded.
     * @throws SAXException Will be thrown when validation exception occur.
     */
    private void validateAgainstXSD(InputStream xml) throws IOException, SAXException {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(pathToXsdSchema);

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(resource.getFile());
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(xml));
    }

    /**
     * Returns path to XSD schema.
     *
     * @return Path to XSD schema.
     */
    String getPathToXsdSchema() {
        return pathToXsdSchema;
    }

    /**
     * Sets path to XSD schema.
     * When this path is equals null validation against XSD will not be performed.
     * Example of correct paths:
     * <p><lu>
     * <li>Must support fully qualified URLs, e.g. "file:C:/test.xsd".
     * <li>Must support classpath pseudo-URLs, e.g. "classpath:test.xsd".
     * </lu></p>
     *
     * @param pathToXsdSchema Path to XSD schema.
     */
    void setPathToXsdSchema(String pathToXsdSchema) {
        this.pathToXsdSchema = pathToXsdSchema;
    }
}
