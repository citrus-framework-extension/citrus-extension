package com.consol.citrus.extension.data;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Abstract class for test data loaders.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public abstract class AbstractTestDataLoader implements TestDataLoader {

    /**
     * Logger
     */
    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Default directory for the test data.
     */
    protected String testDataPath = "classpath:test-data/";

    /**
     * An array of extensions, ex. {"java","xml"}. If this parameter is {@code null}, all files are returned.
     */
    protected final String[] extensions;

    /**
     * It allows to decide if search for test data to load in subdirectories.
     */
    protected boolean recursive = false;

    /**
     * Class which represent test data model.
     */
    protected Class<?> clazz = Object.class;
    /**
     * Test data filter.
     */
    protected TestDataFilter testDataFilter;

    /**
     * Default constructor.
     */
    protected AbstractTestDataLoader() {
        this(null);
    }

    /**
     * Default constructor.
     *
     * @param extensions Extensions
     */
    protected AbstractTestDataLoader(String[] extensions) {
        this.extensions = extensions;
    }

    protected AbstractTestDataLoader(String[] extensions, TestDataFilter testDataFilter) {
        this(extensions);
        this.testDataFilter = testDataFilter;
    }

    @Override
    public Object[][] getTestData() {
        final List<Object> testDataList = new ArrayList<>();
        Collection<File> filesWithTestData = findFilesToLoad();

        filesWithTestData.forEach(testDataFile -> {
            try {
                addFileToTestDataList(testDataList, testDataFile);
            } catch (Exception exception) {
                String exMsg = String.format("Error occurred during loading test data from file '%s'", testDataFile.getAbsolutePath());
                logger.warn(exMsg, exception);
            }
        });

        if (logger.isDebugEnabled()) {
            logger.debug("Number of loaded test data '" + testDataList.size() + "'");
        }

        List<Object> filtratedTestData = filterTestData(testDataList);

        return listToTwoDimensionalArray(filtratedTestData);
    }

    /**
     * Method list all test data files to load.
     *
     * @return Collection of test data files to load.
     */
    protected Collection<File> findFilesToLoad() {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource(testDataPath);
        String path;
        try {
            path = resource.getURI().getPath();
            File fileOrDirectory = new File(path);
            Collection<File> files = new ArrayList<>();
            if (fileOrDirectory.isFile()) {
                if (fileOrDirectory.exists()) {
                    if (FilenameUtils.getExtension(fileOrDirectory.getName()).equals(extensions[0])) {
                        files.add(fileOrDirectory);
                    }
                }
            } else {
                files = FileUtils.listFiles(fileOrDirectory, extensions, recursive);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(files.size() + " files found with matching extensions '" + Arrays.toString(extensions) + "'");
            }

            return files;
        } catch (IOException exception) {
            logger.error("Error occurred during getting path. No test data might be loaded", exception);
        }
        return Collections.emptyList();
    }

    /**
     * Add file to test data list.
     *
     * @param testDataList Test data list.
     * @param testDataFile File with test data with should match model class.
     * @throws Exception Method might throw exception loading data from file failed.
     */
    protected abstract void addFileToTestDataList(List<Object> testDataList, File testDataFile) throws Exception;

    /**
     * Method converts <i>List</i> into two dimensional array.
     *
     * @param testDataList List of test data.
     * @return Two dimensional array with test data.
     */
    protected Object[][] listToTwoDimensionalArray(List<Object> testDataList) {
        Object[][] testDataArray = new Object[testDataList.size()][1];
        for (int i = 0; i < testDataList.size(); i++) {
            Object object = testDataList.get(i);
            testDataArray[i][0] = object;
        }
        return testDataArray;
    }

    /**
     * Returns path to test data.
     *
     * @return Path to test data.
     */
    String getTestDataPath() {
        return testDataPath;
    }

    /**
     * Sets path to test data.
     * Example of correct paths:
     * <p><lu>
     * <li>Must support fully qualified URLs, e.g. "file:C:/some-folder".
     * <li>Must support classpath pseudo-URLs, e.g. "classpath:some-folder".
     * </lu></p>
     *
     * @param testDataPath Path to test data.
     */
    void setTestDataPath(String testDataPath) {
        this.testDataPath = testDataPath;
    }

    /**
     * Returns array of extensions.
     *
     * @return Extensions of the test data.
     */
    String[] getExtensions() {
        return extensions;
    }

    /**
     * Returns information if it will be checking recursively for test data or not.
     *
     * @return true - if should search in sub-folders and false if not.
     */
    boolean isRecursive() {
        return recursive;
    }

    /**
     * Sets recursive parameter.
     *
     * @param recursive Recursive
     */
    void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }

    /**
     * Returns class with represent test data model.
     *
     * @return Class of test data model.
     */
    Class<?> getClazz() {
        return clazz;
    }

    /**
     * Sets class which represent test data model.
     *
     * @param clazz Class of test data model.
     */
    void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    /**
     * This method filter test data.
     *
     * @param testDataList Test data list.
     * @return Filtrated test data.
     */
    private List<Object> filterTestData(List<Object> testDataList) {
        if (testDataFilter != null) {
            testDataList = testDataFilter.filterTestData(testDataList);
            if (logger.isDebugEnabled()) {
                logger.debug("Number of loaded test data after filtration '" + testDataList.size() + "'");
            }
        } else {
            logger.warn("Test data filter is 'null'. Filtration not executed");
        }
        return testDataList;
    }

}
