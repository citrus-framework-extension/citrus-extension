package com.consol.citrus.extension.data;

import java.util.Objects;

public class FilterOperation {

    private final String expression;
    private final FilterOperator filterOperator;

    public FilterOperation(String expression, FilterOperator filterOperator) {
        this.expression = expression;
        this.filterOperator = filterOperator;
    }

    public String getExpression() {
        return expression;
    }

    public FilterOperator getFilterOperator() {
        return filterOperator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof FilterOperation))
            return false;

        FilterOperation otherFilterOperation = (FilterOperation) o;
        return this.expression.equals(otherFilterOperation.expression) &&
                this.filterOperator.equals(otherFilterOperation.filterOperator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(expression, filterOperator);
    }

    @Override
    public String toString() {
        return "FilterOperation{" +
                "expression='" + expression + '\'' +
                ", filterOperator=" + filterOperator +
                '}';
    }
}
