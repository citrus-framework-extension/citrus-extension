package com.consol.citrus.extension.data;

import com.consol.citrus.extension.exceptions.CitrusExtensionRuntimeException;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * DefaultTestDataFilter
 *
 * @author Mariusz Marciniuk
 * @since 0.0.3
 */
public class DefaultTestDataFilter extends AbstractTestDataFilter {
    /**
     * Default constructor
     *
     * @param environment Environment
     */
    public DefaultTestDataFilter(Environment environment) {
        super(environment);
    }

    @Override
    public List<Object> filterTestData(List<Object> testData) {
        if (filterExpressionIsEmpty()) {
            setFilterExpression(findFilterExpression());
        }

        if (filterExpressionIsEmpty()) {
            logger.warn("No filter was found for '" + getFullFilterName() + "' no filtration was executed");
            return testData;
        } else {
            List<Object> filteredTestData = new ArrayList<>();

            try {
                List<FilterOperation> filterOperationList = operations(getFilterExpression());
                for (Object testDataElement : testData) {
                    boolean canBeAdded = true;
                    for (FilterOperation filterOperation : filterOperationList) {
                        String expectedFieldValue = filterOperation.getExpression().split("=")[1];
                        expectedFieldValue = expectedFieldValue.replace("'", "");

                        String fieldValue = getResultFromObject(filterOperation.getExpression(), testDataElement);

                        Pattern pattern = Pattern.compile(expectedFieldValue);
                        Matcher matcher = pattern.matcher(fieldValue);
                        boolean currentResult = matcher.matches();

                        canBeAdded = canBeAdded(canBeAdded, currentResult, filterOperation.getFilterOperator());
                        if (filterOperation.getFilterOperator() == FilterOperator.NONE) {
                            break;
                        }
                    }

                    if (canBeAdded) {
                        filteredTestData.add(testDataElement);
                    } else {
                        logger.warn("Test data element '" + testDataElement + "' not added to the result");
                    }
                }

            } catch (CitrusExtensionRuntimeException exception) {
                logger.warn("Test data not filtered", exception);
            }

            return filteredTestData;
        }
    }

    private boolean canBeAdded(boolean canBeAdded, boolean currentResult, FilterOperator filterOperator) {
        switch (filterOperator) {
            case NONE:
            case AND:
                return canBeAdded && currentResult;
            case OR:
                return canBeAdded || currentResult;
        }
        return false;
    }

    private boolean filterExpressionIsEmpty() {
        return StringUtils.isEmpty(getFilterExpression());
    }

}
