package com.consol.citrus.extension.data;

/**
 * Enum which contains list of operators for TestDataFilter.
 */
public enum FilterOperator {

    AND("AND"), OR("OR"), NONE("NONE");

    private final String operationName;

    FilterOperator(String operationName) {
        this.operationName = operationName;
    }

    @Override
    public String toString() {
        return operationName;
    }
}
