package com.consol.citrus.extension.data;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.List;

/**
 * Implementation of test data loader for json files.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class JsonTestDataLoader extends AbstractTestDataLoader {

    /**
     * Object mapper from json to Java class.
     */
    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Default constructor
     */
    public JsonTestDataLoader() {
        super(new String[]{"json"});
    }

    public JsonTestDataLoader(TestDataFilter testDataFilter) {
        super(new String[]{"json"}, testDataFilter);
    }

    @Override
    protected void addFileToTestDataList(List<Object> testDataList, File testDataFile) throws Exception {
        Object loadedTestData = objectMapper.readValue(testDataFile, clazz);
        testDataList.add(loadedTestData);
    }

    /**
     * Returns object mapper.
     *
     * @return Object mapper
     */
    ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    /**
     * Sets object mapper.
     *
     * @param objectMapper Object mapper
     */
    void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

}
