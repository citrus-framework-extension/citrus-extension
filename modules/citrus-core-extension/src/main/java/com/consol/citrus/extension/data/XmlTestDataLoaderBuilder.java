package com.consol.citrus.extension.data;

/**
 * Test data loader builder for XML files.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class XmlTestDataLoaderBuilder extends AbstractTestDataLoaderBuilder<XmlTestDataLoaderBuilder, XmlTestDataLoader> {

    /**
     * Default constructor.
     */
    protected XmlTestDataLoaderBuilder() {
        super(new XmlTestDataLoader());
    }

    /**
     * XmlTestDataLoaderBuilder with test data filter.
     *
     * @param testDataFilter Test data filter.
     */
    public XmlTestDataLoaderBuilder(TestDataFilter testDataFilter) {
        super(new XmlTestDataLoader(testDataFilter));
    }

    /**
     * Sets path to XSD schema.
     * When this path is equals null validation against XSD will not be performed.
     *
     * @param pathToXsdSchema Path to XSD schema.
     * @return Return builder instance.
     */
    public XmlTestDataLoaderBuilder pathToXsdSchema(String pathToXsdSchema) {
        getTestDataLoader().setPathToXsdSchema(pathToXsdSchema);
        return this;
    }

}
