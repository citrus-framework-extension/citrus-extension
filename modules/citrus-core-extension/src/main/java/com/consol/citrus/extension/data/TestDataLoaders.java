package com.consol.citrus.extension.data;

/**
 * Test data loaders.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public abstract class TestDataLoaders {

    /**
     * Prevents of creating instance.
     */
    private TestDataLoaders() {
    }

    /**
     * JsonTestDataLoaderBuilder
     *
     * @return JsonTestDataLoaderBuilder
     */
    public static JsonTestDataLoaderBuilder json() {
        return new JsonTestDataLoaderBuilder();
    }

    /**
     * JsonTestDataLoaderBuilder with filter.
     *
     * @param testDataFilter Test data filter.
     * @return JsonTestDataLoaderBuilder
     */
    public static JsonTestDataLoaderBuilder json(TestDataFilter testDataFilter) {
        return new JsonTestDataLoaderBuilder(testDataFilter);
    }

    /**
     * XmlTestDataLoaderBuilder
     *
     * @return XmlTestDataLoaderBuilder
     */
    public static XmlTestDataLoaderBuilder xml() {
        return new XmlTestDataLoaderBuilder();
    }

    /**
     * XmlTestDataLoaderBuilder with filter.
     *
     * @param testDataFilter Test data filter.
     * @return XmlTestDataLoaderBuilder
     */
    public static XmlTestDataLoaderBuilder xml(TestDataFilter testDataFilter) {
        return new XmlTestDataLoaderBuilder(testDataFilter);
    }

}
