package com.consol.citrus.extension.http.message;

import com.consol.citrus.extension.message.DefaultMessageDeserializer;
import com.consol.citrus.http.message.HttpMessage;
import com.consol.citrus.message.DefaultMessage;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Deserializer for citrus HttpMessage.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.2
 */
public class HttpMessageDeserializer extends DefaultMessageDeserializer<HttpMessage> {

    public static final String NAME_OF_FIELD_QUERY_PARAMS = "queryParams";
    public static final String NAME_OF_FIELD_COOKIES = "cookies";

    /**
     * Default constructor.
     */
    public HttpMessageDeserializer() {
        this(null);
    }

    /**
     * Constructor with Class parameter.
     *
     * @param vc Class
     */
    public HttpMessageDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public HttpMessage deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        DefaultMessage defaultMessage = super.deserialize(p, ctxt);
        HttpMessage httpMessage = new HttpMessage(defaultMessage);

        if (jsonNode.get(NAME_OF_FIELD_QUERY_PARAMS) != null) {
            ObjectNode queryParams = (ObjectNode) jsonNode.get(NAME_OF_FIELD_QUERY_PARAMS);
            Iterator<Map.Entry<String, JsonNode>> queryParamsIterator = queryParams.fields();

            queryParamsIterator.forEachRemaining(entry -> httpMessage.queryParam(entry.getKey(), entry.getValue().asText()));
        }

        if (jsonNode.get(NAME_OF_FIELD_COOKIES) != null) {
            ObjectNode cookies = (ObjectNode) jsonNode.get(NAME_OF_FIELD_COOKIES);
            Iterator<Map.Entry<String, JsonNode>> cookiesFields = cookies.fields();

            cookiesFields.forEachRemaining(entry -> {
                String cookieName = entry.getKey();
                String cookieValue = entry.getValue().asText();
                httpMessage.cookie(new Cookie(cookieName, cookieValue));
            });
        }

        return httpMessage;
    }
}
