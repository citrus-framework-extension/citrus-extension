package com.consol.citrus.extension.http.endpoint;

import com.consol.citrus.extension.endpoint.AbstractEndpointDeserializer;
import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.http.client.HttpEndpointConfiguration;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * HttpClient endpoint deserializer for Citrus.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.14
 */
public class HttpEndpointDeserializer extends AbstractEndpointDeserializer<HttpClient> {

    protected static final String NAME_OF_FIELD_REQUEST_URL = "requestUrl";
    protected static final String NAME_OF_FIELD_REQUEST_METHOD = "requestMethod";
    protected static final String NAME_OF_FIELD_CHARSET = "charset";
    protected static final String NAME_OF_FIELD_CONTENT_TYPE = "contentType";
    protected static final String NAME_OF_FIELD_DEFAULT_ACCEPT_HEADER = "defaultAcceptHeader";
    protected static final String NAME_OF_FIELD_HANDLE_ATTRIBUTE_HEADERS = "handleAttributeHeaders";
    protected static final String NAME_OF_FIELD_HANDLE_COOKIES = "handleCookies";
    protected static final String NAME_OF_FIELD_DEFAULT_STATUS_CODE = "defaultStatusCode";
    protected static final String NAME_OF_FIELD_BINARY_MEDIA_TYPES = "binaryMediaTypes";

    /**
     * Default constructor.
     */
    public HttpEndpointDeserializer() {
        super();
    }

    /**
     * Constructor with Class parameter.
     *
     * @param vc Class
     */
    @SuppressWarnings("unused")
    public HttpEndpointDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public HttpClient deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        HttpClient httpClient;
        HttpEndpointConfiguration httpEndpointConfiguration = null;
        JsonNode jsonNode = p.getCodec().readTree(p);

        JsonNode endpointConfiguration = jsonNode.get(NAME_OF_FIELD_ENDPOINT_CONFIGURATION);
        if (endpointConfiguration != null) {
            httpEndpointConfiguration = new HttpEndpointConfiguration();

            JsonNode jnRequestUrl = endpointConfiguration.get(NAME_OF_FIELD_REQUEST_URL);
            if (jnRequestUrl != null) {
                httpEndpointConfiguration.setRequestUrl(jnRequestUrl.asText());
            }

            JsonNode jnRequestMethod = endpointConfiguration.get(NAME_OF_FIELD_REQUEST_METHOD);
            if (jnRequestMethod != null) {
                HttpMethod httpMethod = HttpMethod.resolve(jnRequestMethod.asText());
                httpEndpointConfiguration.setRequestMethod(httpMethod);
            }

            JsonNode jnCharset = endpointConfiguration.get(NAME_OF_FIELD_CHARSET);
            if (jnCharset != null) {
                httpEndpointConfiguration.setCharset(jnCharset.asText());
            }

            JsonNode jnContentType = endpointConfiguration.get(NAME_OF_FIELD_CONTENT_TYPE);
            if (jnContentType != null) {
                httpEndpointConfiguration.setContentType(jnContentType.asText());
            }

            JsonNode jnDefaultAcceptHeader = endpointConfiguration.get(NAME_OF_FIELD_DEFAULT_ACCEPT_HEADER);
            if (jnDefaultAcceptHeader != null) {
                httpEndpointConfiguration.setDefaultAcceptHeader(Boolean.parseBoolean(jnDefaultAcceptHeader.asText()));
            }

            JsonNode jnHandleAttributeHeaders = endpointConfiguration.get(NAME_OF_FIELD_HANDLE_ATTRIBUTE_HEADERS);
            if (jnHandleAttributeHeaders != null) {
                httpEndpointConfiguration.setHandleAttributeHeaders(Boolean.parseBoolean(jnHandleAttributeHeaders.asText()));
            }

            JsonNode jnHandleCookies = endpointConfiguration.get(NAME_OF_FIELD_HANDLE_COOKIES);
            if (jnHandleCookies != null) {
                httpEndpointConfiguration.setHandleCookies(Boolean.parseBoolean(jnHandleCookies.asText()));
            }

            JsonNode jnDefaultStatusCode = endpointConfiguration.get(NAME_OF_FIELD_DEFAULT_STATUS_CODE);
            if (jnDefaultStatusCode != null) {
                httpEndpointConfiguration.setDefaultStatusCode(Integer.parseInt(jnDefaultStatusCode.asText()));
            }

            JsonNode jsonNodeBinaryMediaTypes = endpointConfiguration.get(NAME_OF_FIELD_BINARY_MEDIA_TYPES);
            if (jsonNodeBinaryMediaTypes != null) {
                List<String> listOfBinaryMediaTypes = new ArrayList<>();
                for (final JsonNode jsonNodeBinaryMediaType : jsonNodeBinaryMediaTypes) {
                    listOfBinaryMediaTypes.add(jsonNodeBinaryMediaType.asText());
                }

                List<MediaType> binaryMediaTypes = new ArrayList<>();
                for (String stringBinaryMediaType : listOfBinaryMediaTypes) {
                    binaryMediaTypes.add(MediaType.parseMediaType(stringBinaryMediaType));
                }

                httpEndpointConfiguration.setBinaryMediaTypes(binaryMediaTypes);
            }
        }

        if (httpEndpointConfiguration != null) {
            httpClient = new HttpClient(httpEndpointConfiguration);
        } else {
            httpClient = new HttpClient();
        }

        JsonNode jnName = jsonNode.get(NAME_OF_FIELD_NAME);
        if (jnName != null) {
            httpClient.setName(jnName.asText());
        }

        return httpClient;
    }
}
