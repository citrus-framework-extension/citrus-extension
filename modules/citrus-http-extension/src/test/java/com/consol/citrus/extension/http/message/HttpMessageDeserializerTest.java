package com.consol.citrus.extension.http.message;

import com.consol.citrus.http.message.HttpMessage;
import com.consol.citrus.message.MessageHeaders;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HttpMessageDeserializerTest extends AbstractTestNGUnitTest {

    private ObjectMapper objectMapper;

    @BeforeMethod
    public void setUpBeforeTestMethod() {
        objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addDeserializer(HttpMessage.class, new HttpMessageDeserializer());
        objectMapper.registerModule(module);
    }

    protected void assertHttpMessage(HttpMessage defaultMessage) {
        Assert.assertNotNull(defaultMessage);
        Assert.assertNotNull(defaultMessage.getHeader(MessageHeaders.ID));
        Assert.assertNotNull(defaultMessage.getHeader(MessageHeaders.TIMESTAMP));
    }

    @Test
    public void testHttpMessageDeserializerQueryParams() throws IOException {
        String message = "{\n" +
                "    \"payload\": \"Some payload\",\n" +
                "    \"headers\": {\n" +
                "        \"h1\": \"v1\",\n" +
                "        \"h2\": \"v2\"\n" +
                "    },\n" +
                "    \"queryParams\": {\n" +
                "        \"queryParam1\": \"val1\",\n" +
                "        \"queryParam2\": \"val2\"\n" +
                "    }\n" +
                "}";

        HttpMessage httpMessage = objectMapper.readValue(message, HttpMessage.class);

        assertHttpMessage(httpMessage);
        Assert.assertEquals(httpMessage.getPayload(), "Some payload");
        Assert.assertEquals(httpMessage.getHeader("h1"), "v1");
        Assert.assertEquals(httpMessage.getHeader("h2"), "v2");

        Map<String, String> expectedQueryParams = new HashMap<String, String>() {{
            put("queryParam1", "val1");
            put("queryParam2", "val2");
        }};
        Assert.assertEquals(httpMessage.getQueryParams(), expectedQueryParams);
    }

    @Test
    public void testHttpMessageDeserializerCookies() throws IOException {
        String message = "{\n" +
                "    \"payload\": \"Some payload\",\n" +
                "    \"headers\": {\n" +
                "        \"h1\": \"v1\",\n" +
                "        \"h2\": \"v2\",\n" +
                "        \"h3\": \"v3\"\n" +
                "    },\n" +
                "    \"queryParams\": {\n" +
                "        \"queryParam1\": \"val1\",\n" +
                "        \"queryParam2\": \"val2\"\n" +
                "    },\n" +
                "    \"cookies\": {\n" +
                "        \"cookieName1\": \"cookieValue1\",\n" +
                "        \"cookieName2\": \"cookieValue2\"\n" +
                "    }\n" +
                "}";

        HttpMessage httpMessage = objectMapper.readValue(message, HttpMessage.class);

        assertHttpMessage(httpMessage);
        Assert.assertEquals(httpMessage.getPayload(), "Some payload");
        Assert.assertEquals(httpMessage.getHeader("h1"), "v1");
        Assert.assertEquals(httpMessage.getHeader("h2"), "v2");

        Map<String, String> expectedQueryParams = new HashMap<String, String>() {{
            put("queryParam1", "val1");
            put("queryParam2", "val2");
        }};
        Assert.assertEquals(httpMessage.getQueryParams(), expectedQueryParams);

        List<Cookie> expectedCookies = new ArrayList<Cookie>() {{
            add(new Cookie("cookieName1", "cookieValue1"));
            add(new Cookie("cookieName2", "cookieValue2"));
        }};
        Assert.assertEquals(httpMessage.getCookies().size(), expectedCookies.size());

        for (int i = 0; i < httpMessage.getCookies().size(); i++) {
            Cookie cookie = httpMessage.getCookies().get(i);
            Cookie expectedCookie = expectedCookies.stream()
                    .filter(c -> c.getName().equals(cookie.getName()))
                    .findFirst()
                    .orElse(null);

            //noinspection ConstantConditions
            Assert.assertEquals(cookie.getName(), expectedCookie.getName());
            Assert.assertEquals(cookie.getValue(), expectedCookie.getValue());
        }
    }

}