package com.consol.citrus.extension.http.endpoint;

import com.consol.citrus.http.client.HttpClient;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class HttpEndpointDeserializerTest extends AbstractTestNGUnitTest {

    private ObjectMapper objectMapper;

    @BeforeMethod
    public void setUpBeforeTestMethod() {
        objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addDeserializer(HttpClient.class, new HttpEndpointDeserializer());
        objectMapper.registerModule(module);
    }

    @Test
    public void testLoadEndpointFromJson() throws IOException {
        String jsonWithEndpoint = "{\n" +
                "  \"endpointConfiguration\": {\n" +
                "    \"requestUrl\": \"http://localhost\",\n" +
                "    \"requestMethod\": \"GET\",\n" +
                "    \"charset\": \"UTF-16\",\n" +
                "    \"contentType\": \"json/application\",\n" +
                "    \"defaultAcceptHeader\": false,\n" +
                "    \"handleAttributeHeaders\": false,\n" +
                "    \"handleCookies\": true,\n" +
                "    \"defaultStatusCode\": 200,\n" +
                "    \"binaryMediaTypes\": [\n" +
                "      \"application/octet-stream\",\n" +
                "      \"application/pdf\"\n" +
                "    ]\n" +
                "  }\n" +
                "}";

        HttpClient httpClient = objectMapper.readValue(jsonWithEndpoint, HttpClient.class);

        Assert.assertEquals(httpClient.getEndpointConfiguration().getRequestUrl(), "http://localhost");
        Assert.assertEquals(httpClient.getEndpointConfiguration().getRequestMethod(), HttpMethod.GET);
        Assert.assertEquals(httpClient.getEndpointConfiguration().getCharset(), "UTF-16");
        Assert.assertEquals(httpClient.getEndpointConfiguration().getContentType(), "json/application");
        Assert.assertFalse(httpClient.getEndpointConfiguration().isDefaultAcceptHeader());
        Assert.assertFalse(httpClient.getEndpointConfiguration().isHandleAttributeHeaders());
        Assert.assertTrue(httpClient.getEndpointConfiguration().isHandleCookies());
        Assert.assertEquals(httpClient.getEndpointConfiguration().getDefaultStatusCode(), 200);

        List<MediaType> expectedMediaTypes = Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_PDF);
        Assert.assertEquals(httpClient.getEndpointConfiguration().getBinaryMediaTypes(), expectedMediaTypes);
    }

}
