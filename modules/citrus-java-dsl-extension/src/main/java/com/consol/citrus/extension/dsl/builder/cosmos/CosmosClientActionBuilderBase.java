package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.AbstractTestActionBuilder;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import org.springframework.context.ApplicationContext;

/**
 * CosmosClientActionBuilderBase
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
@SuppressWarnings("unchecked")
public abstract class CosmosClientActionBuilderBase<T extends CosmosClientActionBuilderBase<?>> extends AbstractTestActionBuilder<DelegatingTestAction<TestAction>> {

    /**
     * Spring application context.
     */
    protected ApplicationContext applicationContext;

    /**
     * Cosmos client.
     */
    protected CosmosClient cosmosClient;

    /**
     * Cosmos message.
     */
    protected CosmosMessage cosmosMessage;

    /**
     * Default constructor.
     *
     * @param action        Delegated test action
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    protected CosmosClientActionBuilderBase(DelegatingTestAction<TestAction> action,
                                            CosmosClient cosmosClient,
                                            CosmosMessage cosmosMessage) {
        super(action);
        this.cosmosClient = cosmosClient;
        this.cosmosMessage = cosmosMessage;
    }

    /**
     * Sets the Spring bean application context.
     *
     * @param applicationContext ApplicationContext
     * @return T
     */
    public T withApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        return (T) this;
    }

}
