package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.exceptions.CitrusRuntimeException;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.consol.citrus.extension.cosmosdb.client.CosmosOperation.*;

@SuppressWarnings({"UnusedReturnValue", "unused"})
public class CosmosClientSendContainerActionBuilder extends CosmosClientActionBuilderBase<CosmosClientSendContainerActionBuilder> {

    private static final String DEFAULT_PARTITION_KEY = "/id";

    public CosmosClientSendContainerActionBuilder(DelegatingTestAction<TestAction> action,
                                                  CosmosClient cosmosClient,
                                                  CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    public CosmosClientSendDefaultActionBuilder create() {
        return create(DEFAULT_PARTITION_KEY);
    }

    public CosmosClientSendDefaultActionBuilder create(String partitionKey) {
        cosmosMessage.cosmosOperation(CREATE_CONTAINER);
        cosmosMessage.partitionKey(partitionKey);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendDefaultActionBuilder createIfNotExists() {
        return createIfNotExists(DEFAULT_PARTITION_KEY);
    }

    public CosmosClientSendDefaultActionBuilder createIfNotExists(String partitionKey) {
        cosmosMessage.cosmosOperation(CREATE_CONTAINER_IF_NOT_EXISTS);
        cosmosMessage.partitionKey(partitionKey);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendDefaultActionBuilder drop() {
        cosmosMessage.cosmosOperation(DROP_CONTAINER);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendDefaultActionBuilder createItem(Object payload) {
        return createItem(payload.getClass(), payload);
    }

    public CosmosClientSendDefaultActionBuilder createItem(Class<?> clazz, Object payload) {
        return createItem(clazz, payload, new ObjectMapper());
    }

    public CosmosClientSendDefaultActionBuilder createItem(Class<?> clazz, Object payload, ObjectMapper objectMapper) {
        cosmosMessage.cosmosOperation(CREATE_ITEM);
        cosmosMessage.itemClass(clazz);
        try {
            cosmosMessage.setPayload(objectMapper.writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            throw new CitrusRuntimeException("Failed to map object graph for message payload", e);
        }
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendDefaultActionBuilder readItem(Class<?> clazz, String id) {
        cosmosMessage.cosmosOperation(READ_ITEM);
        cosmosMessage.itemClass(clazz);
        cosmosMessage.itemId(id);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendDefaultActionBuilder deleteItem(String id) {
        cosmosMessage.cosmosOperation(DELETE_ITEM);
        cosmosMessage.itemId(id);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendDefaultActionBuilder queryItems(Class<?> clazz, String query) {
        cosmosMessage.cosmosOperation(QUERY_ITEMS);
        cosmosMessage.itemClass(clazz);
        cosmosMessage.query(query);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }
}
