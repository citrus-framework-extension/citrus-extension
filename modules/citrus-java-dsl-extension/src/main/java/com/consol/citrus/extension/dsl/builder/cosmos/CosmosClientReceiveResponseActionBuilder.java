package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.client.CosmosStatus;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import org.springframework.http.HttpStatus;

/**
 * CosmosClientReceiveResponseActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosClientReceiveResponseActionBuilder extends CosmosClientActionBuilderBase<CosmosClientReceiveResponseActionBuilder> {

    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    public CosmosClientReceiveResponseActionBuilder(DelegatingTestAction<TestAction> action,
                                                    CosmosClient cosmosClient,
                                                    CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    /**
     * Receives Cosmos DB response messages as client.
     *
     * @param status Cosmos response status.
     * @return CosmosClientReceiveActionBuilder
     */
    public CosmosClientReceiveActionBuilder response(int status) {
        cosmosMessage.statusCode(status);
        return new CosmosClientReceiveActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    /**
     * Receives Cosmos DB response messages as client.
     *
     * @param status Cosmos response status.
     * @return CosmosClientReceiveActionBuilder
     * @since 0.0.3
     */
    public CosmosClientReceiveActionBuilder response(HttpStatus status) {
        cosmosMessage.status(status);
        return new CosmosClientReceiveActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    /**
     * Receives Cosmos DB response messages as client.
     *
     * @param status Cosmos response status.
     * @return CosmosClientReceiveActionBuilder
     * @since 0.0.3
     */
    public CosmosClientReceiveActionBuilder response(CosmosStatus status) {
        cosmosMessage.status(status);
        return new CosmosClientReceiveActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientReceiveDefaultActionBuilder message(CosmosMessage cosmosMessage) {
        return new CosmosClientReceiveDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }
}
