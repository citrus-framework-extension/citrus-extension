package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

/**
 * Builder for Cosmos DB action.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosActionBuilder extends CosmosClientActionBuilderBase<CosmosActionBuilder> {

    /**
     * Default constructor.
     */
    public CosmosActionBuilder() {
        super(new DelegatingTestAction<>(), new CosmosClient(), new CosmosMessage());
    }

    /**
     * Initiate Cosmos DB client action.
     *
     * @param cosmosClient CosmosClient
     * @return CosmosClientSendReceiveSelectorActionBuilder
     */
    public CosmosClientSendReceiveSelectorActionBuilder client(CosmosClient cosmosClient) {
        this.cosmosClient = cosmosClient;
        return new CosmosClientSendReceiveSelectorActionBuilder(action, this.cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

}
