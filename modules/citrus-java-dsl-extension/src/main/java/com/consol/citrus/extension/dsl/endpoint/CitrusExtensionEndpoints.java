package com.consol.citrus.extension.dsl.endpoint;

import com.consol.citrus.dsl.endpoint.CitrusEndpoints;
import com.consol.citrus.extension.cosmosdb.client.CosmosClientBuilder;

/**
 * This abstract class aggregates Citrus extension endpoints.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public abstract class CitrusExtensionEndpoints extends CitrusEndpoints {

    /**
     * Prevent public instantiation.
     */
    protected CitrusExtensionEndpoints() {
        super();
    }

    /**
     * Creates new CosmosClient builder.
     *
     * @return CosmosClientBuilder
     */
    public static CosmosClientBuilder cosmos() {
        return new CosmosClientBuilder();
    }

}
