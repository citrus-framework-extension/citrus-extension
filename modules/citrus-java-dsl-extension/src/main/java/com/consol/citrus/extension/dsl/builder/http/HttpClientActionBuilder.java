package com.consol.citrus.extension.dsl.builder.http;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.AbstractTestActionBuilder;
import com.consol.citrus.endpoint.Endpoint;
import org.springframework.context.ApplicationContext;

/**
 * Action executes http client operations such as sending requests and receiving responses.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.2
 */
public class HttpClientActionBuilder extends AbstractTestActionBuilder<DelegatingTestAction<TestAction>> {

    /**
     * Spring application context
     */
    private ApplicationContext applicationContext;

    /**
     * Target http client instance
     */
    private Endpoint httpClient;
    /**
     * Http client uri
     */
    private String httpClientUri;

    /**
     * Default constructor.
     */
    public HttpClientActionBuilder(DelegatingTestAction<TestAction> action, Endpoint httpClient) {
        super(action);
        this.httpClient = httpClient;
    }

    /**
     * Default constructor.
     */
    public HttpClientActionBuilder(DelegatingTestAction<TestAction> action, String httpClientUri) {
        super(action);
        this.httpClientUri = httpClientUri;
    }


    /**
     * Sets the Spring bean application context.
     *
     * @param applicationContext Spring application context.
     */
    public HttpClientActionBuilder withApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        return this;
    }

    public HttpClientSendRequestActionBuilder send() {
        HttpClientSendRequestActionBuilder httpClientSendRequestActionBuilder;
        if (httpClient != null) {
            httpClientSendRequestActionBuilder = new HttpClientSendRequestActionBuilder(action, httpClient);
        } else {
            httpClientSendRequestActionBuilder = new HttpClientSendRequestActionBuilder(action, httpClientUri);
        }
        httpClientSendRequestActionBuilder.withApplicationContext(applicationContext);
        return httpClientSendRequestActionBuilder;
    }

    public HttpClientReceiveResponseActionBuilder receive() {
        HttpClientReceiveResponseActionBuilder httpClientReceiveResponseActionBuilder;
        if (httpClient != null) {
            httpClientReceiveResponseActionBuilder = new HttpClientReceiveResponseActionBuilder(action, httpClient);
        } else {
            httpClientReceiveResponseActionBuilder = new HttpClientReceiveResponseActionBuilder(action, httpClientUri);
        }
        httpClientReceiveResponseActionBuilder.withApplicationContext(applicationContext);
        return httpClientReceiveResponseActionBuilder;
    }
}
