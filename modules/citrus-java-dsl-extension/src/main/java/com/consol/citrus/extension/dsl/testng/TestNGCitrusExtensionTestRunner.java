package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.TestAction;
import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.dsl.builder.BuilderSupport;
import com.consol.citrus.dsl.runner.TestRunner;
import com.consol.citrus.dsl.testng.TestNGCitrusTestRunner;
import com.consol.citrus.extension.actions.SleepAction;
import com.consol.citrus.extension.dsl.builder.cosmos.CosmosActionBuilder;
import com.consol.citrus.extension.dsl.builder.http.HttpActionBuilder;
import com.consol.citrus.extension.dsl.runner.DefaultTestRunnerExtension;
import com.consol.citrus.extension.dsl.runner.TestRunnerExtension;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;

/**
 * Extension for TestNGCitrusTestRunner.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class TestNGCitrusExtensionTestRunner extends TestNGCitrusTestRunner implements TestRunnerExtension {

    private TestRunnerExtension testRunnerExtension;

    @Override
    protected TestRunner createTestRunner(Method method, TestContext context) {
        super.createTestRunner(method, context);

        testRunnerExtension = new DefaultTestRunnerExtension(applicationContext, context);
        testRunnerExtension.testClass(getClass());
        testRunnerExtension.packageName(this.getClass().getPackage().getName());

        if (method.getAnnotation(CitrusTest.class) != null) {
            CitrusTest citrusTestAnnotation = method.getAnnotation(CitrusTest.class);
            if (StringUtils.hasText(citrusTestAnnotation.name())) {
                testRunnerExtension.name(citrusTestAnnotation.name());
            } else {
                testRunnerExtension.name(method.getDeclaringClass().getSimpleName() + "." + method.getName());
            }
        } else {
            testRunnerExtension.name(method.getDeclaringClass().getSimpleName() + "." + method.getName());
        }

        return testRunnerExtension;
    }

    public Object extractVariable(String variableName) {
        return testRunnerExtension.extractVariable(variableName);
    }

    public TestAction cosmos(BuilderSupport<CosmosActionBuilder> configurer) {
        return testRunnerExtension.cosmos(configurer);
    }

    public TestAction httpExt(BuilderSupport<HttpActionBuilder> configurer) {
        return testRunnerExtension.httpExt(configurer);
    }

    @Override
    public SleepAction sleep(String sleepExpression, boolean handleException) {
        return testRunnerExtension.sleep(sleepExpression, handleException);
    }

    @Override
    public SleepAction sleep(String sleepExpression) {
        return testRunnerExtension.sleep(sleepExpression, false);
    }
}
