package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.Citrus;
import com.consol.citrus.container.SequenceBeforeSuite;
import com.consol.citrus.context.TestContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Extension to the Citrus TestRunnerBeforeSuiteSupport.
 *
 * @author Mariusz Marciniuk
 * @see com.consol.citrus.dsl.runner.TestRunnerBeforeSuiteSupport
 * @since 0.0.7
 */
public abstract class TestRunnerExtensionBeforeSuiteSupport extends SequenceBeforeSuite implements ApplicationContextAware, InitializingBean {

    /**
     * Runner instance to receive before suite actions
     */
    private TestRunnerExtension testRunnerExtension;

    /**
     * Spring application context for test context initialization
     */
    private ApplicationContext applicationContext;

    /**
     * Subclasses implement this method to add before suite logic.
     *
     * @param runner TestRunnerExtension
     */
    public abstract void beforeSuite(TestRunnerExtension runner);

    @Override
    public void doExecute(TestContext context) {
        beforeSuite(testRunnerExtension);
    }

    @Override
    public void setApplicationContext(@SuppressWarnings("NullableProblems") ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @SuppressWarnings("RedundantThrows")
    @Override
    public void afterPropertiesSet() throws Exception {
        testRunnerExtension = new DefaultTestRunnerExtension(applicationContext, Citrus.newInstance(applicationContext).createTestContext());
    }
}
