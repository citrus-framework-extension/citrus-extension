package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.TestAction;
import com.consol.citrus.TestCase;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.dsl.builder.BuilderSupport;
import com.consol.citrus.dsl.runner.DefaultTestRunner;
import com.consol.citrus.extension.actions.ExtractVariableAction;
import com.consol.citrus.extension.actions.SleepAction;
import com.consol.citrus.extension.dsl.builder.cosmos.CosmosActionBuilder;
import com.consol.citrus.extension.dsl.builder.http.HttpActionBuilder;
import org.springframework.context.ApplicationContext;

/**
 * Extension to the Citrus DefaultTestRunner.
 *
 * @author Mariusz Marciniuk
 * @see com.consol.citrus.dsl.runner.DefaultTestRunner
 * @since 0.0.7
 */
public class DefaultTestRunnerExtension extends DefaultTestRunner implements TestRunnerExtension {

    /**
     * Default constructor
     */
    public DefaultTestRunnerExtension() {
        super();
    }

    /**
     * Constructor initializing test case.
     *
     * @param testCase TestCase
     */
    public DefaultTestRunnerExtension(TestCase testCase) {
        super(testCase);
    }

    /**
     * Constructor using Spring bean application context.
     *
     * @param applicationContext ApplicationContext
     * @param context            TestContext
     */
    public DefaultTestRunnerExtension(ApplicationContext applicationContext, TestContext context) {
        super(applicationContext, context);
    }

    @Override
    public Object extractVariable(String variableName) {
        ExtractVariableAction extractVariableAction = new ExtractVariableAction();
        extractVariableAction.setVariableName(variableName);
        run(extractVariableAction);
        return extractVariableAction.getExtractedVariable();
    }

    @Override
    public TestAction cosmos(BuilderSupport<CosmosActionBuilder> configurer) {
        CosmosActionBuilder builder = new CosmosActionBuilder()
                .withApplicationContext(getApplicationContext());
        configurer.configure(builder);
        return run(builder.build()).getDelegate();
    }

    @Override
    public TestAction httpExt(BuilderSupport<HttpActionBuilder> configurer) {
        HttpActionBuilder httpActionBuilder = new HttpActionBuilder()
                .withApplicationContext(getApplicationContext());
        configurer.configure(httpActionBuilder);
        return run(httpActionBuilder.build()).getDelegate();
    }

    @Override
    public SleepAction sleep(String sleepExpression, boolean handleException) {
        SleepAction sleepAction = new SleepAction()
                .setDelayTimeExpression(sleepExpression)
                .setHandleException(handleException);
        return run(sleepAction);
    }

    @Override
    public SleepAction sleep(String sleepExpression) {
        return sleep(sleepExpression, false);
    }
}
