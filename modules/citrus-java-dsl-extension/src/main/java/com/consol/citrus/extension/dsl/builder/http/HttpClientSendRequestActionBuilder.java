package com.consol.citrus.extension.dsl.builder.http;

import com.consol.citrus.TestAction;
import com.consol.citrus.actions.SendMessageAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.SendMessageBuilder;
import com.consol.citrus.endpoint.Endpoint;
import com.consol.citrus.http.message.HttpMessage;
import com.consol.citrus.http.message.HttpMessageContentBuilder;
import com.consol.citrus.http.message.HttpMessageUtils;
import com.consol.citrus.message.Message;
import com.consol.citrus.validation.builder.StaticMessageContentBuilder;

/**
 * @author Mariusz Marciniuk
 * @since 0.0.2
 */
public class HttpClientSendRequestActionBuilder extends SendMessageBuilder<SendMessageAction, HttpClientSendRequestActionBuilder> {
    /**
     * Http message to send or receive
     */
    @SuppressWarnings("FieldMayBeFinal")
    private HttpMessage httpMessage = new HttpMessage();

    /**
     * Default constructor using http client endpoint.
     *
     * @param delegate   Delegated test action.
     * @param httpClient Http citrus client.
     */
    public HttpClientSendRequestActionBuilder(DelegatingTestAction<TestAction> delegate, Endpoint httpClient) {
        super(delegate);
        delegate.setDelegate(new SendMessageAction());
        getAction().setEndpoint(httpClient);
        initMessage(httpMessage);
    }

    /**
     * Default constructor using http client uri.
     *
     * @param delegate      Delegated test action.
     * @param httpClientUri Http client uri.
     */
    public HttpClientSendRequestActionBuilder(DelegatingTestAction<TestAction> delegate, String httpClientUri) {
        super(delegate);
        delegate.setDelegate(new SendMessageAction());
        getAction().setEndpointUri(httpClientUri);
        initMessage(httpMessage);
    }

    /**
     * Initialize message builder.
     *
     * @param message Http Citrus message.
     */
    private void initMessage(HttpMessage message) {
        StaticMessageContentBuilder staticMessageContentBuilder = StaticMessageContentBuilder.withMessage(message);
        staticMessageContentBuilder.setMessageHeaders(message.getHeaders());
        getAction().setMessageBuilder(new HttpMessageContentBuilder(message, staticMessageContentBuilder));
    }

    @Override
    public HttpClientSendRequestActionBuilder message(Message message) {
        HttpMessageUtils.copy(message, httpMessage);
        return this;
    }

}
