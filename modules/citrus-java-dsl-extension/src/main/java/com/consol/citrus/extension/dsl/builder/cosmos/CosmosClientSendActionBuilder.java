package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

/**
 * CosmosClientSendActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosClientSendActionBuilder extends CosmosClientActionBuilderBase<CosmosClientSendActionBuilder> {

    /**
     * Default constructor.
     *
     * @param action       DelegatingTestAction<TestAction>
     * @param cosmosClient CosmosClient
     */
    public CosmosClientSendActionBuilder(DelegatingTestAction<TestAction> action,
                                         CosmosClient cosmosClient,
                                         CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    /**
     * Cosmos database.
     *
     * @param databaseName Cosmos database.
     * @return CosmosClientSendDatabaseActionBuilder
     */
    public CosmosClientSendDatabaseActionBuilder database(String databaseName) {
        cosmosMessage.databaseName(databaseName);
        return new CosmosClientSendDatabaseActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }


    public CosmosClientSendDefaultActionBuilder message(CosmosMessage cosmosMessage) {
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }
}
