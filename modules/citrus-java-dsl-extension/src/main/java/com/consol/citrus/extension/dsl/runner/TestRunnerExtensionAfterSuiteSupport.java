package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.Citrus;
import com.consol.citrus.container.SequenceAfterSuite;
import com.consol.citrus.context.TestContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Extension to the Citrus TestRunnerBeforeSuiteSupport.
 *
 * @author Mariusz Marciniuk
 * @see com.consol.citrus.dsl.runner.TestRunnerAfterSuiteSupport
 * @since 0.0.7
 */
public abstract class TestRunnerExtensionAfterSuiteSupport extends SequenceAfterSuite implements ApplicationContextAware, InitializingBean {

    /**
     * Runner instance to receive after suite actions
     */
    private TestRunnerExtension testRunnerExtension;

    /**
     * Spring application context for test context initialization
     */
    private ApplicationContext applicationContext;

    public abstract void afterSuite(TestRunnerExtension testRunnerExtension);

    @Override
    public void doExecute(TestContext context) {
        afterSuite(testRunnerExtension);
    }

    @SuppressWarnings("RedundantThrows")
    @Override
    public void afterPropertiesSet() throws Exception {
        testRunnerExtension = new DefaultTestRunnerExtension(applicationContext, Citrus.newInstance(applicationContext).createTestContext());
    }

    @Override
    public void setApplicationContext(@SuppressWarnings("NullableProblems") ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
