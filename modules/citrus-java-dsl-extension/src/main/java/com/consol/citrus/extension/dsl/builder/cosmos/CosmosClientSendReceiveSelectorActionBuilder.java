package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

/**
 * CosmosClientSendReceiveSelectorActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosClientSendReceiveSelectorActionBuilder extends CosmosClientActionBuilderBase<CosmosClientSendReceiveSelectorActionBuilder> {

    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    public CosmosClientSendReceiveSelectorActionBuilder(DelegatingTestAction<TestAction> action,
                                                        CosmosClient cosmosClient,
                                                        CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    /**
     * Sends Cosmos request as client.
     *
     * @return CosmosClientSendActionBuilder
     */
    public CosmosClientSendActionBuilder send() {
        return new CosmosClientSendActionBuilder(action, cosmosClient, new CosmosMessage())
                .withApplicationContext(applicationContext);
    }

    /**
     * Receives Cosmos DB messages as client.
     *
     * @return CosmosClientReceiveActionBuilder
     */
    public CosmosClientReceiveResponseActionBuilder receive() {
        return new CosmosClientReceiveResponseActionBuilder(action, cosmosClient, new CosmosMessage())
                .withApplicationContext(applicationContext);
    }

}
