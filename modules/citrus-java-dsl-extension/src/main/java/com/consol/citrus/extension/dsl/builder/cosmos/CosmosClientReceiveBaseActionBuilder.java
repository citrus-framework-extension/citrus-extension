package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.actions.ReceiveMessageAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.ReceiveMessageBuilder;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import com.consol.citrus.message.MessageType;
import com.consol.citrus.validation.builder.StaticMessageContentBuilder;

/**
 * CosmosClientReceiveBaseActionBuilder
 *
 * @param <T> T extends CosmosClientReceiveBaseActionBuilder<\?>
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public abstract class CosmosClientReceiveBaseActionBuilder<T extends CosmosClientReceiveBaseActionBuilder<?>> extends ReceiveMessageBuilder<ReceiveMessageAction, T> {

    /**
     * Cosmos DB message to receive.
     */
    protected CosmosMessage cosmosMessage;

    /**
     * Cosmos client
     */
    protected final CosmosClient cosmosClient;

    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    public CosmosClientReceiveBaseActionBuilder(DelegatingTestAction<TestAction> action,
                                                CosmosClient cosmosClient,
                                                CosmosMessage cosmosMessage) {
        super(action);
        this.cosmosClient = cosmosClient;
        this.cosmosMessage = cosmosMessage;
        action.setDelegate(new ReceiveMessageAction());
        getAction().setEndpoint(this.cosmosClient);
        initMessage(this.cosmosMessage);
        messageType(MessageType.JSON);
        headerNameIgnoreCase(true);
    }

    /**
     * Initialize message builder.
     *
     * @param message CosmosMessage
     */
    private void initMessage(CosmosMessage message) {
        StaticMessageContentBuilder staticMessageContentBuilder = StaticMessageContentBuilder.withMessage(message);
        staticMessageContentBuilder.setMessageHeaders(message.getHeaders());
        getAction().setMessageBuilder(staticMessageContentBuilder);
    }

}
