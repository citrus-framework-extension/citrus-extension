package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.actions.SendMessageAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.SendMessageBuilder;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import com.consol.citrus.validation.builder.StaticMessageContentBuilder;

/**
 * @author Mariiusz Marciniuk
 * @since 0.0.1
 */
public abstract class CosmosClientSendBaseActionBuilder<T extends CosmosClientSendBaseActionBuilder<?>> extends SendMessageBuilder<SendMessageAction, T> {

    /**
     * Cosmos DB message to send.
     */
    protected CosmosMessage cosmosMessage;

    /**
     * Cosmos client
     */
    protected CosmosClient cosmosClient;

    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    protected CosmosClientSendBaseActionBuilder(DelegatingTestAction<TestAction> action,
                                                CosmosClient cosmosClient,
                                                CosmosMessage cosmosMessage) {
        super(action);
        this.cosmosClient = cosmosClient;
        this.cosmosMessage = cosmosMessage;
        action.setDelegate(new SendMessageAction());
        getAction().setEndpoint(this.cosmosClient);
        initMessage(this.cosmosMessage);
    }

    /**
     * Initialize message builder.
     *
     * @param cosmosMessage CosmosMessage
     */
    private void initMessage(CosmosMessage cosmosMessage) {
        StaticMessageContentBuilder staticMessageContentBuilder = StaticMessageContentBuilder.withMessage(cosmosMessage);
        staticMessageContentBuilder.setMessageHeaders(cosmosMessage.getHeaders());
        getAction().setMessageBuilder(staticMessageContentBuilder);
    }

}
