package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

/**
 * CosmosClientSendDefaultActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.7
 */
public class CosmosClientSendDefaultActionBuilder extends CosmosClientSendBaseActionBuilder<CosmosClientSendDefaultActionBuilder> {
    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    protected CosmosClientSendDefaultActionBuilder(DelegatingTestAction<TestAction> action, CosmosClient cosmosClient, CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }
}
