package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.builder.BuilderSupport;
import com.consol.citrus.dsl.runner.TestRunner;
import com.consol.citrus.extension.actions.SleepAction;
import com.consol.citrus.extension.dsl.builder.cosmos.CosmosActionBuilder;
import com.consol.citrus.extension.dsl.builder.http.HttpActionBuilder;

/**
 * Extension to the Citrus TestRunner.
 *
 * @author Mariusz Marciniuk
 * @see com.consol.citrus.dsl.runner.TestRunner
 * @since 0.0.7
 */
public interface TestRunnerExtension extends TestRunner {

    /**
     * Creates and executes a new extract variable action which allows to extract variable from Citrus Framework
     * variables storage.
     *
     * @param variableName Name of variable to extract.
     * @return Extracted object.
     */
    Object extractVariable(String variableName);

    /**
     * Add sleep action with time sleep expression.
     * Example:
     * <p><ul>
     * <li>5ms - 5 milliseconds</li>
     * <li>5s - 5 seconds</li>
     * <li>5m - 5 minutes</li>
     * <li>5h - 5 hours ;)</li>
     * <li>5d - 5 days O_O</li>
     * <li>5M - 5 months ಠ_ಠ</li>
     * <li>5y - 5 years ¯\_(ツ)_/¯</li>
     * </ul><p>
     *
     * @param sleepExpression           Sleep expression eg. `5s`
     * @param handleException True or false value. If it's set to true. It will show warning in logs and it will omit this action.
     * @return SleepAction
     */
    SleepAction sleep(String sleepExpression, boolean handleException);

    /**
     *  Add sleep action with time sleep expression with <i>handle exception</i> set to <i>false</i>.
     *
     * @see #sleep(String, boolean)
     * @param sleepExpression Sleep expression eg. `5s`
     * @return SleepAction
     */
    SleepAction sleep(String sleepExpression);

    /**
     * Creates and executes a new Cosmos test action.
     *
     * @param configurer Cosmos action builder.
     * @return TestAction
     */
    TestAction cosmos(BuilderSupport<CosmosActionBuilder> configurer);

    /**
     * Creates and executes a new Http action base on Http Citrus message.
     *
     * @param configurer Http action builder.
     * @return TestAction TestAction
     */
    TestAction httpExt(BuilderSupport<HttpActionBuilder> configurer);

}
