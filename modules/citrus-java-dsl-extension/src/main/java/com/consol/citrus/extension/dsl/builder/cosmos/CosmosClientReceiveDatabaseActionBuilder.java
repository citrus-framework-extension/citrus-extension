package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

import static com.consol.citrus.extension.cosmosdb.client.CosmosOperation.*;

/**
 * CosmosClientReceiveDatabaseActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
@SuppressWarnings("UnusedReturnValue")
public class CosmosClientReceiveDatabaseActionBuilder extends CosmosClientActionBuilderBase<CosmosClientReceiveDatabaseActionBuilder> {

    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    public CosmosClientReceiveDatabaseActionBuilder(DelegatingTestAction<TestAction> action,
                                                    CosmosClient cosmosClient,
                                                    CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    /**
     * Receive created Cosmos database.
     *
     * @return CosmosClientSendCreateDatabaseActionBuilder
     */
    public CosmosClientReceiveDefaultActionBuilder create() {
        cosmosMessage.cosmosOperation(CREATE_DATABASE);
        return new CosmosClientReceiveDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    /**
     * Receive created Cosmos database.
     *
     * @return CosmosClientReceiveCreateDatabaseActionBuilder
     */
    public CosmosClientReceiveDefaultActionBuilder createIfNotExists() {
        cosmosMessage.cosmosOperation(CREATE_DATABASE_IF_NOT_EXISTS);
        return new CosmosClientReceiveDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    /**
     * Receive dropped Cosmos database.
     *
     * @return CosmosClientSendDropDatabaseActionBuilder
     */
    public CosmosClientReceiveDefaultActionBuilder drop() {
        cosmosMessage.cosmosOperation(DROP_DATABASE);
        return new CosmosClientReceiveDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientReceiveContainerActionBuilder container(String containerName) {
        cosmosMessage.containerName(containerName);
        return new CosmosClientReceiveContainerActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }
}
