package com.consol.citrus.extension.dsl.builder.http;

import com.consol.citrus.TestAction;
import com.consol.citrus.actions.ReceiveMessageAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.ReceiveMessageBuilder;
import com.consol.citrus.endpoint.Endpoint;
import com.consol.citrus.http.message.HttpMessage;
import com.consol.citrus.http.message.HttpMessageContentBuilder;
import com.consol.citrus.http.message.HttpMessageUtils;
import com.consol.citrus.message.Message;
import com.consol.citrus.message.MessageType;
import com.consol.citrus.validation.builder.StaticMessageContentBuilder;

/**
 * @author Mariusz Marciniuk
 * @since 0.0.2
 */
public class HttpClientReceiveResponseActionBuilder extends ReceiveMessageBuilder<ReceiveMessageAction, HttpClientReceiveResponseActionBuilder> {
    /**
     * Http message to send or receive
     */
    @SuppressWarnings("FieldMayBeFinal")
    private HttpMessage httpMessage = new HttpMessage();

    /**
     * Default constructor using http client endpoint.
     *
     * @param delegate   Delegated test action.
     * @param httpClient Http citrus client.
     */
    public HttpClientReceiveResponseActionBuilder(DelegatingTestAction<TestAction> delegate, Endpoint httpClient) {
        super(delegate);
        delegate.setDelegate(new ReceiveMessageAction());
        getAction().setEndpoint(httpClient);
        initMessage(httpMessage);
        messageType(MessageType.XML);
        headerNameIgnoreCase(true);
    }

    /**
     * Default constructor using http client uri.
     *
     * @param delegate      Delegated test action.
     * @param httpClientUri Http client URI.
     */
    public HttpClientReceiveResponseActionBuilder(DelegatingTestAction<TestAction> delegate, String httpClientUri) {
        super(delegate);
        delegate.setDelegate(new ReceiveMessageAction());
        getAction().setEndpointUri(httpClientUri);
        initMessage(httpMessage);
        messageType(MessageType.XML);
        headerNameIgnoreCase(true);
    }

    /**
     * Initialize message builder.
     *
     * @param message Http Citrus message.
     */
    private void initMessage(HttpMessage message) {
        StaticMessageContentBuilder staticMessageContentBuilder = StaticMessageContentBuilder.withMessage(message);
        staticMessageContentBuilder.setMessageHeaders(message.getHeaders());
        getAction().setMessageBuilder(new HttpMessageContentBuilder(message, staticMessageContentBuilder));
    }

    @Override
    public HttpClientReceiveResponseActionBuilder message(Message message) {
        HttpMessageUtils.copy(message, httpMessage);
        return this;
    }

}
