package com.consol.citrus.extension.dsl.builder.http;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.dsl.builder.AbstractTestActionBuilder;
import com.consol.citrus.http.client.HttpClient;
import org.springframework.context.ApplicationContext;

/**
 * Http action builder for client base on HttpMessage.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.2
 */
public class HttpActionBuilder extends AbstractTestActionBuilder<DelegatingTestAction<TestAction>> {

    /**
     * Spring application context
     */
    private ApplicationContext applicationContext;

    /**
     * Default constructor.
     */
    public HttpActionBuilder() {
        super(new DelegatingTestAction<>());
    }

    /**
     * Initiate http client action.
     */
    public HttpClientActionBuilder client(HttpClient httpClient) {
        return new HttpClientActionBuilder(action, httpClient)
                .withApplicationContext(applicationContext);
    }

    /**
     * Initiate http client action.
     */
    public HttpClientActionBuilder client(String httpClient) {
        return new HttpClientActionBuilder(action, httpClient)
                .withApplicationContext(applicationContext);
    }

    /**
     * Sets the Spring bean application context.
     *
     * @param applicationContext Spring application context.
     */
    public HttpActionBuilder withApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        return this;
    }

}
