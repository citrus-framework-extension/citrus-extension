package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

/**
 * CosmosClientReceiveActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosClientReceiveActionBuilder extends CosmosClientActionBuilderBase<CosmosClientReceiveActionBuilder> {

    /**
     * Default constructor.
     *
     * @param action        Delegated test action.
     * @param cosmosClient  CosmosClient
     * @param cosmosMessage CosmosMessage
     */
    public CosmosClientReceiveActionBuilder(DelegatingTestAction<TestAction> action,
                                            CosmosClient cosmosClient,
                                            CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    /**
     * Cosmos database operations.
     *
     * @param databaseName Cosmos database name.
     * @return CosmosClientReceiveDatabaseActionBuilder
     */
    public CosmosClientReceiveDatabaseActionBuilder database(String databaseName) {
        cosmosMessage.databaseName(databaseName);
        return new CosmosClientReceiveDatabaseActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

}
