package com.consol.citrus.extension.dsl.builder.cosmos;

import com.consol.citrus.TestAction;
import com.consol.citrus.dsl.actions.DelegatingTestAction;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;

import static com.consol.citrus.extension.cosmosdb.client.CosmosOperation.*;

/**
 * CosmosClientSendDatabaseActionBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
@SuppressWarnings("UnusedReturnValue")
public class CosmosClientSendDatabaseActionBuilder extends CosmosClientActionBuilderBase<CosmosClientSendDatabaseActionBuilder> {

    /**
     * Default constructor.
     *
     * @param action       DelegatingTestAction<TestAction>
     * @param cosmosClient CosmosClient
     */
    public CosmosClientSendDatabaseActionBuilder(DelegatingTestAction<TestAction> action,
                                                 CosmosClient cosmosClient,
                                                 CosmosMessage cosmosMessage) {
        super(action, cosmosClient, cosmosMessage);
    }

    /**
     * Create Cosmos database.
     *
     * @return CosmosClientSendCreateDatabaseActionBuilder
     */
    public CosmosClientSendDefaultActionBuilder create() {
        cosmosMessage.cosmosOperation(CREATE_DATABASE);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    /**
     * Create Cosmos database if not exists.
     *
     * @return CosmosClientSendCreateDatabaseActionBuilder
     */
    public CosmosClientSendDefaultActionBuilder createIfNotExists() {
        cosmosMessage.cosmosOperation(CREATE_DATABASE_IF_NOT_EXISTS);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    /**
     * Drop Cosmos database.
     *
     * @return CosmosClientSendDropDatabaseActionBuilder
     */
    public CosmosClientSendDefaultActionBuilder drop() {
        cosmosMessage.cosmosOperation(DROP_DATABASE);
        return new CosmosClientSendDefaultActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }

    public CosmosClientSendContainerActionBuilder container(String containerName) {
        cosmosMessage.containerName(containerName);
        return new CosmosClientSendContainerActionBuilder(action, cosmosClient, cosmosMessage)
                .withApplicationContext(applicationContext);
    }
}
