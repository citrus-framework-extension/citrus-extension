package com.consol.citrus.extension.dsl.endpoint;

import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CitrusExtensionEndpointsTest {

    @Test
    public void testCosmosClientBuilder() {
        CosmosClient endpoint = CitrusExtensionEndpoints.cosmos().build();
        Assert.assertNotNull(endpoint);
    }

}
