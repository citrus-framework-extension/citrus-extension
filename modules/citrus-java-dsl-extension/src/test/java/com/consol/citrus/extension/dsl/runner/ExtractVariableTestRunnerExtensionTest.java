package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.TestCase;
import com.consol.citrus.actions.CreateVariablesAction;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExtractVariableTestRunnerExtensionTest extends AbstractTestNGUnitTest {

    @Test
    public void testExtractVariable() {
        String variableKey = "varKey";
        String variableValue = "varValue";
        final Object[] extractedVariable = new Object[1];

        MockTestRunner mockTestRunner = new MockTestRunner(getClass().getSimpleName(), applicationContext, context) {
            @Override
            public void execute() {
                createVariable(variableKey, variableValue);
                extractedVariable[0] = extractVariable(variableKey);
            }
        };

        TestCase testCase = mockTestRunner.getTestCase();
        Assert.assertEquals(testCase.getActionCount(), 2);
        Assert.assertEquals(testCase.getActions().get(0).getClass(), CreateVariablesAction.class);

        Assert.assertEquals(extractedVariable[0].toString(), variableValue);
    }

}
