package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.context.TestContext;
import org.springframework.context.ApplicationContext;

/**
 * @author Mariusz Maricniuk
 * @since 0.0.7
 */
public class MockTestRunner extends DefaultTestRunnerExtension {

    /**
     * Constructor using an application context.
     *
     * @param name               Name
     * @param applicationContext ApplicationContext
     * @param context            TestContext
     */
    public MockTestRunner(String name, ApplicationContext applicationContext, TestContext context) {
        super(applicationContext, context);

        name(name);

        start();
        execute();
        stop();
    }

    public void execute() {
    }

}
