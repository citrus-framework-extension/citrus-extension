package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.exceptions.CitrusRuntimeException;
import com.consol.citrus.extension.dsl.TestConfig;
import com.consol.citrus.extension.exceptions.CitrusExtensionRuntimeException;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {TestConfig.class})
public class SleepActionTestRunnerTest extends TestNGCitrusExtensionTestRunner {

    @CitrusTest
    @Test
    public void testSleepAction() {
        variable("sleepExpression", "1s");
        sleep("${sleepExpression}");
    }

    @CitrusTest
    @Test(expectedExceptions = {CitrusExtensionRuntimeException.class, CitrusRuntimeException.class},
            expectedExceptionsMessageRegExp = "com.consol.citrus.exceptions.CitrusRuntimeException: Unknown variable 'sleepExpression'")
    public void testSleepActionException() {
        sleep("${sleepExpression}");
    }

    @CitrusTest
    @Test
    public void testSleepActionHandelException() {
        sleep("${sleepExpression}", true);
    }

}
