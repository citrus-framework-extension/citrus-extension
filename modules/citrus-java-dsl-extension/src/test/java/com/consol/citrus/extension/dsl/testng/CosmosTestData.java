package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class CosmosTestData {

    @JsonDeserialize(using = CosmosMessageDeserializer.class)
    private CosmosMessage cosmosRequestMessage;
    @JsonDeserialize(using = CosmosMessageDeserializer.class)
    private CosmosMessage cosmosControlMessage;

    public CosmosMessage getCosmosRequestMessage() {
        return cosmosRequestMessage;
    }

    public void setCosmosRequestMessage(CosmosMessage cosmosRequestMessage) {
        this.cosmosRequestMessage = cosmosRequestMessage;
    }

    public CosmosMessage getCosmosControlMessage() {
        return cosmosControlMessage;
    }

    public void setCosmosControlMessage(CosmosMessage cosmosControlMessage) {
        this.cosmosControlMessage = cosmosControlMessage;
    }

    @Override
    public String toString() {
        return "CosmosTestData{" +
                "cosmosRequestMessage=" + cosmosRequestMessage +
                ", cosmosControlMessage=" + cosmosControlMessage +
                '}';
    }
}
