package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.extension.http.message.HttpMessageDeserializer;
import com.consol.citrus.http.message.HttpMessage;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@SuppressWarnings("unused")
public class TestData {

    @JsonDeserialize(using = HttpMessageDeserializer.class)
    private HttpMessage httpRequestMessage;
    @JsonDeserialize(using = HttpMessageDeserializer.class)
    private HttpMessage httpControlMessage;

    public HttpMessage getHttpRequestMessage() {
        return httpRequestMessage;
    }

    public void setHttpRequestMessage(HttpMessage httpRequestMessage) {
        this.httpRequestMessage = httpRequestMessage;
    }

    public HttpMessage getHttpControlMessage() {
        return httpControlMessage;
    }

    public void setHttpControlMessage(HttpMessage httpControlMessage) {
        this.httpControlMessage = httpControlMessage;
    }

    @Override
    public String toString() {
        return "TestData{" +
                "httpRequestMessage=" + httpRequestMessage +
                ", httpControlMessage=" + httpControlMessage +
                '}';
    }
}
