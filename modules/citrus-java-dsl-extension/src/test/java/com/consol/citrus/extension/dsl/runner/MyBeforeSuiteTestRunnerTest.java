package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.extension.dsl.TestConfig;
import com.consol.citrus.extension.dsl.testng.TestNGCitrusExtensionTestRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {TestConfig.class})
public class MyBeforeSuiteTestRunnerTest extends TestNGCitrusExtensionTestRunner {

    @Autowired
    private MyBeforeSuiteTestRunner myBeforeSuiteTestRunner;

    @CitrusTest
    @Test
    public void testBeforeSuiteTestRunnerTest() {
        log.info("Test - MyBeforeSuiteTestRunnerTest");
        Assert.assertEquals(myBeforeSuiteTestRunner.getExecutionCount(), 1);
    }

}
