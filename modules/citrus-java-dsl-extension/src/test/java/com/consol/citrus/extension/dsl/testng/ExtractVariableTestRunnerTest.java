package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.annotations.CitrusTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ExtractVariableTestRunnerTest extends TestNGCitrusExtensionTestRunner {

    @CitrusTest
    @Test
    public void testExtractVariable() {
        String variableKey = "varKey";
        String variableValue = "varValue";
        createVariable(variableKey, variableValue);

        Object extractedVariable = extractVariable(variableKey);

        Assert.assertEquals(extractedVariable.toString(), variableValue);
    }

    @CitrusTest
    @Test
    public void testExtractVariableCreatedWithVariable() {
        String variableKey = "varKey";
        String variableValue = "varValue";
        variable(variableKey, variableValue);

        Object extractedVariable = extractVariable(variableKey);

        Assert.assertEquals(extractedVariable.toString(), variableValue);
    }

}
