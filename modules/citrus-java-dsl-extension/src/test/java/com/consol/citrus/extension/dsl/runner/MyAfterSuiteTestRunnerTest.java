package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.extension.dsl.TestConfig;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {TestConfig.class})
public class MyAfterSuiteTestRunnerTest extends AbstractTestNGUnitTest {

    @Autowired
    private MyAfterSuiteTestRunner myAfterSuiteTestRunner;

    @Test
    public void testAfterSuiteBuilder() {
        Assert.assertEquals(myAfterSuiteTestRunner.getExecutionCount(), 0);
        myAfterSuiteTestRunner.execute(context);
        Assert.assertEquals(myAfterSuiteTestRunner.getExecutionCount(), 1);
    }

}
