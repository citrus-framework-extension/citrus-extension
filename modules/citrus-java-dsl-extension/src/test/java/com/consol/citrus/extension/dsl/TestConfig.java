package com.consol.citrus.extension.dsl;

import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.data.TestDataLoader;
import com.consol.citrus.extension.data.TestDataLoaders;
import com.consol.citrus.extension.dsl.endpoint.CitrusExtensionEndpoints;
import com.consol.citrus.extension.dsl.runner.MyAfterSuiteTestRunner;
import com.consol.citrus.extension.dsl.runner.MyBeforeSuiteTestRunner;
import com.consol.citrus.extension.dsl.testng.CosmosTestData;
import com.consol.citrus.extension.dsl.testng.TestData;
import com.consol.citrus.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestConfig {

    @Value("${cosmosClientTest.cosmosUri:https://localhost:8081/}")
    private String cosmosUri = "https://localhost:8081/";
    @Value("${cosmosClientTest.cosmosKey:C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==}")
    private String cosmosKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";
    @Value("${cosmosClientTest.dataBaseName:citrus-extension-test-database}")
    private String dataBaseName = "citrus-extension-test-database";
    @Value("${cosmosClientTest.containerName:citrus-extension-test-container}")
    private String containerName = "citrus-extension-test-container";
    @Value("${cosmosClientTest.partitionKey:/id}")
    private String partitionKey = "/id";

    @Bean
    public TestDataLoader testDataLoader() {
        return TestDataLoaders.json()
                .testDataPath("classpath:cosmos-ext-test-runner-test-data")
                .clazz(CosmosTestData.class)
                .recursiveSearch(false)
                .build();
    }

    @Bean
    public CosmosClient cosmosClient() {
        return CitrusExtensionEndpoints.cosmos()
                .endpoint(cosmosUri)
                .key(cosmosKey)
                .build();
    }

    @Bean
    public String dataBaseName() {
        return dataBaseName;
    }

    @Bean
    public String containerName() {
        return containerName;
    }

    @Bean
    public String partitionKey() {
        return partitionKey;
    }

    @Bean
    public TestDataLoader testDataLoaderHttp() {
        return TestDataLoaders.json()
                .testDataPath("classpath:http-ext-test-runner-test-data")
                .clazz(TestData.class)
                .recursiveSearch(false)
                .build();
    }

    @Bean
    public HttpClient httpClient() {
        return CitrusExtensionEndpoints.http()
                .client()
                .requestUrl("https://petstore.swagger.io")
                .build();
    }

    @Bean
    public MyBeforeSuiteTestRunner myBeforeSuiteTestRunner() {
        return new MyBeforeSuiteTestRunner();
    }

    @Bean
    public MyAfterSuiteTestRunner myAfterSuiteTestRunner() {
        return new MyAfterSuiteTestRunner();
    }

}
