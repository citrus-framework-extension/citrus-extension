package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.extension.data.TestDataLoader;
import com.consol.citrus.extension.dsl.TestConfig;
import com.consol.citrus.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {TestConfig.class})
public class HttpExtTestRunnerTest extends TestNGCitrusExtensionTestRunner {

    @Autowired
    private TestDataLoader testDataLoaderHttp;
    @Autowired
    private HttpClient httpClient;

    @CitrusTest
    @Test(dataProvider = "testDataProvider")
    public void testHttpExtTestRunner(TestData testData) {
        echo("Test data: " + testData);

        httpExt(h -> h.client(httpClient)
                .send()
                .message(testData.getHttpRequestMessage()));

        httpExt(h -> h.client(httpClient)
                .receive()
                .message(testData.getHttpControlMessage()));
    }

    @DataProvider
    public Object[][] testDataProvider() {
        return testDataLoaderHttp.getTestData();
    }

}
