package com.consol.citrus.extension.dsl.runner;

import com.consol.citrus.actions.AbstractTestAction;
import com.consol.citrus.context.TestContext;

public class MyBeforeSuiteTestRunner extends TestRunnerExtensionBeforeSuiteSupport {

    @SuppressWarnings("FieldMayBeFinal")
    private CounterTestAction counterTestAction = new CounterTestAction();

    @Override
    public void beforeSuite(TestRunnerExtension runner) {
        runner.echo("This action should be executed before suite");
        runner.run(counterTestAction);
    }

    public int getExecutionCount() {
        return counterTestAction.getCounter();
    }

    private static class CounterTestAction extends AbstractTestAction {

        private int counter = 0;

        @Override
        public void doExecute(TestContext context) {
            counter++;
        }

        /**
         * Gets the value of the counter property.
         *
         * @return the counter
         */
        public int getCounter() {
            return counter;
        }
    }

}
