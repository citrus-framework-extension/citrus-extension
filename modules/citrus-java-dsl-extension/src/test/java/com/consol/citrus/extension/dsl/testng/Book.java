package com.consol.citrus.extension.dsl.testng;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unused")
public class Book {

    private String id;
    private String title;

    public Book() {
        this("1", "");
    }

    public Book(long id, String title) {
        this(Long.toString(id), title);
    }

    public Book(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        ObjectMapper objectMapper = new ObjectMapper();
        String payload = "";
        try {
            payload = objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException ignored) {
        }
        return payload;
    }
}
