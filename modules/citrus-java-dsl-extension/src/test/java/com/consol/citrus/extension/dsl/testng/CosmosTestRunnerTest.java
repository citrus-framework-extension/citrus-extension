package com.consol.citrus.extension.dsl.testng;

import com.consol.citrus.annotations.CitrusTest;
import com.consol.citrus.extension.cosmosdb.client.CosmosClient;
import com.consol.citrus.extension.cosmosdb.client.CosmosStatus;
import com.consol.citrus.extension.data.TestDataLoader;
import com.consol.citrus.extension.dsl.TestConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(classes = {TestConfig.class})
public class CosmosTestRunnerTest extends TestNGCitrusExtensionTestRunner {

    @Autowired
    private CosmosClient cosmosClientEndpoint;
    @Autowired
    private String dataBaseName;
    @Autowired
    private String containerName;
    @Autowired
    private String partitionKey;
    @Autowired
    private TestDataLoader testDataLoader;

    private void createDataBaseIfNotExistsAndCheck() {
        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .createIfNotExists());

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(201)
                .database(dataBaseName)
                .createIfNotExists());
    }

    private void dropDatabaseAndCheck() {
        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .drop());

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(204)
                .database(dataBaseName)
                .drop());
    }

    private void createContainerIfNotExists() {
        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .createIfNotExists(partitionKey));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(201)
                .database(dataBaseName)
                .container(containerName)
                .createIfNotExists(partitionKey));
    }

    private Book createItem(int id) {
        Book book = new Book(id, "Some book");

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .createItem(Book.class, book));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(201)
                .database(dataBaseName)
                .container(containerName)
                .createItem(Book.class, book));

        return book;
    }

    private Book createItem() {
        return createItem(1);
    }

    @CitrusTest
    @Test
    public void testCreateDataBaseIfNotExists_and_DeleteIt() {
        createDataBaseIfNotExistsAndCheck();

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateDataBaseIfNotExists_and_DeleteIt_HttpStatus() {
        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .createIfNotExists());

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(HttpStatus.CREATED)
                .database(dataBaseName)
                .createIfNotExists());

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateDataBaseIfNotExists_and_DeleteIt_CosmosStatus() {
        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .createIfNotExists());

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(CosmosStatus.CREATED)
                .database(dataBaseName)
                .createIfNotExists());

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateDataBase_and_DeleteIt() {
        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .create());

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(201)
                .database(dataBaseName)
                .create());

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateContainer_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .create(partitionKey));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(201)
                .database(dataBaseName)
                .container(containerName)
                .create(partitionKey));

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateContainerIfNotExists_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();

        createContainerIfNotExists();

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateContainerIfNotExists_then_DropIt_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .drop());

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(204)
                .database(dataBaseName)
                .container(containerName)
                .drop());

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testCreateItem_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();

        createItem();

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testReadItem_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();
        Book book = createItem();

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .readItem(Book.class, book.getId()));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(200)
                .database(dataBaseName)
                .container(containerName)
                .readItem(Book.class, book.getId())
                .payload(book.toString()));

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testQueryItems_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();
        Book book = createItem();
        final String query = "SELECT * FROM c WHERE c.id = '" + book.getId() + "'";

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, query));

        List<Book> books = new ArrayList<>();
        books.add(book);

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(200)
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, query)
                .payload(books, new ObjectMapper()));

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testQueryItemsWithUseOfVariable_and_DeleteDatabase() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();
        Book book = createItem();

        variable("id", 1);
        String query = "SELECT * FROM c WHERE c.id = '${id}'";

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, query));

        List<Book> books = new ArrayList<>();
        books.add(book);

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(200)
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, query)
                .payload(books, new ObjectMapper()));

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testDeleteItem() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();
        Book book = createItem(1);

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .deleteItem(book.getId()));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(CosmosStatus.NO_CONTENT)
                .database(dataBaseName)
                .container(containerName)
                .deleteItem(book.getId())
                .payload(""));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, "SELECT * FROM c"));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(200)
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, "SELECT * FROM c")
                .payload("[]"));

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test
    public void testDeleteItems() {
        createDataBaseIfNotExistsAndCheck();
        createContainerIfNotExists();

        for (int i = 0; i < 5; i++) {
            createItem(i);
        }

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, "SELECT * FROM c"));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(200)
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, "SELECT * FROM c")
                .extractFromPayload("$[*].id", "listOfItemsToDelete"));

        String listOfItemsToDelete = extractVariable("listOfItemsToDelete").toString();
        listOfItemsToDelete = listOfItemsToDelete.replace("[", "");
        listOfItemsToDelete = listOfItemsToDelete.replace("]", "");

        String[] arrayOfItemsToDelete = listOfItemsToDelete.split(",");

        for (String s : arrayOfItemsToDelete) {
            String idOfElementToDelete = s.replace("\"", "");

            cosmos(c -> c.client(cosmosClientEndpoint)
                    .send()
                    .database(dataBaseName)
                    .container(containerName)
                    .deleteItem(idOfElementToDelete));

            cosmos(c -> c.client(cosmosClientEndpoint)
                    .receive()
                    .response(CosmosStatus.NO_CONTENT)
                    .database(dataBaseName)
                    .container(containerName)
                    .deleteItem(idOfElementToDelete)
                    .payload(""));
        }

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, "SELECT * FROM c"));

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .response(200)
                .database(dataBaseName)
                .container(containerName)
                .queryItems(Book.class, "SELECT * FROM c")
                .payload("[]"));

        dropDatabaseAndCheck();
    }

    @CitrusTest
    @Test(dataProvider = "dataProviderForTestSendMessageBaseOnData")
    public void testSendMessageBaseOnData(CosmosTestData cosmosTestData) {
        cosmosTestData
                .getCosmosRequestMessage()
                .databaseName(cosmosTestData
                        .getCosmosRequestMessage()
                        .getDatabaseName() + dataBaseName);

        cosmos(c -> c.client(cosmosClientEndpoint)
                .send()
                .message(cosmosTestData.getCosmosRequestMessage()));

        cosmosTestData
                .getCosmosControlMessage()
                .databaseName(cosmosTestData
                        .getCosmosControlMessage()
                        .getDatabaseName() + dataBaseName);

        cosmos(c -> c.client(cosmosClientEndpoint)
                .receive()
                .message(cosmosTestData.getCosmosControlMessage()));
    }

    @DataProvider
    public Object[][] dataProviderForTestSendMessageBaseOnData() {
        return testDataLoader.getTestData();
    }

}
