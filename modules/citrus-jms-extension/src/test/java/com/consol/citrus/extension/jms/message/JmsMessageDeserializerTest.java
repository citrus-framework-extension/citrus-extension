package com.consol.citrus.extension.jms.message;

import com.consol.citrus.jms.message.JmsMessage;
import com.consol.citrus.message.MessageHeaders;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class JmsMessageDeserializerTest extends AbstractTestNGUnitTest {

    private ObjectMapper objectMapper;

    @BeforeMethod
    public void setUpBeforeTestMethod() {
        objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addDeserializer(JmsMessage.class, new JmsMessageDeserializer());
        objectMapper.registerModule(module);
    }

    protected void assertJmsMessage(JmsMessage defaultMessage) {
        Assert.assertNotNull(defaultMessage);
        Assert.assertNotNull(defaultMessage.getHeader(MessageHeaders.ID));
        Assert.assertNotNull(defaultMessage.getHeader(MessageHeaders.TIMESTAMP));
    }

    @Test
    public void testJmsMessageDeserializer() throws IOException {
        String message = "{\n" +
                "  \"payload\": \"Some payload\",\n" +
                "  \"headers\": {\n" +
                "    \"h1\": \"v1\",\n" +
                "    \"h2\": \"v2\"\n" +
                "  }\n" +
                "}";

        JmsMessage jmsMessage = objectMapper.readValue(message, JmsMessage.class);

        assertJmsMessage(jmsMessage);
        Assert.assertEquals(jmsMessage.getPayload(), "Some payload");
        Assert.assertEquals(jmsMessage.getHeader("h1"), "v1");
        Assert.assertEquals(jmsMessage.getHeader("h2"), "v2");
    }

    @Test
    public void testJmsMessageDeserializerWithJmsHeaders() throws IOException {
        String message = "{\n" +
                "  \"payload\": \"Jms payload\",\n" +
                "  \"headers\": {\n" +
                "    \"citrus_jms_messageId\": \"citrus_jms_messageId\",\n" +
                "    \"citrus_jms_correlationId\": \"citrus_correlationId\",\n" +
                "    \"citrus_jms_replyTo\": \"org.apache.activemq.command.ActiveMQTopic:my.topic\",\n" +
                "    \"citrus_jms_redelivered\": \"citrus_jms_redelivered\",\n" +
                "    \"citrus_jms_priority\": \"citrus_jms_priority\",\n" +
                "    \"citrus_jms_destination\": \"citrus_jms_destination\",\n" +
                "    \"citrus_jms_deliveryMode\": \"citrus_jms_deliveryMode\",\n" +
                "    \"citrus_jms_expiration\": \"citrus_jms_expiration\",\n" +
                "    \"citrus_jms_type\": \"citrus_jms_type\",\n" +
                "    \"citrus_jms_timestamp\": 12345\n" +
                "  }\n" +
                "}";

        JmsMessage jmsMessage = objectMapper.readValue(message, JmsMessage.class);

        assertJmsMessage(jmsMessage);
        Assert.assertEquals(jmsMessage.getPayload(), "Jms payload");
        Assert.assertEquals(jmsMessage.getMessageId(), "citrus_jms_messageId");
        Assert.assertEquals(jmsMessage.getCorrelationId(), "citrus_correlationId");
        Assert.assertEquals(jmsMessage.getReplyTo().toString(), "topic://my.topic");
        Assert.assertEquals(jmsMessage.getType(), "citrus_jms_type");
        Assert.assertEquals(jmsMessage.getTimestamp().longValue(), 12345L);
    }

}
