package com.consol.citrus.extension.jms.message;

import com.consol.citrus.extension.exceptions.CitrusExtensionRuntimeException;
import com.consol.citrus.extension.message.DefaultMessageDeserializer;
import com.consol.citrus.jms.message.JmsMessage;
import com.consol.citrus.message.DefaultMessage;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

import javax.jms.Destination;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static com.consol.citrus.jms.message.JmsMessageHeaders.REPLY_TO;

/**
 * Deserializer for citrus JmsMessage.
 *
 * <p>
 * Header <i>citrus_jms_replyTo</i> should be provide in following pattern: <i>nameOfTheDestinationClass:destinationName</i>.
 * Example: org.apache.activemq.command.ActiveMQTopic:my.topic
 * <p>
 *
 * @author Mariusz Marciniuk
 * @since 0.0.13
 */
public class JmsMessageDeserializer extends DefaultMessageDeserializer<JmsMessage> {

    /**
     * Default constructor.
     */
    public JmsMessageDeserializer() {
        this(null);
    }

    /**
     * Constructor with Class parameter.
     *
     * @param vc Class
     */
    public JmsMessageDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public JmsMessage deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        DefaultMessage defaultMessage = super.deserialize(p, ctxt);
        JmsMessage jmsMessage = new JmsMessage(defaultMessage);

        if (jmsMessage.getHeader(REPLY_TO) != null && jmsMessage.getHeader(REPLY_TO) instanceof String) {
            String clazz = jmsMessage.getHeader(REPLY_TO).toString().split(":")[0];
            String destinationName = jmsMessage.getHeader(REPLY_TO).toString().split(":")[1];
            try {
                Class<?> destinationClass = Class.forName(clazz);
                Constructor<?> destinationConstructor = destinationClass.getConstructor(String.class);
                Destination destination = (Destination) destinationConstructor.newInstance(destinationName);
                jmsMessage.replyTo(destination);
            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException exception) {
                throw new CitrusExtensionRuntimeException(exception);
            }
        }

        return jmsMessage;
    }
}
