# If emulator was started with /AllowNetworkAccess, replace the below with the actual IP address of it:
EMULATOR_HOST=localhost
EMULATOR_PORT=8081
EMULATOR_CERT_PATH=/tmp/cosmos_emulator.pem
openssl s_client -showcerts -connect ${EMULATOR_HOST}:${EMULATOR_PORT} < /dev/null | openssl x509 -outform PEM > $EMULATOR_CERT_PATH
# delete the cert if already exists
sudo $JAVA_HOME/bin/keytool -delete -alias cosmos_emulator
# import the cert
sudo $JAVA_HOME/bin/keytool -importcert -alias cosmos_emulator -file $EMULATOR_CERT_PATH
