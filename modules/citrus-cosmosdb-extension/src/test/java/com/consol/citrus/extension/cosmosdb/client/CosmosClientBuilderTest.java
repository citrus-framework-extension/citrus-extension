package com.consol.citrus.extension.cosmosdb.client;

import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosmosClientBuilderTest extends AbstractTestNGUnitTest {

    @Test
    public void testSetEndpoint() {
        String cosmosUrl = "url";
        CosmosClientBuilder cosmosClientBuilder = new CosmosClientBuilder();

        cosmosClientBuilder.endpoint(cosmosUrl);
        CosmosClient cosmosClient = cosmosClientBuilder.getEndpoint();

        Assert.assertEquals(cosmosClient.getEndpointConfiguration().getEndpoint(), cosmosUrl);
    }

    @Test
    public void testSetKey() {
        String key = "key";
        CosmosClientBuilder cosmosClientBuilder = new CosmosClientBuilder();

        cosmosClientBuilder.key(key);
        CosmosClient cosmosClient = cosmosClientBuilder.getEndpoint();

        Assert.assertEquals(cosmosClient.getEndpointConfiguration().getKey(), key);
    }

}
