package com.consol.citrus.extension.cosmosdb.client;

@SuppressWarnings("unused")
public class BookIncorrect {

    private String id;
    private int title;

    public BookIncorrect() {
        this("", 0);
    }

    public BookIncorrect(String id, int title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "BookIncorrect{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
