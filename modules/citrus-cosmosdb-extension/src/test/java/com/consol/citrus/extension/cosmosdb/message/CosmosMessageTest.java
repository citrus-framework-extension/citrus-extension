package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.extension.cosmosdb.client.CosmosOperation;
import com.consol.citrus.extension.cosmosdb.client.CosmosStatus;
import org.springframework.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosmosMessageTest {

    @Test
    public void testCosmosPayload() {
        String payload = "cosmos-payload";
        CosmosMessage cosmosMessage = new CosmosMessage(payload);

        Assert.assertEquals(cosmosMessage.getPayload().toString(), payload);
    }

    @Test
    public void testStatusCode() {
        int statusCode = 200;
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.statusCode(statusCode);

        Assert.assertEquals(cosmosMessage.getStatusCode().intValue(), statusCode);
    }

    @Test
    public void testStatusWithUseOfHttpStatus() {
        HttpStatus status = HttpStatus.ACCEPTED;
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.status(status);

        Assert.assertEquals(cosmosMessage.getStatusCode().intValue(), status.value());
        Assert.assertEquals(cosmosMessage.getReasonPhrase(), status.getReasonPhrase());
    }

    @Test
    public void testStatusWithUseOfCosmosStatus() {
        CosmosStatus status = CosmosStatus.NOT_FOUND;
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.status(status);

        Assert.assertEquals(cosmosMessage.getStatusCode().intValue(), status.value());
        Assert.assertEquals(cosmosMessage.getReasonPhrase(), status.getReasonPhrase());
    }

    @Test
    public void testStatusCodeNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getStatusCode());
    }

    @Test
    public void testReasonPhrase() {
        CosmosStatus status = CosmosStatus.CONFLICT;
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.reasonPhrase(status.getReasonPhrase());

        Assert.assertEquals(cosmosMessage.getReasonPhrase(), status.getReasonPhrase());
    }

    @Test
    public void testReasonPhraseNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getReasonPhrase());
    }

    @Test
    public void testCosmosOperation() {
        CosmosOperation cosmosOperation = CosmosOperation.CREATE_DATABASE;
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.cosmosOperation(cosmosOperation);

        Assert.assertEquals(cosmosMessage.getCosmosOperation(), cosmosOperation);
    }

    @Test
    public void testCosmosOperationNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getCosmosOperation());
    }

    @Test
    public void testDatabaseName() {
        String databaseName = "database";
        CosmosMessage cosmosMessage = new CosmosMessage();
        cosmosMessage.databaseName(databaseName);
        Assert.assertEquals(cosmosMessage.getDatabaseName(), databaseName);
    }

    @Test
    public void testDatabaseNameNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getDatabaseName());
    }

    @Test
    public void testContainerName() {
        String containerName = "containerName";
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.containerName(containerName);

        Assert.assertEquals(cosmosMessage.getContainerName(), containerName);
    }

    @Test
    public void testContainerNameNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getContainerName());
    }

    @Test
    public void testPartitionKey() {
        String partitionKey = "/id";
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.partitionKey(partitionKey);

        Assert.assertEquals(cosmosMessage.getPartitionKey(), partitionKey);
    }

    @Test
    public void testPartitionKeyNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getPartitionKey());
    }

    @Test
    public void testItemClass() {
        Class<?> itemClass = Object.class;
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.itemClass(itemClass);

        Assert.assertEquals(cosmosMessage.getItemClass(), itemClass);
    }

    @Test
    public void testItemClassNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getItemClass());
    }

    @Test
    public void testReadItemId() {
        String id = "id";
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.itemId(id);

        Assert.assertEquals(cosmosMessage.getItemId(), id);
    }

    @Test
    public void testReadItemIdNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getItemId());
    }

    @Test
    public void testQuery() {
        String query = "SELECT * FROM e";
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.query(query);

        Assert.assertEquals(cosmosMessage.getQuery(), query);
    }

    @Test
    public void testQueryNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getQuery());
    }

    @Test
    public void testEndpoint() {
        String endpoint = "https://localhost:8080";
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.endpoint(endpoint);

        Assert.assertEquals(cosmosMessage.getEndpoint(), endpoint);
    }

    @Test
    public void testEndpointNull() {
        CosmosMessage cosmosMessage = new CosmosMessage();
        Assert.assertNull(cosmosMessage.getEndpoint());
    }

}
