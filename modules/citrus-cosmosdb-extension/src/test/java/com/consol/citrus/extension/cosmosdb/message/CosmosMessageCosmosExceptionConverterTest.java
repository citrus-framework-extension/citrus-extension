package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosmosMessageCosmosExceptionConverterTest extends AbstractTestNGUnitTest {

    @Test(expectedExceptions = {UnsupportedOperationException.class},
            expectedExceptionsMessageRegExp = "CosmosMessageCosmosExceptionConverter does not support CosmosMessage 'convertOutbound'")
    public void testConvertOutboundExceptionWhichReturnsCosmosMessage() {
        CosmosMessageCosmosExceptionConverter cosmosMessageCosmosExceptionConverter = new CosmosMessageCosmosExceptionConverter();
        cosmosMessageCosmosExceptionConverter.convertOutbound(new CosmosMessage(), new CosmosEndpointConfiguration(), context);
    }

    @Test(expectedExceptions = {UnsupportedOperationException.class},
            expectedExceptionsMessageRegExp = "CosmosMessageCosmosExceptionConverter does not support CosmosMessage objects")
    public void testConvertOutboundException() {
        CosmosMessageCosmosExceptionConverter cosmosMessageCosmosExceptionConverter = new CosmosMessageCosmosExceptionConverter();
        cosmosMessageCosmosExceptionConverter.convertOutbound(new CosmosMessage(), new CosmosMessage(), new CosmosEndpointConfiguration(), context);
    }

    @Test
    public void testConvertInbound() {
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        Exception exception = new UnsupportedOperationException("Some exception");
        CosmosMessageCosmosExceptionConverter cosmosMessageCosmosExceptionConverter = new CosmosMessageCosmosExceptionConverter();

        CosmosMessage cosmosMessage = (CosmosMessage) cosmosMessageCosmosExceptionConverter.convertInbound(exception, endpointConfiguration, context);

        Assert.assertEquals(cosmosMessage.getStatusCode(), new Integer(500));
        Assert.assertNotNull(cosmosMessage.getPayload());
        logger.info(cosmosMessage);
    }

}
