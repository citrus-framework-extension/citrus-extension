package com.consol.citrus.extension.cosmosdb.client;

@SuppressWarnings("unused")
public class Book {

    private String id;
    private String title;

    public Book() {
        this(0, "");
    }

    public Book(long id, String title) {
        this.id = Long.toString(id);
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
