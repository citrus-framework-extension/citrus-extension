package com.consol.citrus.extension.cosmosdb.client;

import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.springframework.beans.factory.annotation.Value;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.consol.citrus.extension.cosmosdb.client.CosmosOperation.*;

@SuppressWarnings("FieldMayBeFinal")
public class CosmosClientTest extends AbstractTestNGUnitTest {

    @Value("${cosmosClientTest.cosmosUri:https://localhost:8081/}")
    private String cosmosUri = "https://localhost:8081/";
    @Value("${cosmosClientTest.cosmosKey:C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==}")
    private String cosmosKey = "C2y6yDjf5/R+ob0N8A7Cgv30VRDJIWEHLM+4QDU5DE2nQ9nDuVTqobD4b8mGGyPMbIZnqyMsEcaGQy67XIw/Jw==";
    @Value("${cosmosClientTest.dataBaseName:citrus-extension-test-database}")
    private String dataBaseName = "citrus-extension-test-database";
    @Value("${cosmosClientTest.containerName:citrus-extension-test-container}")
    private String containerName = "citrus-extension-test-container";

    @Test
    public void testCreatingInstanceOfClient() {
        CosmosClient cosmosClient = new CosmosClient();
        Assert.assertNotNull(cosmosClient);
    }

    private void createDataBaseIfNotExists() {
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setCosmosOperation(CREATE_DATABASE_IF_NOT_EXISTS);

        cosmosClient.send(requestMessage, context);
        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);

        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertTrue(responseMessage.getStatusCode() == 200 || responseMessage.getStatusCode() == 201);
    }

    private void createContainerIfNotExists() {
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();
        requestMessage.partitionKey("/id");

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);
        endpointConfiguration.setCosmosOperation(CREATE_CONTAINER_IF_NOT_EXISTS);

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertTrue(responseMessage.getStatusCode() == 200 || responseMessage.getStatusCode() == 201);
    }

    private Book createItem() {
        Book book = new Book(1, "Some book");

        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();
        requestMessage.partitionKey("/id");
        requestMessage.setPayload(book);
        requestMessage.itemClass(Book.class);

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);
        endpointConfiguration.setCosmosOperation(CREATE_ITEM);

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertTrue(responseMessage.getStatusCode() == 200 || responseMessage.getStatusCode() == 201);

        return book;
    }

    private void dropDataBase() {
        CosmosEndpointConfiguration endpointConfigurationDrop = new CosmosEndpointConfiguration();
        endpointConfigurationDrop.setEndpoint(cosmosUri);
        endpointConfigurationDrop.setKey(cosmosKey);
        endpointConfigurationDrop.setDatabaseName(dataBaseName);
        endpointConfigurationDrop.setCosmosOperation(DROP_DATABASE);

        CosmosClient cosmosClientDrop = new CosmosClient(endpointConfigurationDrop);
        CosmosMessage requestMessageDrop = new CosmosMessage();

        cosmosClientDrop.send(requestMessageDrop, context);

        CosmosMessage responseMessageDrop = (CosmosMessage) cosmosClientDrop.receive(context, endpointConfigurationDrop.getTimeout());
        logger.info(responseMessageDrop);
        Assert.assertNotNull(responseMessageDrop.getStatusCode());
        Assert.assertEquals(responseMessageDrop.getStatusCode(), new Integer(204));
    }

    @Test
    public void testCREATE_DATABASE_and_DROP_DATABASE() {
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setCosmosOperation(CREATE_DATABASE);

        cosmosClient.send(requestMessage, context);
        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);

        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertTrue(responseMessage.getStatusCode() == 200 || responseMessage.getStatusCode() == 201);

        dropDataBase();
    }

    @Test
    public void testCREATE_DATABASE_IF_NOT_EXISTS_and_DROP_DATABASE() {
        createDataBaseIfNotExists();

        dropDataBase();
    }

    @Test
    public void testCREATE_DATABASE_IncorrectEndpointAndIncorrectKey() {
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();

        endpointConfiguration.setEndpoint("incorrectEndpoint");
        endpointConfiguration.setKey("incorrectKey");
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setCosmosOperation(CREATE_DATABASE);

        cosmosClient.send(requestMessage, context);
        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(requestMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertEquals(responseMessage.getStatusCode(), new Integer(500));
    }

    @Test
    public void testCREATE_CONTAINER_IF_NOT_EXISTS_and_DROP_DATABASE() {
        createDataBaseIfNotExists();

        createContainerIfNotExists();

        dropDataBase();
    }

    @Test
    public void testCREATE_CONTAINER_and_DROP_CONTAINER_then_DROP_DATABASE() {
        createDataBaseIfNotExists();

        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();
        requestMessage.partitionKey("/id");

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);
        endpointConfiguration.setCosmosOperation(CREATE_CONTAINER);

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertTrue(responseMessage.getStatusCode() == 200 || responseMessage.getStatusCode() == 201);

        CosmosEndpointConfiguration endpointConfigurationDropContainer = new CosmosEndpointConfiguration();
        CosmosClient cosmosClientDropContainer = new CosmosClient(endpointConfigurationDropContainer);
        CosmosMessage requestMessageDropContainer = new CosmosMessage();
        requestMessage.partitionKey("/id");

        endpointConfigurationDropContainer.setEndpoint(cosmosUri);
        endpointConfigurationDropContainer.setKey(cosmosKey);
        endpointConfigurationDropContainer.setDatabaseName(dataBaseName);
        endpointConfigurationDropContainer.setContainerName(containerName);
        endpointConfigurationDropContainer.setCosmosOperation(DROP_CONTAINER);

        cosmosClientDropContainer.send(requestMessageDropContainer, context);

        CosmosMessage responseMessageDropContainer = (CosmosMessage) cosmosClientDropContainer.receive(context, endpointConfigurationDropContainer.getTimeout());
        logger.info(responseMessageDropContainer);
        Assert.assertNotNull(responseMessageDropContainer.getStatusCode());
        Assert.assertEquals(responseMessageDropContainer.getStatusCode(), new Integer(204));

        dropDataBase();
    }

    @Test
    public void testCREATE_ITEM_then_DROP_DATABASE() {
        createDataBaseIfNotExists();
        createContainerIfNotExists();

        createItem();

        dropDataBase();
    }

    @Test
    public void testREAD_ITEM_then_DROP_DATABASE() {
        createDataBaseIfNotExists();
        createContainerIfNotExists();
        createItem();

        Book book = new Book(1, "Some book");

        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();
        requestMessage.partitionKey("/id");
        requestMessage.itemClass(Book.class);
        requestMessage.itemId(book.getId());

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);
        endpointConfiguration.setCosmosOperation(READ_ITEM);

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertTrue(responseMessage.getStatusCode() == 200 || responseMessage.getStatusCode() == 201);

        dropDataBase();
    }

    @Test
    public void testQUERY_ITEMS_then_DROP_DATABASE() {
        createDataBaseIfNotExists();
        createContainerIfNotExists();
        createItem();

        String query = "SELECT * FROM c WHERE c.id=\"1\"";
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();
        requestMessage.itemClass(Book.class);
        requestMessage.query(query);

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);
        endpointConfiguration.setCosmosOperation(QUERY_ITEMS);

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertEquals(responseMessage.getStatusCode(), new Integer(200));

        dropDataBase();
    }

    @Test
    public void testQUERY_ITEMS_incorrectModel_then_DROP_DATABASE() {
        createDataBaseIfNotExists();
        createContainerIfNotExists();
        createItem();

        String query = "SELECT * FROM c WHERE c.id=\"1\"";
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();
        requestMessage.itemClass(BookIncorrect.class);
        requestMessage.query(query);

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);
        endpointConfiguration.setCosmosOperation(QUERY_ITEMS);

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertEquals(responseMessage.getStatusCode(), new Integer(200));

        String payloadSoldContains = "Caused by: com.fasterxml.jackson.databind.exc.InvalidFormatException: Cannot deserialize value of type `int` from String \"Some book\": not a valid int value";
        Assert.assertTrue(responseMessage.getPayload(String.class).contains(payloadSoldContains));

        dropDataBase();
    }

    @Test
    public void testDELETE_ITEM_then_DROP_DATABASE() {
        createDataBaseIfNotExists();
        createContainerIfNotExists();
        Book book = createItem();

        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        CosmosClient cosmosClient = new CosmosClient(endpointConfiguration);
        CosmosMessage requestMessage = new CosmosMessage();

        endpointConfiguration.setEndpoint(cosmosUri);
        endpointConfiguration.setKey(cosmosKey);
        endpointConfiguration.setDatabaseName(dataBaseName);
        endpointConfiguration.setContainerName(containerName);

        requestMessage.cosmosOperation(DELETE_ITEM);
        requestMessage.itemId(book.getId());

        cosmosClient.send(requestMessage, context);

        CosmosMessage responseMessage = (CosmosMessage) cosmosClient.receive(context, endpointConfiguration.getTimeout());
        logger.info(responseMessage);
        Assert.assertNotNull(responseMessage.getStatusCode());
        Assert.assertEquals(responseMessage.getStatusCode().intValue(), CosmosStatus.NO_CONTENT.value());

        dropDataBase();
    }

}
