package com.consol.citrus.extension.cosmosdb.client;

import com.consol.citrus.endpoint.resolver.DynamicEndpointUriResolver;
import com.consol.citrus.endpoint.resolver.EndpointUriResolver;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageCosmosResponseConverter;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageCosmosExceptionConverter;
import com.consol.citrus.message.DefaultMessageCorrelator;
import com.consol.citrus.message.MessageCorrelator;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosmosEndpointConfigurationTest {

    @Test
    public void testSetterGetterEndpoint() {
        String endpoint = "https://localhost:8080";
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setEndpoint(endpoint);

        Assert.assertEquals(endpointConfiguration.getEndpoint(), endpoint);
    }

    @Test
    public void testSetterGetterKey() {
        String key = "soahweiotfh3o2403474ejf3iug213q";
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setKey(key);

        Assert.assertEquals(endpointConfiguration.getKey(), key);
    }

    @Test
    public void testSetterGetterCosmosOperation() {
        CosmosOperation cosmosOperation = CosmosOperation.CREATE_DATABASE_IF_NOT_EXISTS;
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setCosmosOperation(cosmosOperation);

        Assert.assertEquals(endpointConfiguration.getCosmosOperation(), cosmosOperation);
    }

    @Test
    public void testSetterGetterDatabaseName() {
        String databaseName = "databaseName";
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setDatabaseName(databaseName);

        Assert.assertEquals(endpointConfiguration.getDatabaseName(), databaseName);
    }

    @Test
    public void testSetterGetterCorrelationManager() {
        MessageCorrelator correlator = new DefaultMessageCorrelator();
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setCorrelator(correlator);

        Assert.assertEquals(endpointConfiguration.getCorrelator(), correlator);
    }

    @Test
    public void testSetterGetterEndpointUriResolver() {
        EndpointUriResolver endpointUriResolver = new DynamicEndpointUriResolver();
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setEndpointUriResolver(endpointUriResolver);

        Assert.assertEquals(endpointConfiguration.getEndpointUriResolver(), endpointUriResolver);
    }

    @Test
    public void testSetterGetterMessageCorrelator() {
        CosmosMessageCosmosResponseConverter cosmosMessageCosmosResponseConverter = new CosmosMessageCosmosResponseConverter();
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setCosmosMessageCosmosResponseConverter(cosmosMessageCosmosResponseConverter);

        Assert.assertEquals(endpointConfiguration.getCosmosMessageCosmosResponseConverter(), cosmosMessageCosmosResponseConverter);
    }

    @Test
    public void testSetterGetterContainerName() {
        String containerName = "containerName";
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setContainerName(containerName);

        Assert.assertEquals(endpointConfiguration.getContainerName(), containerName);
    }

    @Test
    public void testSetterGetter() {
        CosmosMessageCosmosExceptionConverter cosmosMessageCosmosExceptionConverter = new CosmosMessageCosmosExceptionConverter();
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();

        endpointConfiguration.setCosmosMessageCosmosExceptionConverter(cosmosMessageCosmosExceptionConverter);

        Assert.assertEquals(endpointConfiguration.getCosmosMessageCosmosExceptionConverter(), cosmosMessageCosmosExceptionConverter);
    }

}
