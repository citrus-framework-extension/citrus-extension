package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.consol.citrus.extension.cosmosdb.client.CosmosOperation;
import com.consol.citrus.message.DefaultMessage;
import com.consol.citrus.message.Message;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosmosMessageCosmosResponseConverterTest extends AbstractTestNGUnitTest {

    @Test
    public void testConvertOutbound() {
        String payload = "{\"test\": \"testData\"}";
        String databaseName = "databaseName";
        String containerName = "containerName";
        CosmosOperation cosmosOperation = CosmosOperation.CREATE_ITEM;

        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        endpointConfiguration.setCosmosOperation(cosmosOperation);
        endpointConfiguration.setDatabaseName(databaseName);
        endpointConfiguration.setContainerName(containerName);

        CosmosMessageCosmosResponseConverter converter = new CosmosMessageCosmosResponseConverter();
        CosmosMessage cosmosMessage = new CosmosMessage(payload);
        cosmosMessage.itemClass(payload.getClass());

        CosmosMessage cosmosMessageOutbound = converter.convertOutbound(cosmosMessage, endpointConfiguration, context);

        Assert.assertNull(cosmosMessageOutbound.getStatusCode());
        Assert.assertEquals(cosmosMessageOutbound.getCosmosOperation(), cosmosOperation);
        Assert.assertEquals(cosmosMessageOutbound.getDatabaseName(), databaseName);
        Assert.assertEquals(cosmosMessageOutbound.getContainerName(), containerName);
        Assert.assertNull(cosmosMessageOutbound.getPartitionKey());
        Assert.assertEquals(cosmosMessageOutbound.getItemClass(), payload.getClass());
        Assert.assertNull(cosmosMessageOutbound.getQuery());
        Assert.assertEquals(cosmosMessageOutbound.getPayload(), payload);
    }

    @Test
    public void testConvertOutboundMessage() {
        String payload = "{\"test\": \"testData\"}";
        String databaseName = "databaseName";
        String containerName = "containerName";
        CosmosOperation cosmosOperation = CosmosOperation.CREATE_ITEM;

        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        endpointConfiguration.setCosmosOperation(cosmosOperation);
        endpointConfiguration.setDatabaseName(databaseName);
        endpointConfiguration.setContainerName(containerName);

        CosmosMessageCosmosResponseConverter converter = new CosmosMessageCosmosResponseConverter();
        Message cosmosMessage = new DefaultMessage(payload);

        CosmosMessage cosmosMessageOutbound = converter.convertOutbound(cosmosMessage, endpointConfiguration, context);

        Assert.assertNull(cosmosMessageOutbound.getStatusCode());
        Assert.assertEquals(cosmosMessageOutbound.getCosmosOperation(), cosmosOperation);
        Assert.assertEquals(cosmosMessageOutbound.getDatabaseName(), databaseName);
        Assert.assertEquals(cosmosMessageOutbound.getContainerName(), containerName);
        Assert.assertNull(cosmosMessageOutbound.getPartitionKey());
        Assert.assertNull(cosmosMessageOutbound.getQuery());
        Assert.assertEquals(cosmosMessageOutbound.getPayload(), payload);
    }

    @Test(expectedExceptions = {UnsupportedOperationException.class},
            expectedExceptionsMessageRegExp = "CosmosMessageCosmosResponseConverter does not support CosmosMessage objects")
    public void testConvertOutboundException() {
        CosmosMessageCosmosResponseConverter responseConverter = new CosmosMessageCosmosResponseConverter();
        responseConverter.convertOutbound(new CosmosMessage(), new CosmosMessage(), new CosmosEndpointConfiguration(), context);
    }

}
