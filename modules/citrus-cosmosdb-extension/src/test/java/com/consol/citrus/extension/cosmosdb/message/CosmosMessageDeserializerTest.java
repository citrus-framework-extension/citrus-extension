package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.extension.cosmosdb.client.Book;
import com.consol.citrus.extension.cosmosdb.client.CosmosOperation;
import com.consol.citrus.message.MessageHeaders;
import com.consol.citrus.testng.AbstractTestNGUnitTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.consol.citrus.extension.cosmosdb.message.CosmosResponseMessageHeaders.COSMOS_ITEM_CLASS;

public class CosmosMessageDeserializerTest extends AbstractTestNGUnitTest {

    private ObjectMapper objectMapper;

    @BeforeMethod
    public void setUpBeforeTestMethod() {
        objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(CosmosMessage.class, new CosmosMessageDeserializer());
        objectMapper.registerModule(module);
    }

    protected void assertCosmosMessage(CosmosMessage cosmosMessage) {
        Assert.assertNotNull(cosmosMessage);
        Assert.assertNotNull(cosmosMessage.getHeader(MessageHeaders.ID));
        Assert.assertNotNull(cosmosMessage.getHeader(MessageHeaders.TIMESTAMP));
    }

    @Test
    public void testCosmosMessageDeserializer() throws IOException {
        String message = "{\n" +
                "  \"headers\": {\n" +
                "    \"citrus_cosmos_status_code\": 200,\n" +
                "    \"citrus_cosmos_reason_phrase\": \"OK\",\n" +
                "    \"citrus_cosmos_operation\": \"CREATE_DATABASE_IF_NOT_EXISTS\",\n" +
                "    \"citrus_cosmos_endpoint\": \"https://someHostName:443/\",\n" +
                "    \"citrus_cosmos_database_name\": \"someDatabaseName\",\n" +
                "    \"citrus_cosmos_container_name\": \"someContainerName\",\n" +
                "    \"citrus_cosmos_partition_key\": \"/id\",\n" +
                "    \"citrus_cosmos_item_class\": \"com.consol.citrus.extension.cosmosdb.client.Book\",\n" +
                "    \"citrus_cosmos_item_id\": \"/id\",\n" +
                "    \"citrus_cosmos_query\": \"SELECT * FROM c\"\n" +
                "  },\n" +
                "  \"payload\": \"Some payload\"\n" +
                "}";

        CosmosMessage cosmosMessage = objectMapper.readValue(message, CosmosMessage.class);

        assertCosmosMessage(cosmosMessage);
        Assert.assertEquals(cosmosMessage.getStatusCode().intValue(), 200);
        Assert.assertEquals(cosmosMessage.getReasonPhrase(), "OK");
        Assert.assertEquals(cosmosMessage.getCosmosOperation(), CosmosOperation.CREATE_DATABASE_IF_NOT_EXISTS);
        Assert.assertEquals(cosmosMessage.getEndpoint(), "https://someHostName:443/");
        Assert.assertEquals(cosmosMessage.getDatabaseName(), "someDatabaseName");
        Assert.assertEquals(cosmosMessage.getContainerName(), "someContainerName");
        Assert.assertEquals(cosmosMessage.getPartitionKey(), "/id");
        Assert.assertEquals(cosmosMessage.getItemClass(), Book.class);
        Assert.assertEquals(cosmosMessage.getItemId(), "/id");
        Assert.assertEquals(cosmosMessage.getQuery(), "SELECT * FROM c");
        Assert.assertEquals(cosmosMessage.getPayload(), "Some payload");
    }

    @Test
    public void testCosmosMessageDeserializerItemClassHeader() throws IOException {
        String message = "{\n" +
                "  \"headers\": {\n" +
                "    \"citrus_cosmos_item_class\": \"com.consol.citrus.extension.cosmosdb.client.Book\"\n" +
                "  }\n" +
                "}";

        CosmosMessage cosmosMessage = objectMapper.readValue(message, CosmosMessage.class);

        assertCosmosMessage(cosmosMessage);
        Assert.assertEquals(cosmosMessage.getHeader(COSMOS_ITEM_CLASS), Book.class);
    }

    @Test
    public void testCosmosMessageDeserializerItemClassHeaderNotExistingClass() throws IOException {
        String message = "{\n" +
                "  \"headers\": {\n" +
                "    \"citrus_cosmos_item_class\": \"com.consol.citrus.extension.cosmosdb.client.ExistingClass\"\n" +
                "  }\n" +
                "}";

        CosmosMessage cosmosMessage = objectMapper.readValue(message, CosmosMessage.class);

        assertCosmosMessage(cosmosMessage);
        Assert.assertNull(cosmosMessage.getHeader(COSMOS_ITEM_CLASS));
    }

}