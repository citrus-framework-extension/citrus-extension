package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.consol.citrus.extension.cosmosdb.client.CosmosOperation;
import com.consol.citrus.message.DefaultMessage;
import com.consol.citrus.message.Message;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CosmosMessageBaseAbstractConverterTest {

    @Test
    public void testConvertOutboundMessage() {
        String payload = "{ \"test\":\"someInfo\" }";
        CosmosMessage cosmosMessage = new CosmosMessage(payload);

        CosmosMessageBaseAbstractConverter<?> converter = new CosmosMessageBaseAbstractConverter<Object>() {
            @Override
            public CosmosMessage convertOutbound(Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
                return null;
            }

            @Override
            public void convertOutbound(CosmosMessage externalMessage, Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {

            }

            @Override
            public Message convertInbound(Object externalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
                return null;
            }
        };

        CosmosMessage cosmosMessageResponse = converter.convertOutboundMessage(cosmosMessage);

        Assert.assertEquals(cosmosMessageResponse.getPayload(), payload);
    }

    @Test
    public void testConvertOutboundMessageDefaultMessage() {
        String payload = "{ \"test\":\"someInfo\" }";
        Message message = new DefaultMessage(payload);

        CosmosMessageBaseAbstractConverter<?> converter = new CosmosMessageBaseAbstractConverter<Object>() {
            @Override
            public CosmosMessage convertOutbound(Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
                return null;
            }

            @Override
            public void convertOutbound(CosmosMessage externalMessage, Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {

            }

            @Override
            public Message convertInbound(Object externalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
                return null;
            }
        };

        CosmosMessage cosmosMessageResponse = converter.convertOutboundMessage(message);

        Assert.assertEquals(cosmosMessageResponse.getPayload(), message.getPayload());
    }

    @Test
    public void testEndpointConfigurationToCosmosMessage() {
        CosmosEndpointConfiguration endpointConfiguration = new CosmosEndpointConfiguration();
        endpointConfiguration.setEndpoint("endpoint");
        endpointConfiguration.setCosmosOperation(CosmosOperation.CREATE_DATABASE);
        endpointConfiguration.setDatabaseName("databaseName");
        endpointConfiguration.setContainerName("containerName");


        CosmosMessageBaseAbstractConverter<?> converter = new CosmosMessageBaseAbstractConverter<Object>() {
            @Override
            public CosmosMessage convertOutbound(Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
                return null;
            }

            @Override
            public void convertOutbound(CosmosMessage externalMessage, Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {

            }

            @Override
            public Message convertInbound(Object externalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
                return null;
            }
        };

        CosmosMessage cosmosMessageResponse = converter.endpointConfigurationToCosmosMessage(endpointConfiguration);

        Assert.assertEquals(cosmosMessageResponse.getCosmosOperation(), endpointConfiguration.getCosmosOperation());
        Assert.assertEquals(cosmosMessageResponse.getDatabaseName(), endpointConfiguration.getDatabaseName());
        Assert.assertEquals(cosmosMessageResponse.getContainerName(), endpointConfiguration.getContainerName());
        Assert.assertEquals(cosmosMessageResponse.getEndpoint(), endpointConfiguration.getEndpoint());
    }

}
