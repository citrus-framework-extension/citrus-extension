package com.consol.citrus.extension.cosmosdb.message;

import com.azure.cosmos.models.CosmosResponse;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * Message converter implementation able to convert Cosmos DB request and response entities to internal message
 * representation and other way round.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosMessageCosmosResponseConverter extends CosmosMessageBaseAbstractConverter<CosmosResponse<?>> {

    @Override
    public CosmosMessage convertInbound(CosmosResponse<?> externalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
        ObjectMapper objectMapper = new ObjectMapper();
        CosmosMessage cosmosMessage = endpointConfigurationToCosmosMessage(endpointConfiguration);
        cosmosMessage.statusCode(externalMessage.getStatusCode());

        for (Map.Entry<String, String> entry : externalMessage.getResponseHeaders().entrySet()) {
            cosmosMessage.setHeader(entry.getKey(), entry.getValue());
        }

        try {
            cosmosMessage.setPayload(objectMapper.writeValueAsString(externalMessage));
        } catch (JsonProcessingException ignored) {
        }

        return cosmosMessage;
    }

}
