package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.consol.citrus.message.Message;
import com.consol.citrus.message.MessageConverter;

/**
 * Abstract base Cosmos message converter implementation able to convert Cosmos DB.
 *
 * @param <I> Inbound message
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public abstract class CosmosMessageBaseAbstractConverter<I> implements MessageConverter<I, CosmosMessage, CosmosEndpointConfiguration> {

    /**
     * Converts the outbound Message object into a CosmosMessage
     *
     * @param message The message to convert
     * @return The converted message as CosmosMessage
     */
    protected CosmosMessage convertOutboundMessage(Message message) {
        CosmosMessage cosmosMessage;
        if (message instanceof CosmosMessage) {
            cosmosMessage = (CosmosMessage) message;
        } else {
            cosmosMessage = new CosmosMessage(message);
        }
        return cosmosMessage;
    }

    /**
     * Converts CosmosEndpointConfiguration into CosmosMessage
     *
     * @param endpointConfiguration CosmosEndpointConfiguration
     * @return CosmosMessage
     */
    protected CosmosMessage endpointConfigurationToCosmosMessage(CosmosEndpointConfiguration endpointConfiguration) {
        CosmosMessage cosmosMessage = new CosmosMessage();

        cosmosMessage.endpoint(endpointConfiguration.getEndpoint());
        cosmosMessage.cosmosOperation(endpointConfiguration.getCosmosOperation());
        cosmosMessage.databaseName(endpointConfiguration.getDatabaseName());
        cosmosMessage.containerName(endpointConfiguration.getContainerName());

        return cosmosMessage;
    }

    @Override
    public CosmosMessage convertOutbound(Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
        CosmosMessage cosmosMessage = convertOutboundMessage(internalMessage);

        if (cosmosMessage.getCosmosOperation() == null) {
            cosmosMessage.cosmosOperation(endpointConfiguration.getCosmosOperation());
        }

        if (cosmosMessage.getDatabaseName() == null) {
            cosmosMessage.databaseName(endpointConfiguration.getDatabaseName());
        }

        if (cosmosMessage.getContainerName() == null) {
            cosmosMessage.containerName(endpointConfiguration.getContainerName());
        }

        return cosmosMessage;
    }

    @Override
    public void convertOutbound(CosmosMessage externalMessage,
                                Message internalMessage,
                                CosmosEndpointConfiguration endpointConfiguration,
                                TestContext context) {
        throw new UnsupportedOperationException(this.getClass().getSimpleName() + " does not support CosmosMessage objects");
    }

}
