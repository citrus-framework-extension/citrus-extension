package com.consol.citrus.extension.cosmosdb.client;

import com.consol.citrus.endpoint.AbstractPollableEndpointConfiguration;
import com.consol.citrus.endpoint.resolver.DynamicEndpointUriResolver;
import com.consol.citrus.endpoint.resolver.EndpointUriResolver;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageCosmosItemResponseConverter;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageCosmosPagedIterableResponseConverter;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageCosmosResponseConverter;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessageCosmosExceptionConverter;
import com.consol.citrus.message.DefaultMessageCorrelator;
import com.consol.citrus.message.MessageCorrelator;

/**
 * Cosmos endpoint configuration.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosEndpointConfiguration extends AbstractPollableEndpointConfiguration {

    /**
     * Cosmos endpoint
     */
    private String endpoint;

    /**
     * Cosmos key
     */
    private String key;

    /**
     * Cosmos DB operation
     */
    private CosmosOperation cosmosOperation = CosmosOperation.CREATE_DATABASE_IF_NOT_EXISTS;

    /**
     * Cosmos database name.
     */
    private String databaseName;

    /**
     * Cosmos container name.
     */
    private String containerName;

    /**
     * Reply message correlator
     */
    private MessageCorrelator correlator = new DefaultMessageCorrelator();

    /**
     * Cosmos DB message converter.
     */
    private CosmosMessageCosmosResponseConverter cosmosMessageCosmosResponseConverter = new CosmosMessageCosmosResponseConverter();

    /**
     * Cosmos DB item message converter.
     */
    private CosmosMessageCosmosItemResponseConverter cosmosMessageCosmosItemResponseConverter = new CosmosMessageCosmosItemResponseConverter();

    /**
     * Cosmos DB paged message converter.
     */
    private CosmosMessageCosmosPagedIterableResponseConverter cosmosMessageCosmosPagedIterableResponseConverter = new CosmosMessageCosmosPagedIterableResponseConverter();

    /**
     * Cosmos DB exception message converter.
     */
    private CosmosMessageCosmosExceptionConverter cosmosMessageCosmosExceptionConverter = new CosmosMessageCosmosExceptionConverter();

    /**
     * Resolves dynamic endpoint uri
     */
    private EndpointUriResolver endpointUriResolver = new DynamicEndpointUriResolver();

    /**
     * Cosmos endpoint
     *
     * @return Cosmos endpoint
     */
    public String getEndpoint() {
        return endpoint;
    }

    /**
     * Cosmos endpoint
     *
     * @param endpoint Cosmos endpoint
     */
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * Returns cosmos key
     *
     * @return Cosmos key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets cosmos key
     *
     * @param key Cosmos key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Returns cosmos operation type.
     *
     * @return CosmosOperation
     */
    public CosmosOperation getCosmosOperation() {
        return cosmosOperation;
    }

    /**
     * Sets cosmos operation type.
     *
     * @param cosmosOperation Cosmos operation type
     */
    public void setCosmosOperation(CosmosOperation cosmosOperation) {
        this.cosmosOperation = cosmosOperation;
    }

    /**
     * Returns Cosmos database name.
     *
     * @return Cosmos database name
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Sets Cosmos database name.
     *
     * @param databaseName Cosmos database name
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * Returns Cosmos container name.
     *
     * @return Container name
     */
    public String getContainerName() {
        return containerName;
    }

    /**
     * Sets Cosmos container name
     *
     * @param containerName Cosmos container name
     */
    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    /**
     * Set the reply message correlator.
     *
     * @param correlator the correlator to set
     */
    public void setCorrelator(MessageCorrelator correlator) {
        this.correlator = correlator;
    }

    /**
     * Gets the correlator.
     *
     * @return the correlator
     */
    public MessageCorrelator getCorrelator() {
        return correlator;
    }

    /**
     * Sets the endpoint uri resolver.
     *
     * @param endpointUriResolver the endpointUriResolver to set
     */
    public void setEndpointUriResolver(EndpointUriResolver endpointUriResolver) {
        this.endpointUriResolver = endpointUriResolver;
    }

    /**
     * Gets the endpointUriResolver.
     *
     * @return the endpointUriResolver
     */
    public EndpointUriResolver getEndpointUriResolver() {
        return endpointUriResolver;
    }

    /**
     * Returns Cosmos message converter.
     *
     * @return CosmosMessageCosmosResponseConverter
     */
    public CosmosMessageCosmosResponseConverter getCosmosMessageCosmosResponseConverter() {
        return cosmosMessageCosmosResponseConverter;
    }

    /**
     * Sets Cosmos message converter.
     *
     * @param messageConverter CosmosMessageCosmosResponseConverter
     */
    public void setCosmosMessageCosmosResponseConverter(CosmosMessageCosmosResponseConverter messageConverter) {
        this.cosmosMessageCosmosResponseConverter = messageConverter;
    }

    /**
     * Returns Cosmos item message converter.
     *
     * @return CosmosMessageCosmosItemResponseConverter
     */
    public CosmosMessageCosmosItemResponseConverter getCosmosMessageCosmosItemResponseConverter() {
        return cosmosMessageCosmosItemResponseConverter;
    }

    /**
     * Stets Cosmos item message converter.
     *
     * @param cosmosMessageCosmosItemResponseConverter CosmosMessageCosmosItemResponseConverter
     */
    public void setCosmosMessageCosmosItemResponseConverter(CosmosMessageCosmosItemResponseConverter cosmosMessageCosmosItemResponseConverter) {
        this.cosmosMessageCosmosItemResponseConverter = cosmosMessageCosmosItemResponseConverter;
    }

    /**
     * Returns Cosmos paged message converter.
     *
     * @return CosmosMessageCosmosPagedIterableResponseConverter
     */
    public CosmosMessageCosmosPagedIterableResponseConverter getCosmosMessageCosmosPagedIterableResponseConverter() {
        return cosmosMessageCosmosPagedIterableResponseConverter;
    }

    /**
     * Stets Cosmos paged message converter.
     *
     * @param cosmosMessageCosmosPagedIterableResponseConverter CosmosMessageCosmosPagedIterableResponseConverter
     */
    public void setCosmosMessageCosmosPagedIterableResponseConverter(CosmosMessageCosmosPagedIterableResponseConverter cosmosMessageCosmosPagedIterableResponseConverter) {
        this.cosmosMessageCosmosPagedIterableResponseConverter = cosmosMessageCosmosPagedIterableResponseConverter;
    }

    /**
     * Returns Cosmos exception message converter
     *
     * @return CosmosMessageCosmosExceptionConverter
     */
    public CosmosMessageCosmosExceptionConverter getCosmosMessageCosmosExceptionConverter() {
        return cosmosMessageCosmosExceptionConverter;
    }

    /**
     * Sets Cosmos exception message converter.
     *
     * @param cosmosMessageCosmosExceptionConverter CosmosMessageCosmosExceptionConverter
     */
    public void setCosmosMessageCosmosExceptionConverter(CosmosMessageCosmosExceptionConverter cosmosMessageCosmosExceptionConverter) {
        this.cosmosMessageCosmosExceptionConverter = cosmosMessageCosmosExceptionConverter;
    }

}
