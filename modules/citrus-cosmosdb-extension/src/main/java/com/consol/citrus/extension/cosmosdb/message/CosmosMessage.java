package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.extension.cosmosdb.client.CosmosStatus;
import com.consol.citrus.extension.cosmosdb.client.CosmosOperation;
import com.consol.citrus.message.DefaultMessage;
import com.consol.citrus.message.Message;
import org.springframework.http.HttpStatus;

import static com.consol.citrus.extension.cosmosdb.message.CosmosResponseMessageHeaders.*;

/**
 * Cosmos response message.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
@SuppressWarnings("UnusedReturnValue")
public class CosmosMessage extends DefaultMessage {

    /**
     * Default constructor
     */
    public CosmosMessage() {
        super();
    }

    /**
     * Constructs copy of given message.
     *
     * @param message The base message for the copy operation.
     */
    public CosmosMessage(final Message message) {
        super(message);
    }

    /**
     * Constructor with parameter
     *
     * @param payload Payload
     */
    public CosmosMessage(final Object payload) {
        super(payload);
    }

    /**
     * Sets status code.
     *
     * @param statusCode Status code
     * @return The altered CosmosMessage
     */
    public CosmosMessage statusCode(final Integer statusCode) {
        setHeader(COSMOS_STATUS_CODE, statusCode);
        return this;
    }

    /**
     * Sets status code.
     *
     * @param status Status code
     * @return The altered CosmosMessage
     * @since 0.0.3
     */
    public CosmosMessage status(final HttpStatus status) {
        statusCode(status.value());
        reasonPhrase(status.getReasonPhrase());
        return this;
    }

    /**
     * Sets status code.
     *
     * @param status Status code
     * @return The altered CosmosMessage
     * @since 0.0.3
     */
    public CosmosMessage status(final CosmosStatus status) {
        statusCode(status.value());
        reasonPhrase(status.getReasonPhrase());
        return this;
    }

    /**
     * Returns status code.
     *
     * @return Status code
     */
    public Integer getStatusCode() {
        Object statusCode = getHeader(COSMOS_STATUS_CODE);

        if (statusCode != null) {
            if (statusCode instanceof Integer) {
                return (Integer) statusCode;
            } else {
                return Integer.valueOf(statusCode.toString());
            }
        }

        return null;
    }

    /**
     * Sets the Cosmos response reason phrase header.
     *
     * @param reasonPhrase The reason phrase header value to use
     * @return The altered CosmosMessage
     * @since 0.0.3
     */
    public CosmosMessage reasonPhrase(String reasonPhrase) {
        setHeader(COSMOS_REASON_PHRASE, reasonPhrase);
        return this;
    }

    /**
     * Gets the Cosmos response reason phrase.
     *
     * @return The reason phrase of the message
     */
    public String getReasonPhrase() {
        Object reasonPhrase = getHeader(COSMOS_REASON_PHRASE);
        if (reasonPhrase != null) {
            return reasonPhrase.toString();
        }
        return null;
    }

    /**
     * Sets cosmos operation type.
     *
     * @param cosmosOperation Cosmos operation
     * @return The altered CosmosMessage
     */
    public CosmosMessage cosmosOperation(final CosmosOperation cosmosOperation) {
        setHeader(COSMOS_OPERATION, cosmosOperation);
        return this;
    }

    /**
     * Returns cosmos operation.
     *
     * @return CosmosOperation
     */
    public CosmosOperation getCosmosOperation() {
        final Object cosmosOperation = getHeader(COSMOS_OPERATION);
        if (cosmosOperation != null) {
            return CosmosOperation.valueOf(cosmosOperation.toString());
        }
        return null;
    }

    /**
     * Sets Cosmos endpoint.
     *
     * @param endpoint Cosmos endpoint
     * @return The altered CosmosMessage
     */
    public CosmosMessage endpoint(final String endpoint) {
        setHeader(COSMOS_ENDPOINT, endpoint);
        return this;
    }

    /**
     * Returns Cosmos endpoint.
     *
     * @return Cosmos endpoint
     */
    public String getEndpoint() {
        final Object endpoint = getHeader(COSMOS_ENDPOINT);
        if (endpoint != null) {
            return endpoint.toString();
        }
        return null;
    }

    /**
     * Sets Cosmos database name.
     *
     * @param databaseName Cosmos database name
     * @return The altered CosmosMessage
     */
    public CosmosMessage databaseName(final String databaseName) {
        setHeader(COSMOS_DATABASE_NAME, databaseName);
        return this;
    }

    /**
     * Returns Cosmos database name.
     *
     * @return Database name
     */
    public String getDatabaseName() {
        final Object databaseName = getHeader(COSMOS_DATABASE_NAME);
        if (databaseName != null) {
            return databaseName.toString();
        }
        return null;
    }

    /**
     * Sets Cosmos container name
     *
     * @param containerName Cosmos container name
     * @return The altered CosmosMessage
     */
    public CosmosMessage containerName(final String containerName) {
        setHeader(COSMOS_CONTAINER_NAME, containerName);
        return this;
    }

    /**
     * Returns Cosmos container name
     *
     * @return Cosmos container name
     */
    public String getContainerName() {
        final Object containerName = getHeader(COSMOS_CONTAINER_NAME);
        if (containerName != null) {
            return containerName.toString();
        }
        return null;
    }

    /**
     * Sets Cosmos DB container partition key
     *
     * @param partitionKey Cosmos DB container partition key
     * @return The altered CosmosMessage
     */
    public CosmosMessage partitionKey(final String partitionKey) {
        setHeader(COSMOS_PARTITION_KEY, partitionKey);
        return this;
    }

    /**
     * Returns Cosmos DB container partition key
     *
     * @return Cosmos DB container partition key
     */
    public String getPartitionKey() {
        final Object partitionKey = getHeader(COSMOS_PARTITION_KEY);
        if (partitionKey != null) {
            return partitionKey.toString();
        }
        return null;
    }

    /**
     * Sets Cosmos DB item class.
     *
     * @return The altered CosmosMessage
     */
    public CosmosMessage itemClass(final Class<?> itemClass) {
        setHeader(COSMOS_ITEM_CLASS, itemClass);
        return this;
    }

    /**
     * Returns Cosmos DB item class.
     *
     * @return Cosmos DB item class.
     */
    public Class<?> getItemClass() {
        final Object itemClass = getHeader(COSMOS_ITEM_CLASS);
        if (itemClass != null) {
            if (itemClass instanceof String) {
                try {
                    return Class.forName(itemClass.toString());
                } catch (ClassNotFoundException e) {
                    return null;
                }
            }
            return (Class<?>) itemClass;
        }
        return null;
    }

    /**
     * Sets Cosmos DB item id.
     *
     * @return The altered CosmosMessage
     */
    public CosmosMessage itemId(final String id) {
        setHeader(COSMOS_ITEM_ID, id);
        return this;
    }

    /**
     * Returns Cosmos DB item id.
     *
     * @return Cosmos DB item id.
     */
    public String getItemId() {
        final Object readItemId = getHeader(COSMOS_ITEM_ID);
        if (readItemId != null) {
            return readItemId.toString();
        }
        return null;
    }

    /**
     * Sets Cosmos DB query.
     *
     * @param query Cosmos DB query
     * @return The altered CosmosMessage
     */
    public CosmosMessage query(final String query) {
        setHeader(COSMOS_QUERY, query);
        return this;
    }

    /**
     * Returns Cosmos DB query.
     *
     * @return Cosmos DB query
     */
    public String getQuery() {
        final Object query = getHeader(COSMOS_QUERY);
        if (query != null) {
            return query.toString();
        }
        return null;
    }

}
