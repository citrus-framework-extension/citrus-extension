package com.consol.citrus.extension.cosmosdb.client;

import com.azure.cosmos.CosmosClientBuilder;
import com.azure.cosmos.CosmosContainer;
import com.azure.cosmos.CosmosDatabase;
import com.azure.cosmos.models.*;
import com.azure.cosmos.util.CosmosPagedIterable;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.endpoint.AbstractEndpoint;
import com.consol.citrus.exceptions.ActionTimeoutException;
import com.consol.citrus.extension.cosmosdb.message.CosmosMessage;
import com.consol.citrus.message.Message;
import com.consol.citrus.message.correlation.CorrelationManager;
import com.consol.citrus.message.correlation.PollingCorrelationManager;
import com.consol.citrus.messaging.Producer;
import com.consol.citrus.messaging.ReplyConsumer;
import com.consol.citrus.messaging.SelectiveConsumer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.consol.citrus.extension.cosmosdb.client.CosmosOperation.*;

/**
 * Cosmos client sends messages via Http protocol to some Cosmos DB instance, defined by a request endpoint url. Synchronous response
 * messages are cached in local memory and receive operations are able to fetch responses from this cache later on.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosClient extends AbstractEndpoint implements Producer, ReplyConsumer {

    /**
     * Logger
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Store of reply messages
     */
    private CorrelationManager<Message> correlationManager;

    /**
     * Default constructor initializing endpoint configuration.
     */
    public CosmosClient() {
        super(new CosmosEndpointConfiguration());
        this.correlationManager = new PollingCorrelationManager<>(getEndpointConfiguration(), "Reply message did not arrive yet");
    }

    /**
     * Default constructor using endpoint configuration.
     *
     * @param endpointConfiguration CosmosEndpointConfiguration
     */
    public CosmosClient(CosmosEndpointConfiguration endpointConfiguration) {
        super(endpointConfiguration);
        this.correlationManager = new PollingCorrelationManager<>(endpointConfiguration, "Reply message did not arrive yet");
    }

    @Override
    public CosmosEndpointConfiguration getEndpointConfiguration() {
        return (CosmosEndpointConfiguration) super.getEndpointConfiguration();
    }

    @Override
    public void send(Message message, TestContext context) {
        CosmosMessage cosmosMessage;
        if (message instanceof CosmosMessage) {
            cosmosMessage = (CosmosMessage) message;
        } else {
            cosmosMessage = new CosmosMessage(message);
        }

        String correlationKeyName = getEndpointConfiguration().getCorrelator().getCorrelationKeyName(getName());
        String correlationKey = getEndpointConfiguration().getCorrelator().getCorrelationKey(cosmosMessage);
        correlationManager.saveCorrelationKey(correlationKeyName, correlationKey, context);

        String cosmosEndpointUri;
        if (getEndpointConfiguration().getEndpointUriResolver() != null) {
            cosmosEndpointUri = getEndpointConfiguration().getEndpointUriResolver().resolveEndpointUri(cosmosMessage, getEndpointConfiguration().getEndpoint());
        } else {
            cosmosEndpointUri = getEndpointConfiguration().getEndpoint();
        }

        String cosmosKey = getEndpointConfiguration().getKey();

        CosmosOperation cosmosOperation = getEndpointConfiguration().getCosmosOperation();
        if (cosmosMessage.getCosmosOperation() != null) {
            cosmosOperation = cosmosMessage.getCosmosOperation();
        }

        String cosmosDatabaseName = getEndpointConfiguration().getDatabaseName();
        if (cosmosMessage.getDatabaseName() != null) {
            cosmosDatabaseName = cosmosMessage.getDatabaseName();
        }

        String cosmosContainerName = getEndpointConfiguration().getContainerName();
        if (cosmosMessage.getContainerName() != null) {
            cosmosContainerName = cosmosMessage.getContainerName();
        }

        String cosmosPartitionKey = cosmosMessage.getPartitionKey();

        if (logger.isDebugEnabled()) {
            String debugMessage = String.format("Sending Cosmos message to: '%s' with operation '%s' using database name '%s'",
                    cosmosEndpointUri, cosmosOperation, cosmosDatabaseName);

            if (cosmosOperation == CREATE_CONTAINER ||
                    cosmosOperation == CREATE_CONTAINER_IF_NOT_EXISTS ||
                    cosmosOperation == DROP_CONTAINER ||
                    cosmosOperation == CREATE_ITEM ||
                    cosmosOperation == READ_ITEM ||
                    cosmosOperation == QUERY_ITEMS) {
                debugMessage += String.format(" and using container name '%s'", cosmosContainerName);
            }

            logger.debug(debugMessage);
            if (cosmosOperation == CREATE_ITEM) {
                logger.debug("Message to send: " + System.lineSeparator() + cosmosMessage.getPayload());
            }
        }

        CosmosMessage requestCosmosMessage = getEndpointConfiguration()
                .getCosmosMessageCosmosResponseConverter()
                .convertOutbound(cosmosMessage, getEndpointConfiguration(), context);

        com.azure.cosmos.CosmosClient cosmosClient = null;
        try {
            CosmosResponse<?> cosmosResponse = null;
            CosmosItemResponse<?> cosmosItemResponse = null;
            CosmosPagedIterable<?> cosmosPagedIterable = null;
            cosmosClient = new CosmosClientBuilder()
                    .endpoint(cosmosEndpointUri)
                    .key(cosmosKey)
                    .buildClient();

            CosmosDatabase cosmosDatabase;
            CosmosContainer cosmosContainer;
            switch (cosmosOperation) {
                case CREATE_DATABASE_IF_NOT_EXISTS:
                    cosmosResponse = cosmosClient.createDatabaseIfNotExists(cosmosDatabaseName);
                    break;
                case CREATE_DATABASE:
                    cosmosResponse = cosmosClient.createDatabase(cosmosDatabaseName);
                    break;
                case DROP_DATABASE:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosResponse = cosmosDatabase.delete();
                    break;
                case CREATE_CONTAINER_IF_NOT_EXISTS:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosResponse = cosmosDatabase.createContainerIfNotExists(cosmosContainerName, cosmosPartitionKey);
                    break;
                case CREATE_CONTAINER:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosResponse = cosmosDatabase.createContainer(cosmosContainerName, cosmosPartitionKey);
                    break;
                case DROP_CONTAINER:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosContainer = cosmosDatabase.getContainer(cosmosContainerName);
                    cosmosResponse = cosmosContainer.delete();
                    break;
                case CREATE_ITEM:
                    Object payload;
                    if (requestCosmosMessage.getPayload() instanceof String) {
                        payload = new ObjectMapper().readValue((String) requestCosmosMessage.getPayload(), requestCosmosMessage.getItemClass());
                    } else {
                        payload = requestCosmosMessage.getPayload();
                    }
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosContainer = cosmosDatabase.getContainer(cosmosContainerName);
                    cosmosItemResponse = cosmosContainer.createItem(payload);
                    break;
                case READ_ITEM:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosContainer = cosmosDatabase.getContainer(cosmosContainerName);
                    cosmosItemResponse = cosmosContainer.readItem(requestCosmosMessage.getItemId(), new PartitionKey(requestCosmosMessage.getItemId()), requestCosmosMessage.getItemClass());
                    break;
                case DELETE_ITEM:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosContainer = cosmosDatabase.getContainer(cosmosContainerName);
                    cosmosItemResponse = cosmosContainer.deleteItem(requestCosmosMessage.getItemId(), new PartitionKey(requestCosmosMessage.getItemId()), new CosmosItemRequestOptions());
                    break;
                case QUERY_ITEMS:
                    cosmosDatabase = cosmosClient.getDatabase(cosmosDatabaseName);
                    cosmosContainer = cosmosDatabase.getContainer(cosmosContainerName);
                    cosmosPagedIterable = cosmosContainer.queryItems(requestCosmosMessage.getQuery(), new CosmosQueryRequestOptions(), requestCosmosMessage.getItemClass());
                    break;
            }

            logger.info("Cosmos message was sent to endpoint: '" + cosmosEndpointUri + "'");
            CosmosMessage cosmosMessageResponse;
            if (cosmosResponse != null) {
                cosmosMessageResponse = getEndpointConfiguration().getCosmosMessageCosmosResponseConverter().convertInbound(cosmosResponse, getEndpointConfiguration(), context);
            } else if (cosmosItemResponse != null) {
                cosmosMessageResponse = getEndpointConfiguration().getCosmosMessageCosmosItemResponseConverter().convertInbound(cosmosItemResponse, getEndpointConfiguration(), context);
                if (cosmosMessageResponse.getPayload(String.class).equals("null")) {
                    cosmosMessageResponse.setPayload(cosmosMessage.getPayload());
                }
            } else if (cosmosPagedIterable != null) {
                cosmosMessageResponse = getEndpointConfiguration().getCosmosMessageCosmosPagedIterableResponseConverter().convertInbound(cosmosPagedIterable, getEndpointConfiguration(), context);
            } else {
                cosmosMessageResponse = new CosmosMessage();
            }
            cosmosMessageResponseEnrichmentMissingData(cosmosMessageResponse, cosmosMessage);
            cosmosMessageResponse.status(CosmosStatus.valueOf(cosmosMessageResponse.getStatusCode()));

            correlationManager.store(correlationKey, cosmosMessageResponse);
        } catch (Exception exception) {
            logger.info("Caught Cosmos client exception: " + exception.getMessage());
            logger.info("Propagating Cosmos client exception according to error handling strategy");
            CosmosMessage responseExceptionMessage = getEndpointConfiguration().getCosmosMessageCosmosExceptionConverter()
                    .convertInbound(exception, getEndpointConfiguration(), context);

            cosmosMessageResponseEnrichmentMissingData(responseExceptionMessage, cosmosMessage);
            responseExceptionMessage.status(CosmosStatus.valueOf(responseExceptionMessage.getStatusCode()));

            correlationManager.store(correlationKey, responseExceptionMessage);
        } finally {
            if (cosmosClient != null) {
                cosmosClient.close();
            }
        }

    }

    private CosmosMessage cosmosMessageResponseEnrichmentMissingData(CosmosMessage cosmosMessageResponse, CosmosMessage cosmosMessage) {
        cosmosMessageResponse.cosmosOperation(cosmosMessage.getCosmosOperation());

        if (cosmosMessageResponse.getDatabaseName() == null && cosmosMessage.getDatabaseName() != null)
            cosmosMessageResponse.databaseName(cosmosMessage.getDatabaseName());

        if (cosmosMessageResponse.getContainerName() == null && cosmosMessage.getContainerName() != null)
            cosmosMessageResponse.containerName(cosmosMessage.getContainerName());

        if (cosmosMessageResponse.getPartitionKey() == null && cosmosMessage.getPartitionKey() != null)
            cosmosMessageResponse.partitionKey(cosmosMessage.getPartitionKey());

        if (cosmosMessageResponse.getItemClass() == null && cosmosMessage.getItemClass() != null)
            cosmosMessageResponse.itemClass(cosmosMessage.getItemClass());

        if (cosmosMessageResponse.getItemId() == null && cosmosMessage.getItemId() != null)
            cosmosMessageResponse.itemId(cosmosMessage.getItemId());

        if (cosmosMessageResponse.getQuery() == null && cosmosMessage.getQuery() != null)
            cosmosMessageResponse.query(cosmosMessage.getQuery());

        return cosmosMessageResponse;
    }

    @Override
    public Message receive(TestContext context) {
        return receive(correlationManager.getCorrelationKey(
                getEndpointConfiguration().getCorrelator().getCorrelationKeyName(getName()), context), context);
    }

    @Override
    public Message receive(String selector, TestContext context) {
        return receive(selector, context, getEndpointConfiguration().getTimeout());
    }

    @Override
    public Message receive(TestContext context, long timeout) {
        return receive(correlationManager.getCorrelationKey(
                getEndpointConfiguration().getCorrelator().getCorrelationKeyName(getName()), context), context, timeout);
    }

    @Override
    public Message receive(String selector, TestContext context, long timeout) {
        Message message = correlationManager.find(selector, timeout);
        if (message == null) {
            throw new ActionTimeoutException("Action timeout while receiving synchronous reply message from http server");
        }
        return message;
    }

    /**
     * Creates a message producer for this endpoint for sending messages
     * to this endpoint.
     *
     * @return Producer
     */
    @Override
    public Producer createProducer() {
        return this;
    }

    /**
     * Creates a message consumer for this endpoint. Consumer receives
     * messages on this endpoint.
     *
     * @return SelectiveConsumer
     */
    @Override
    public SelectiveConsumer createConsumer() {
        return this;
    }

    /**
     * Sets the correlation manager.
     *
     * @param correlationManager Correlation manager
     */
    public void setCorrelationManager(CorrelationManager<Message> correlationManager) {
        this.correlationManager = correlationManager;
    }
}
