package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.context.TestContext;
import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.consol.citrus.message.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.exception.ExceptionUtils;

/**
 * CosmosMessageCosmosExceptionConverter implementation able to convert Cosmos DB exception to internal message
 * representation and other way round.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosMessageCosmosExceptionConverter extends CosmosMessageBaseAbstractConverter<Exception> {
    @Override
    public CosmosMessage convertOutbound(Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
        throw new UnsupportedOperationException("CosmosMessageCosmosExceptionConverter does not support CosmosMessage 'convertOutbound'");
    }

    @Override
    public void convertOutbound(CosmosMessage externalMessage, Message internalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
        throw new UnsupportedOperationException("CosmosMessageCosmosExceptionConverter does not support CosmosMessage objects");
    }

    @Override
    public CosmosMessage convertInbound(Exception externalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
        ObjectMapper objectMapper = new ObjectMapper();
        CosmosMessage cosmosMessage = endpointConfigurationToCosmosMessage(endpointConfiguration);

        cosmosMessage.statusCode(500);

        try {
            cosmosMessage.setPayload(objectMapper.writeValueAsString(externalMessage));
        } catch (JsonProcessingException exception) {
            cosmosMessage.setPayload(ExceptionUtils.getStackTrace(exception));
        }

        return cosmosMessage;
    }
}
