package com.consol.citrus.extension.cosmosdb.client;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;

/**
 * Enumeration of Cosmos DB status codes.
 *
 * @author Mariusz Marciniuk
 * @see <a href="https://docs.microsoft.com/pl-pl/rest/api/cosmos-db/http-status-codes-for-cosmosdb">HTTP Status Codes for Azure Cosmos DB</a>
 * @since 0.0.3
 */
@SuppressWarnings("unused")
public enum CosmosStatus {

    /**
     * {@code 200 OK}
     * <p>
     * One of the following REST operations were successful:
     * <ul>
     *     <li>GET on a resource.</li>
     *     <li>PUT on a resource.</li>
     *     <li>POST on a resource.</li>
     *     <li>POST on a stored procedure resource to execute the stored procedure.</li>
     * </ul>
     *  </p>
     */
    OK(HttpStatus.OK),
    /**
     * {@code 201 Created}
     * <p>
     * A POST operation to create a resource is successful.
     * </p>
     */
    CREATED(HttpStatus.CREATED),
    /**
     * {@code 204 No content}
     * <p>
     * The DELETE operation is successful.
     * </p>
     */
    NO_CONTENT(HttpStatus.NO_CONTENT),
    /**
     * {@code 400 Bad Request}
     * <p>
     * The JSON, SQL, or JavaScript in the request body is invalid.
     * <p>
     * In addition, a 400 can also be returned when the required properties of a resource are not present or set in the body of the POST or PUT on the resource.
     * <p>
     * 400 is also returned when the consistent level for a GET operation is overridden by a stronger consistency from the one set for the account.
     * <p>
     * 400 is also returned when a request that requires an x-ms-documentdb-partitionkey does not include it.
     * </p>
     */
    BAD_REQUEST(HttpStatus.BAD_REQUEST),
    /**
     * {@code 401 Unauthorized}
     * <p>
     * 401 is returned when the Authorization header is invalid for the requested resource.
     * </p>
     */
    UNAUTHORIZED(HttpStatus.UNAUTHORIZED),
    /**
     * {@code 403 Forbidden}
     * <p>
     * The authorization token expired.
     * <p>
     * 403 code is also returned during a POST operation to create a resource when the resource quota has been reached. An example of this scenario is when you try to add documents to a collection that has reached its provisioned storage.
     * <p>
     * 403 can also be returned when a stored procedure, trigger, or UDF has been flagged for high resource usage and blocked from execution.
     * <p>
     * 403 forbidden error is returned when the firewall rules configured on your Azure Cosmos DB account block your request. Any requests originating from machines outside the allowed list will receive a 403 response.
     * <p>
     * 403.3 – This status code is returned for write requests during the manual failover operation. This status code is used as a redirection code by drivers to forward the write requests to a new write region. Direct REST client must perform GET on DatabaseAccount to identify the current write region and forward the write request to that endpoint.
     * </p>
     */
    FORBIDDEN(HttpStatus.FORBIDDEN),
    /**
     * {@code 404 Not Found}
     * <p>
     * The operation is attempting to act on a resource that no longer exists. For example, the resource may have already been deleted.
     * </p>
     */
    NOT_FOUND(HttpStatus.NOT_FOUND),
    /**
     * {@code 408 Request Timeout}
     * <p>
     * The operation did not complete within the allotted amount of time. This code is returned when a stored procedure, trigger, or UDF (within a query) does not complete execution within the maximum execution time.
     * </p>
     */
    REQUEST_TIMEOUT(HttpStatus.REQUEST_TIMEOUT),
    /**
     * {@code 409 Conflict}
     * <p>
     * The ID provided for a resource on a PUT or POST operation has been taken by an existing resource. Use another ID for the resource to resolve this issue. For partitioned collections, ID must be unique within all documents with the same partition key value.
     * </p>
     */
    CONFLICT(HttpStatus.CONFLICT),
    /**
     * {@code 412 Precondition Failed}
     * <p>
     * The operation specified an eTag that is different from the version available at the server, that is, an optimistic concurrency error. Retry the request after reading the latest version of the resource and updating the eTag on the request.
     * </p>
     */
    PRECONDITION_FAILED(HttpStatus.PRECONDITION_FAILED),
    /**
     * {@code 413 Request Entity Too Large}
     * <p>
     * The document size in the request exceeded the allowable document size for a request. The max allowable document size is 2 MB.
     * </p>
     */
    @SuppressWarnings("deprecation") REQUEST_ENTITY_TOO_LARGE(HttpStatus.REQUEST_ENTITY_TOO_LARGE),
    /**
     * {@code 423 Locked}
     * <p>
     * The throughput scale operation cannot be performed because there is another scale operation in progress.
     * </p>
     */
    LOCKED(HttpStatus.LOCKED),
    /**
     * {@code 424 Failed Dependency}
     * <p>
     * When a document operation fails within the transactional scope of a TransactionalBatch operation, all other operations within the batch are considered failed dependencies. This status code indicates that the current operation was considered failed because of another failure within the same transactional scope.
     * </p>
     */
    FAILED_DEPENDENCY(HttpStatus.FAILED_DEPENDENCY),
    /**
     * {@code 429 Too Many Requests}
     * <p>
     * The collection has exceeded the provisioned throughput limit. Retry the request after the server specified retry after duration. For more information, see request units.
     * </p>
     */
    TOO_MANY_REQUESTS(HttpStatus.TOO_MANY_REQUESTS),
    /**
     * {@code 449 Retry With}
     * <p>
     * The operation encountered a transient error. This code only occurs on write operations. It is safe to retry the operation.
     * </p>
     */
    RETRY_WITH(449, "Retry With"),
    /**
     * {@code 500 Internal Server Error}
     * <p>
     * The operation failed due to an unexpected service error. Contact support. See Filing an Azure support issue.
     * </p>
     */
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR),
    /**
     * {@code 503 Service Unavailable}
     * <p>
     * The operation could not be completed because the service was unavailable. This situation could happen due to network connectivity or service availability issues. It is safe to retry the operation. If the issue persists, contact support.
     * </p>
     */
    SERVICE_UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE),

    // Substatus codes for server-side issues

    /**
     * {@code 4000 Failed to get/access the Azure AD token}
     * <p>
     * This error occurs if Azure Cosmos DB can’t get the Azure Active Directory (Azure AD) access token. This token is required for Azure Cosmos DB to access the Key Vault. The error could occur due to a networking issue or a data center issue and it’s not something the user can take an action. Create a support request to reach the Azure Cosmos DB team to resolve the issue.
     * </p>
     */
    FAILED_TO_GET_OR_ACCESS_THE_AZURE_AD_TOKEN(4000, "Failed to get/access the Azure AD token"),
    /**
     * {@code 4001 Azure AD service is unavailable}
     * <p>
     * This error occurs if the Azure AD service is down or has issues. You can check the Azure outage dashboard to verify if there is any existing outage. These outages are typically resolved within a couple of hours. It’s best for you can contact the Azure AD team and let them know the issue you are seeing. If the Azure AD team finds that there is no issue, then create a support request to reach the Azure Cosmos DB team for resolution.
     * </p>
     */
    AZURE_AD_SERVICE_IS_UNAVAILABLE(4001, "Azure AD service is unavailable"),
    /**
     * {@code 4004 Key Vault service is unavailable}
     * <p>
     * This error occurs if Azure Cosmos DB tries to access the Key Vault, but the service is not available. This could be due to a networking issue to reach Key Vault or the service itself may be down. You can check the Azure outage dashboard to verify if there is any existing outage. These outages are typically resolved within a couple of hours. It’s best for you can contact the Key Vault team and let them know the issue you are seeing. If the Key Vault team finds that there is no issue, then create a support request to reach the Azure Cosmos DB team for resolution.
     * </p>
     */
    KEY_VAULT_SERVICE_IS_UNAVAILABLE(4004, "Key Vault service is unavailable"),
    /**
     * {@code 4007 Internal server error}
     * <p>
     * This is an internal server error and it occurs if the input bytes are not in the base64 format.
     * </p>
     */
    INTERNAL_SERVER_ERROR_SERVER_SIDE(4007, "Internal server error"),
    /**
     * {@code 4008 Key Vault internal service errors}
     * <p>
     * This error occurs if Azure Cosmos DB is unable to access the Key Vault. It could be due to a networking issue or if the Key Vault service itself is down. You can check the Azure outage dashboard to verify if there is any existing outage. These outages are typically resolved within a couple of hours. It’s best for you can contact the Key Vault team and let them know the issue you are seeing. If the Key Vault team finds that there is no issue, then reach out to the Azure Cosmos DB team for resolution.
     * </p>
     */
    KEY_VAULT_INTERNAL_SERVICE_ERRORS(4008, "Key Vault internal service errors"),
    /**
     * {@code 1013 Collection create operation is in progress}
     * <p>
     * If you encounter timeout exception when creating a collection, do a read operation to validate if the collection was created successfully. The read operation throws an exception until the collection create operation is successful. If the read operation throws an exception with status code of 404, and sub status code of 1013, it means that the collection create operation is still in progress. Retry the read operation until you get 200 or 201 status codes, these codes let you know that the collection has been successfully created.
     * </p>
     */
    COLLECTION_CREATE_OPERATION_IS_IN_PROGRESS(1013, "Collection create operation is in progress"),

    //Substatus codes for end-user issues

    /**
     * {@code 4002 Key Vault doesn’t grant permission to the Azure AD, or the key is disabled}
     * <p>
     * This issue occurs if you have removed the Azure Cosmos DB identity from the Key Vault access policies or if you have disabled the key. This issue is typically caused by the end user. If this error occurs, make sure that the Azure Cosmos DB has access to the Key Vault and the key is enabled.
     * </p>
     */
    KEY_MISSING_PERMISSION_OR_DISABLED(4002, "Key Vault doesn’t grant permission to the Azure AD, or the key is disabled"),
    /**
     * {@code 4003 Key is not found}
     * <p>
     * This issue occurs if the key is deleted from the Key Vault. This issue is typically caused by the end user. One of the prerequisites to use Azure Cosmos DB with customer-managed keys is that the Key Vault has soft delete and purge protection enabled. This means you can recover the deleted key and restore access to Azure Cosmos DB.
     * </p>
     */
    KEY_IS_NOT_FOUND(4003, "Key is not found"),
    /**
     * {@code 4005 Unable to wrap or unwrap the key}
     */
    UNABLE_TO_WRAP_OR_UNWRAP_THE_KEY(4005, "Unable to wrap or unwrap the key"),
    /**
     * {@code 4006 Key URL is invalid}
     * <p>
     * This error occurs during provisioning if you have included the key version in the Key Vault URL. This error is often caused by the end user. To resolve this error, remove the version and try again. For example, if you have used the URL in the format https://<KeyVaultName>.vault.azure.net/keys/<KeyName>/<KeyVersion>, update it to https://<KeyVaultName>.vault.azure.net/keys/<KeyName>/
     * </p>
     */
    KEY_URL_IS_INVALID(4006, "Key URL is invalid"),
    /**
     * {@code 4009 Key Vault DNS name can't be resolved}
     * <p>
     * This error occurs if the Key Vault DNS name couldn’t be resolved, because you have used the incorrect Key Vault name. This error is caused by the end user. To resolve it, correct the Key Vault name and try again.
     * </p>
     */
    KEY_VAULT_DNS_NAME_CANT_BE_RESOLVED(4009, "Key Vault DNS name can't be resolved");


    private final int value;
    private final String reasonPhrase;

    CosmosStatus(int value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    CosmosStatus(HttpStatus httpStatus) {
        this(httpStatus.value(), httpStatus.getReasonPhrase());
    }

    /**
     * @return Return the integer value of this status code.
     */
    public int value() {
        return value;
    }

    /**
     * @return Return the reason phrase of this status code.
     */
    public String getReasonPhrase() {
        return reasonPhrase;
    }

    /**
     * Return a string representation of this status code.
     */
    @Override
    public String toString() {
        return Integer.toString(value);
    }

    /**
     * Return the enum constant of this type with the specified numeric value.
     *
     * @param statusCode the numeric value of the enum to be returned
     * @return the enum constant with the specified numeric value
     * @throws IllegalArgumentException if this enum has no constant for the specified numeric value
     */
    public static CosmosStatus valueOf(int statusCode) {
        CosmosStatus status = resolve(statusCode);
        if (status == null) {
            throw new IllegalArgumentException("No matching constant for [" + statusCode + "]");
        }
        return status;
    }

    /**
     * Resolve the given status code to an {@code CosmosStatus}, if possible.
     *
     * @param statusCode the CosmosStatus status code (potentially non-standard)
     * @return the corresponding {@code CosmosStatus}, or {@code null} if not found
     * @since 0.0.3
     */
    @Nullable
    public static CosmosStatus resolve(int statusCode) {
        for (CosmosStatus status : values()) {
            if (status.value == statusCode) {
                return status;
            }
        }
        return null;
    }

}
