package com.consol.citrus.extension.cosmosdb.client;

/**
 * Enumeration of Cosmos DB operations.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public enum CosmosOperation {

    CREATE_DATABASE,
    CREATE_DATABASE_IF_NOT_EXISTS,
    DROP_DATABASE,
    CREATE_CONTAINER,
    CREATE_CONTAINER_IF_NOT_EXISTS,
    DROP_CONTAINER,
    CREATE_ITEM,
    READ_ITEM,
    DELETE_ITEM,
    QUERY_ITEMS,

}
