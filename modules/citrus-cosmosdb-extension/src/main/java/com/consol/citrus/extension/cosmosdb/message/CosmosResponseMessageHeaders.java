package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.message.MessageHeaders;

/**
 * Cosmos response message headers.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosResponseMessageHeaders {

    /**
     * Prevent instantiation.
     */
    private CosmosResponseMessageHeaders() {
    }

    public static final String COSMOS_PREFIX = MessageHeaders.PREFIX + "cosmos_";

    public static final String COSMOS_STATUS_CODE = COSMOS_PREFIX + "status_code";
    public static final String COSMOS_REASON_PHRASE = COSMOS_PREFIX + "reason_phrase";
    public static final String COSMOS_OPERATION = COSMOS_PREFIX + "operation";
    public static final String COSMOS_ENDPOINT = COSMOS_PREFIX + "endpoint";
    public static final String COSMOS_DATABASE_NAME = COSMOS_PREFIX + "database_name";
    public static final String COSMOS_CONTAINER_NAME = COSMOS_PREFIX + "container_name";
    public static final String COSMOS_PARTITION_KEY = COSMOS_PREFIX + "partition_key";
    public static final String COSMOS_ITEM_CLASS = COSMOS_PREFIX + "item_class";
    public static final String COSMOS_ITEM_ID = COSMOS_PREFIX + "item_id";
    public static final String COSMOS_QUERY = COSMOS_PREFIX + "query";

}
