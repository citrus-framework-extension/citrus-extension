package com.consol.citrus.extension.cosmosdb.message;

import com.azure.cosmos.util.CosmosPagedIterable;
import com.consol.citrus.context.TestContext;
import com.consol.citrus.extension.cosmosdb.client.CosmosEndpointConfiguration;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.Locale;

/**
 * Message converter implementation able to convert Cosmos DB request and response (paged response) entities to internal message
 * representation and other way round.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosMessageCosmosPagedIterableResponseConverter extends CosmosMessageBaseAbstractConverter<CosmosPagedIterable<?>> {

    @Override
    public CosmosMessage convertInbound(CosmosPagedIterable<?> externalMessage, CosmosEndpointConfiguration endpointConfiguration, TestContext context) {
        ObjectMapper objectMapper = new ObjectMapper();
        CosmosMessage cosmosMessage = endpointConfigurationToCosmosMessage(endpointConfiguration);
        cosmosMessage.statusCode(200);

        Locale.setDefault(Locale.ENGLISH); // Workaround - https://github.com/Azure/azure-sdk-for-java/issues/9136

        try {
            cosmosMessage.setPayload(objectMapper.writeValueAsString(externalMessage));
        } catch (JsonProcessingException exception) {
            cosmosMessage.setPayload(ExceptionUtils.getStackTrace(exception));
        }

        return cosmosMessage;
    }

}
