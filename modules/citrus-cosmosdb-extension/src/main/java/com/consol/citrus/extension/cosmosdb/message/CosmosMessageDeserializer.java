package com.consol.citrus.extension.cosmosdb.message;

import com.consol.citrus.extension.message.DefaultMessageDeserializer;
import com.consol.citrus.message.DefaultMessage;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;

import java.io.IOException;

import static com.consol.citrus.extension.cosmosdb.message.CosmosResponseMessageHeaders.COSMOS_ITEM_CLASS;

/**
 * Deserializer for citrus CosmosMessage.
 *
 * @author Mariusz Marciniuk
 * @since 0.0.3
 */
public class CosmosMessageDeserializer extends DefaultMessageDeserializer<CosmosMessage> {

    /**
     * Default constructor.
     */
    public CosmosMessageDeserializer() {
        this(null);
    }

    /**
     * Constructor with Class parameter.
     *
     * @param vc Class
     */
    public CosmosMessageDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public CosmosMessage deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        DefaultMessage defaultMessage = super.deserialize(p, ctxt);
        CosmosMessage cosmosMessage = new CosmosMessage(defaultMessage);

        Class<?> itemClassHeader = cosmosMessage.getItemClass();
        if (itemClassHeader == null) {
            cosmosMessage.removeHeader(COSMOS_ITEM_CLASS);
        } else {
            cosmosMessage.itemClass(itemClassHeader);
        }

        return cosmosMessage;
    }
}
