package com.consol.citrus.extension.cosmosdb.client;

import com.consol.citrus.endpoint.AbstractEndpointBuilder;

/**
 * CosmosClientBuilder
 *
 * @author Mariusz Marciniuk
 * @since 0.0.1
 */
public class CosmosClientBuilder extends AbstractEndpointBuilder<CosmosClient> {

    /**
     * Cosmos DB endpoint client.
     */
    private CosmosClient cosmosClientEndpoint = new CosmosClient();

    @Override
    protected CosmosClient getEndpoint() {
        return cosmosClientEndpoint;
    }

    /**
     * Sets the endpointUrl property.
     *
     * @param endpointUrl Cosmos DB endpoint.
     * @return The altered CosmosClientBuilder.
     */
    public CosmosClientBuilder endpoint(String endpointUrl) {
        cosmosClientEndpoint.getEndpointConfiguration().setEndpoint(endpointUrl);
        return this;
    }

    /**
     * Sets the key property.
     *
     * @param key Cosmos DB key.
     * @return The altered CosmosClientBuilder.
     */
    public CosmosClientBuilder key(String key) {
        cosmosClientEndpoint.getEndpointConfiguration().setKey(key);
        return this;
    }

}
